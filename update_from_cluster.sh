#! /bin/sh

echo "Copying plot images"
rsync -azPr vm-box:~/stabilitysampling/plots/images ./plots

echo "Copying data cache"
rsync -azPr vm-box:~/stabilitysampling/data/cache ./data


