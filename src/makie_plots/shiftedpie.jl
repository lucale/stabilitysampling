using Makie 

"""
    ShiftedPie(pos, fractions; kwargs...)

Creates a pie chart with the given `fractions` shifted to position `pos`.
The position should be a 2D point accessed by (`pos[1]` | `pos[2]` ).

## Attributes

Available attributes and their defaults for Plot{Makie.ShiftedPie} are:

    color           :gray
    inner_radius    0
    inspectable     true
    normalize       true
    offset          0
    radius          1
    strokecolor     :black
    strokewidth     1
    transparency    false
    vertex_per_deg  1
    visible         true

"""
@recipe(ShiftedPie, pos, values) do scene
    Theme(
        normalize = true,
        color = :gray,
        strokecolor = :black,
        strokewidth = 1,
        vertex_per_deg = 5,
        xradius = 1,
        yradius = 1,
        inner_xradius = 0,
        inner_yradius = 0,
        offset = 0,
        inspectable = theme(scene, :inspectable),
        visible = true,
        transparency = false
    )
end

function Makie.plot!(plot::ShiftedPie)

    pos = plot[:pos]
    values = plot[:values]

    polys = lift(plot, pos, values, plot.vertex_per_deg, plot.xradius, plot.yradius, plot.inner_xradius, plot.inner_yradius, plot.offset, plot.normalize, Makie.current_axis().scene.camera.pixel_space) do pos, vals, vertex_per_deg, xradius, yradius, inner_xradius, inner_yradius, offset, normalize, pixel_space

        T = eltype(vals)

        # find start and end angles of all pie pieces
        summed = cumsum([zero(T); vals])
        boundaries = if normalize
            summed ./ summed[end] .* 2pi
        else
            summed
        end

        # create vector of a vector of points for each piece
        vertex_arrays = map(boundaries[1:end-1], boundaries[2:end]) do sta, en
            shift = Point2f( pos[1], pos[2] )
            distance = en - sta
            # how many vertices are needed for the curve?
            nvertices = max(2, ceil(Int, rad2deg(distance) * vertex_per_deg))

            # curve points
            points = map(LinRange(sta, en, nvertices)) do rad
                p =  [ Point2f(xradius * cos(rad + offset), yradius * sin(rad + offset)) ... , 0, 0] 
                shift + Point2f(p[1], p[2])
            end

            # add inner points (either curve or one point)
            if inner_xradius != 0 || inner_yradius != 0 
                inner_points = map(LinRange(en, sta, nvertices)) do rad
                    p = [ Point2f(inner_xradius * cos(rad + offset), inner_yradius * sin(rad + offset)) ... , 0, 0] 
                    shift + Point2f(p[1], p[2])
                end

                append!(points, inner_points)
            else
                push!(points, shift)
            end

            points
        end
    end

    
    # plot pieces as polys
    plot[:poly] = poly!(
        plot, polys,
        color = plot.color, strokewidth = plot.strokewidth,
        strokecolor = plot.strokecolor, inspectable = plot.inspectable,
        visible = plot.visible, transparency = plot.transparency
    )

    plot
end
