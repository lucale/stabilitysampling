function pairplot(x::Array, labels::Vector{String} = string.(1:size(x, 2)) ;kwargs... )
    ndim = size(x,2)
    fig_width = 300
    aspect = 0.8
    buffer = 60
    fig = Figure(size=(fig_width*( ndim < 2 ? 2 : ndim) + buffer, fig_width*aspect*ndim + buffer))
    pairplot!(fig, x, labels; kwargs...)

    for i=1:ndim
        colsize!(fig.layout, i, 230)
        rowsize!(fig.layout, i, 230*aspect)
    end
    return fig
end

function pairplot!(fig, x::Array, labels::Vector{String} = repeat([""], size(x, 2)) ; 
        densitycolor = :black, 
        histcolor = (:slategray, 0.4),
        contourcolormap = [:gray, :slategray],
        hexbincolormap = [:snow3, :silver, :ivory4],
        bins = 40, levels = 5,
        lims = fill(:auto, size(x,2)), keeplims = false
    )
    
    ndim = size(x,2)
    a = minimum(x, dims=1)[1,:]
    b = maximum(x, dims=1)[1,:]
    if any(b .- a .< 1e-12) 
        idx = findall( b .- a .< 1e-12 ) 
        println("Some dimensions have no variation! \nAffected dimensions are $idx")
    end

    for i=1:length(lims)
        if lims[i] != :auto
            a[i] = lims[i][1]
            b[i] = lims[i][2]
        end
    end
    
    H = [] # diagonal: histograms and densities 
    for i=1:ndim
        #if b[i] > a[i] 
            ax = Axis(fig[i,i], ytickcolor=(:white,0.0), yticklabelcolor=(:white,0.0))   
            if i==1 ax.ylabel = labels[1] end
            if i==ndim ax.xlabel = labels[i] end
            if !keeplims && b[i] > a[i] 
                xlims!(ax, (a[i]-1e-12,b[i]+1e-12)) 
            end
            d = density!(ax, x[:,i], strokewidth=1.25, color=(:white, 0.0), strokecolor=densitycolor)
            h = hist!(ax, x[:,i], color=histcolor, normalization=:pdf, bins=floor(Int,sqrt(size(x,1)-1))+1)
            push!(H,h)
        #end
    end

    if ndim > 1
        nbins = map( h->h.bins[], H) .+ 1
        t = collect.(range.(a, b .+ (b .- a) .* 1e-3, nbins .+1))
                
        is_in_bin = [ (t[i][1:end-1]' .<= x[:,i]) .&& (x[:,i] .< t[i][2:end]') for i=1:ndim ]
        t = map(t->0.5 .* (t[1:end-1] .+ t[2:end]), t)
        for i=1:ndim
            for j=1:i-1
                ax = Axis(fig[i,j])
                
                if j==1 
                    ax.ylabel = labels[i] 
                else
                    ax.ytickcolor=(:white,0.0)
                    ax.yticklabelcolor=(:white,0.0)
                end
                if i==ndim 
                    ax.xlabel = labels[j] 
                else
                    ax.xtickcolor=(:white, 0.0)
                    ax.xticklabelcolor=(:white,0.0)
                end

                if !keeplims && 1 <= j <= ndim 
                    xlims!(ax, lims[j] == :auto ? (a[j]-1e-12,b[j]+1e-12) : lims[i])
                end

                h = hexbin!(ax, x[:,j], x[:,i], bins=40, colormap=hexbincolormap) 
                
                nfreq_ij = sum( reshape(is_in_bin[i], size(is_in_bin[i]) ... , 1) .* reshape(is_in_bin[j], size(is_in_bin[j],1),1,size(is_in_bin[j],2)), dims=1)[1,:,:]
                p_ij = nfreq_ij ./ sum(nfreq_ij)
                m = minimum(p_ij) ; h = maximum(p_ij) - m
                #levelsets = 10 .^ range(-1, -.1, levels) .* h .+ m

                contour!(ax, t[j], t[i], p_ij'; linewidth=1, colormap=contourcolormap, levels)

                if !keeplims
                    xlims!(ax, (a[j]-1e-12,b[j]+1e-12))                
                    ylims!(ax, (a[i]-1e-12,b[i]+1e-12))
                end
            end
        end
    end
    return fig
end
