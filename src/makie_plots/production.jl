function plot_yearly_production(n::Network)

    # Get yearly production and CO2 emissions
    yearlyprod = yearly_production(n)
    carriers = collect( keys(yearlyprod) )
    energies = collect( values(yearlyprod) )
    t_co2_per_kWh = n.carriers.co2_emissions[ indexin(carriers, n.carriers.Carrier) ]
    co2_emissions = energies .* t_co2_per_kWh

    # Seperate conventional and renewable
    is_conventional = co2_emissions .> 0 .|| carriers .== "nuclear"  
    idx = sortperm(-1e9 .* is_conventional .+ energies, rev=true)
    carriers = carriers[idx]
    energies = energies[idx]
    is_conventional = is_conventional[idx]
    co2_emissions = co2_emissions[idx]

    # Get nice names and colors for the carriers
    idx = indexin(carriers, n.carriers.Carrier) 
    nice_names = n.carriers.nice_name[idx]
    colors = n.carriers.color[idx]

    #=
    # DEBUG: Print the yearly production and CO2 emissions
    for (c,e,co2) in zip(carriers, energies, co2_emissions)
        println("$c \t $e \t $co2")
    end
    =#

    fig = Figure(size=(750, 550))
    ax = Axis(fig[1,1])

    # Plot the bars and lines
    yearlyload = yearly_load(n)
    barplot!(ax, 0:length(carriers), [yearlyload, energies...] ./ 1e6, color=[:teal, colors...], alpha=.8)
    vlines!(ax, length(is_conventional) - sum(is_conventional) + 0.5, color=:slategray, linestyle=:dash)
    vlines!(ax, .5, color=:black)
    #ylims!(ax, minimum(energies) / 1e6 , 2 .* yearlyload ./ 1e6 )
    ylims!(ax, 0.1 , 2 .* yearlyload ./ 1e6 )

    # Add text labels to the bars
    text!(ax, 0, yearlyload ./ 1e6, text= string(round(Int, round(yearlyload ./ 1e6, sigdigits=4))) * " TWh", 
        align=(:center,:bottom), color=:teal, fontsize=12)
    for i=1:length(carriers)
        e = energies[i]; unit_e = "MWh"
        if (e > 1e6) 
            e = e ./ 1e6; unit_e = "TWh"  
        elseif (e > 1e3) 
            e = e ./ 1e3; unit_e = "GWh" 
        end
        text!(ax, i, energies[i] ./ 1e6, 
            text= string(round(Int, round(e, sigdigits=4))) * " $unit_e", 
            align=(:center,:bottom), color=colors[i], fontsize=12)

        text!(ax, i, energies[i] ./ 1e6 ,
            text = string(round(energies[i] / yearlyload * 100, sigdigits=3)) * " %",
            align=(:center, :top), color=:black, fontsize=12
        )

        co2 = co2_emissions[i]; unit_co2 = "t"
        if co2 > 0
            if (co2 > 1e6)
                co2 = co2 ./ 1e6; unit_co2 = "Mt"
            elseif (co2 > 1e3)
                co2 = co2 ./ 1e3; unit_co2 = "kt"
            end
            text!(ax, i, 1.3 * energies[i] ./ 1e6, 
                text= "  " * string(round(Int, round(co2, sigdigits=4))) * " $unit_co2 CO₂", 
                align=(:center,:bottom), color=:darkred, fontsize=12)
        end
    end

    # Add more labels 
    #text!(ax, 1.25,  1.5 * yearlyload ./ 1e6, text="Yearly Load", align = (:center, :top), color=:black, fontsize=12)
    renewable_penetration = sum(energies[.! is_conventional]) / sum(energies) 
    text!(ax, 2.25, 1.5 * yearlyload ./ 1e6, 
        text="$(round(100 * renewable_penetration,sigdigits=4))% renewable", align = (:left, :top), fontsize=16)
    total_co2 = sum(co2_emissions)
    text!(ax, length(is_conventional) - sum(is_conventional)+.75, 1.5 * yearlyload ./ 1e6, 
              text="$(round(total_co2 ./ 1e6, sigdigits=4)) Mt CO₂", align = (:left, :top), color=:darkred, fontsize=16, justification=:right)

    ax.xticks = (0:length(carriers), ["Consumption", nice_names...])
    ax.ylabel = "Yearly Energy [TWh]"
    ax.xticklabelrotation = π/4
    ax.yscale = log10
    return fig

end



function plot_production_capacities(n::Network)
    buses = get_buses(n)
    carriers = unique(n.generators.carrier)
    N,C = length(buses), length(carriers)    
    pmax = zeros(N,C)
    for i=1:N
        prodcap = get_production_capacities(n, buses[i])
        cs = keys(prodcap)
        ps = values(prodcap)
        pmax[i, indexin(cs, carriers)] .= ps
    end

    a_x, b_x, a_y, b_y, aspect = geo_limits(n)

    fig = Figure(size=(600 + 220, 600 * aspect))
    ax = geo_ax(fig[1,1], n)

    # Plot the production capacities
    x,y = get_bus_coords(n)
    
    c = get_capacities(n)
    ac,bc = extrema(c)
    crel = c / bc .* 2.0 .+ 0.1 #(c .- ac) ./ (bc .- ac) .* 2.0 .+ 0.1
    B = incidence_matrix(n)
    u = [ argmax(B[:,i] .== -1) for i=1:size(B,2) ]
    v = [ argmax(B[:,i] .==  1) for i=1:size(B,2) ]
    for i=1:size(B,2)
        lines!(ax, [ x[ u[i] ], x[ v[i] ] ], [ y[ u[i] ], y[ v[i] ] ], color=:black, linewidth=3.0 * crel[i] ) 
    end

    idx = indexin(carriers, n.carriers.Carrier)
    colors = n.carriers.color[idx]
    nice_names = n.carriers.nice_name[idx]
    xradius = ( b_x - a_x ) / 100.0 
    yradius = aspect * xradius
    Pabs = sum(pmax, dims=2)[:,1]
    a,b = extrema(Pabs)
    for i=1:N
        s = (Pabs[i] - a) / (b - a) * 2.0 + 1
        shiftedpie!(ax, [x[i], y[i]], pmax[i,:] ./ Pabs[i]; 
            strokewidth=0,
            color=colors, xradius=xradius*s, yradius=yradius*s )
    end


    radii = [1.0, 0.5, .25]
    rlabel = radii .* maximum(Pabs)
    runits = ["MW", "MW", "MW"]
    for i=1:3
        if rlabel[i] > 1e3
            rlabel[i] = rlabel[i] ./ 1e3
            runits[i] = "GW"
        end
    end
    rlabel = round.(Int, round.( rlabel, sigdigits=3) )
    rlabel = string.(rlabel) .* " " .* runits

    for (i,r) in enumerate(radii)
        shiftedpie!(ax, [a_x + (0.04 + 0.07 * i) * (b_x - a_x), b_y - 0.03 * (b_y - a_y)], [1.0]; 
            color=[:black], xradius = (2 * r + .1) * xradius, yradius=  (2 * r + .05)  * yradius, strokewidth=0,

        )
        text!(ax, a_x + 0.08 * (b_x - a_x) + (i-1) * (b_x - a_x) * 0.08, b_y - 0.03 * (b_y - a_y) - 2.5 * yradius,
            text = "$(rlabel[i])", align = (:left, :top), fontsize=10 )
    end
    text!(ax, a_x + 0.18 * (b_x - a_x), b_y - 0.03 * (b_y - a_y) - 7 * yradius,
        text = "Nominal Production Capacity", align = (:center, :top), fontsize=12 )
    


    linewidths = 3.0 .* [2.1, 1.05, 0.525]
    linelabels = maximum(c) .* [1.0, 0.5, 0.25]
    units = ["MW", "MW", "MW"] 
    for i=1:3
        if linelabels[i] > 1e3
            linelabels[i] = linelabels[i] ./ 1e3
            units[i] = "GW"
        end
    end
    linelabels = string.( round.(Int, linelabels ) ) .* " " .* units

    co2 = n.carriers.co2_emissions[ indexin( carriers, n.carriers.Carrier ) ]
    idx = sortperm( sum(pmax,dims=1)[1,:] + co2 * 1000,rev=true )
    colors = colors[idx]
    nice_names = nice_names[idx]
    Legend(fig[1,2], 
        [ [LineElement(linewidth = lw) for lw in linewidths],
          [PolyElement(color=c) for c in colors]
        ],
        [ linelabels, nice_names ],
        [ "Line Capacities", "Carriers"],
        width=220
    )

    return fig
end