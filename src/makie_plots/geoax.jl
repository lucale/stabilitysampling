function geo_limits(x::Vector{<:Real}, y::Vector{<:Real}; margin = 0.8 ) 
    a_x, b_x = extrema(x) .+ [-margin, margin]
    a_y, b_y = extrema(y) .+ [-margin, margin]
    Δx = b_x - a_x
    Δy = b_y - a_y
    aspect = Δy / Δx
    return a_x, b_x, a_y, b_y, aspect
end

global GeoMakie_countries = nothing 
global GeoMakie_coastlines = nothing 

function geo_ax(fig_or_plot, x::Vector{<:Real}, y::Vector{<:Real}; margin = 0.8)
    ax = Axis(fig_or_plot)
    a_x, b_x, a_y, b_y, aspect = geo_limits(x,y; margin)

    # only load countries and coastlines to memory if plotting is needed  
    #global GeoMakie_countries
    #global GeoMakie_coastlines
    if isnothing(GeoMakie_countries)
        global GeoMakie_countries = GeoMakie.GeoJSON.read( read(GeoMakie.assetpath("vector", "countries.geo.json"), String) )
    end
    if isnothing(GeoMakie_coastlines)
        global GeoMakie_coastlines = GeoMakie.coastlines()
    end
    poly!(ax, GeoMakie_countries.geometry; color = (:white, 0), strokecolor = :gray, strokewidth = 0.75)
    lines!(ax, GeoMakie_coastlines, linewidth=0.75)
    xlims!(ax, (a_x, b_x))
    ylims!(ax, (a_y, b_y))
    return ax
end