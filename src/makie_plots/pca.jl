function plot_pca_eigvals(pca::PCA; tol=0.1, kwargs...)
    λ = pca.eigvals

    fig = Figure(size=(450,450)); ax = Axis(fig[1,1])
    lines!(ax, 1:length(λ), λ, color=:slategray, kwargs...)
    scatter!(ax, 1:length(λ), λ, color=:black)

    # quantiles 
    qs = [0.9, 0.75, 0.5]
    ns = [ relevant_dims(pca, q) for q in qs ] 
    vlines!(ax, ns, color=:red, linestyle=:dash)
    for (n,q) in zip(ns, qs)
        text!(ax, n, minimum(λ), text="$n dims explain $(round(q*100, digits=1))% cov", 
            rotation=π/2)
    end

    # zero eigenvalues
    i0 = argmax( λ .< tol ) - .75
    vlines!(ax, i0 , color=:blue, linestyle=:dash)
    text!(ax, i0, minimum(λ), text="$(floor(Int,i0)) non-zero dims", rotation=π/2) 

    # log-log scale (nearly linear)
    ax.xscale = log10
    ax.yscale = log10
    ax.ylabel = "Covariance Eigenvalue"
    ax.xlabel = "Component"
    return fig    
end

function plot_pca_pairplot(pca::PCA; model=nothing, kwargs...)
    x = pca.signal
    α = project(pca, x)
    n = relevant_dims(pca, 0.5)
    labels = ["PC $i" for i=1:n]
    fig = pairplot(α[:, 1:n], labels; kwargs...)
    
    if !isnothing(model) 
        if typeof(model) <: Product
            model = model.v[1:n]
        end
        bounds = stack( extrema(α, dims=1)[1,:], dims=1 )
        y = collect.([ range(bounds[i,1], bounds[i,2], length=100) for i=1:n ])
        ψ_y = [ pdf(model[i], y[i]) for i=1:n ] 
        
        for i=1:n 
            lines!(fig[i,i], y[i], ψ_y[i], color=:red)
            for j=i+1:n
                ax_prev = content(fig[j,i])
                ax = Axis(fig[j,i])
                pi = ψ_y[i]; pj = ψ_y[j]
                ψ_ij = pi .* pj'
                contour!(ax, y[i], y[j], ψ_ij, colormap=[:orange, :red], linewidth=1.5, alpha=0.6, levels=7)
                xlims!(ax, ax_prev.xaxis.attributes.limits[])
                ylims!(ax, ax_prev.yaxis.attributes.limits[])
                
            end
        end
    end

    return fig
end


function plot_pca_modes(k::Vector{<:Real}, B::SparseMatrixCSC, x::Vector{<:Real}, y::Vector{<:Real}, pca::PCA; F = flow_matrix(B,k))
    N = size(B,1)
    V = pca.eigvecs

    layout = (x,y)
    u = getindex.(argmax(B, dims=1)[1,:], 1)
    v = getindex.(argmin(B, dims=1)[1,:], 1)


    ndim = min(16, argmax(pca.relevance .> 0.9))
    nx = min(ndim, 4) ; ny = (ndim-1) ÷ 4 + 1

    a_x, b_x, a_y, b_y, aspect = geo_limits(x,y)
    fig = Figure(size=(300 * nx, 300*aspect * ny) )

    cmap_f = :Greens
    Vf = F * V 
    fabs_lims = (0., maximum(abs.(Vf))/2)
    
    cmap_p = :coolwarm
    p_lims = (-.4, .4)


    for k=1:ndim
        i = (k-1) ÷ 4 + 1
        j = (k-1) % 4 + 1
        ax = geo_ax(fig[i,j], x, y)
        ax.title = "PC $k"
        #hidedecorations!(ax)
        if (j != 1)  hideydecorations!(ax) end
        if (i != ny)  hidexdecorations!(ax) end
        #hidexdecorations!(ax)
        p = V[:, k]
        f = Vf[:,k]

        for l=1:size(B,2)
            xu = layout[1][ u[l] ]
            xv = layout[1][ v[l] ]
            yu = layout[2][ u[l] ]
            yv = layout[2][ v[l] ]
            linesegments!( [xu,xv], [yu,yv], color=abs(f[l]), colormap=cmap_f, colorrange=fabs_lims, linewidth=2)
        end

        mx = ( layout[1][u] .+ layout[1][v] ) ./ 2
        my = ( layout[2][u] .+ layout[2][v] ) ./ 2
        dx = layout[1][u] .- layout[1][v]
        dy = layout[2][u] .- layout[2][v]
        r = atan.(dx, -dy) .+ ( f .<= 0.0 ) .* π
        scatter!(ax, mx, my, color=abs.(f), colormap=cmap_f, colorrange=fabs_lims, markersize=10, marker=:utriangle, rotation=r)

        scatter!(ax, layout[1], layout[2], color=V[:,k], colormap=cmap_p, colorrange=p_lims, markersize=10)
    end
    Colorbar(fig[1:ny,nx+1], colormap=cmap_p, colorrange=p_lims, label="PCA Mode")

    return fig

end


function plot_pca_coef_daytime(pca::PCA, snapshots::Vector{DateTime})

    λ = sqrt.(pca.eigvals)
    x = pca.signal
    α = project(pca, x)
    t_in_hours = getproperty.(Hour.(snapshots .- snapshots[1]), :value) .% 24    
    
    ndim = min(16, relevant_dims(pca, 0.6))
    nx = min(ndim, 4) ; ny = (ndim-1) ÷ 4 + 1
    fig = Figure(size=(250*nx, 300*ny))


    for k=1:ndim
        i = (k-1) ÷ 4 + 1
        j = (k-1) % 4 + 1
        ax = Axis(fig[i,j])
        ax.title = "PC $k ( $(round(λ[k]/sum(λ) * 100, digits=1))% )"
        ax.xticks = 0:6:24
        xlims!(ax, -.01, 23.01)
        ax.xlabel = "Day time [hours]"
        #ax.ylabel = L"PCA Coefficient $ \alpha_%$k $ [MW]"
        if (j == 1) ax.ylabel = "Amplitude [MW]" end
        if (i != ny)  hidexdecorations!(ax) end


        m = zeros(24); s = zeros(24)
        for h=0:23
            mask = ( t_in_hours .% 24 .== h )
            α_k = α[:,k][mask]
            m[h+1] = mean(α_k)
            s[h+1] = std(α_k)
        end

        α_std = std(α[:,k])

        band!(ax, 0:23, m .- s, m .+ s, color=(:slategray,0.2))
        hlines!(ax, [-α_std, 0, α_std], color=[:darkgreen, :blue, :darkgreen], linewidth=0.75, alpha=0.5)
        vlines!(ax, 0:6:24, color=:slategray, linestyle=:dash)
        lines!(ax, 0:23, m, color=:black)


    end

    return fig
end 

# Function to calculate FFT frequencies
function fft_freq(n::Int, d::Real=1.0)
    val = 1.0 / (n * d)
    if n % 2 == 0
        return [ collect( 0:(n÷2-1) ) ;  collect( -(n÷2):(-1) )] .* val
    else
        return [ collect( 0:((n-1)÷2) );  collect(-((n-1)÷2):(-1) ) ] .* val
    end
end

function power_spectral_density(signal::AbstractMatrix{<:Real}, dt=1.0)
    T, N = size(signal)

    # Perform FFT along the time dimension for each spatial component
    fft_result = fft(signal, 1)

    # Calculate the Power Spectral Density (PSD)
    psd = abs.(fft_result).^2 / (T * dt)

    # only the first half of the frequency components (up to Nyquist frequency)
    psd = psd[1:div(T, 2) + 1, :]

    # Frequencies corresponding to the PSD
    freq = fft_freq(T, dt)[1:div(T, 2) + 1]

    return psd, freq 
end

function plot_pca_coef_psd(pca::PCA, snapshots::Vector{DateTime})


    λ = sqrt.(pca.eigvals)
    x = pca.signal
    α = project(pca, x)


    ndim = min(16, relevant_dims(pca, 0.6))
    nx = min(ndim, 4) ; ny = (ndim-1) ÷ 4 + 1
    α = α[:,1:ndim]
    
    dt_in_hours = Hour(snapshots[2] - snapshots[1]).value
    psd, freq = power_spectral_density(α, dt_in_hours) 
    psd = psd[2:end, :]; freq = freq[2:end]
    periods_in_days = 1.0 ./ (freq .* 24)  # Convert frequency (1/hour) to period in days
    
    fig = Figure(size=(250*nx, 300*ny))
    

    for k=1:ndim
        i = (k-1) ÷ 4 + 1
        j = (k-1) % 4 + 1
        ax = Axis(fig[i,j])
        ax.title = "PC $k ( $(round(λ[k]/sum(λ) * 100, digits=1))% )"
        vlines!(ax, [1, 7, 30], color=:slategray, linestyle=:dash)

        plt = lines!(ax, max.(1e-16, periods_in_days), min.(1e8, max.(1e-16, psd[:,k] .* 1e-6)))

        ax.xscale = log10
        xlims!(ax, max.(1/12.0, extrema(periods_in_days)))
        n_xticks = floor(Int, log2( length(periods_in_days)) )
        xticks = periods_in_days[ 2 .^ [1:n_xticks ... ]]
        xticklabels = map( t->string(round(t, sigdigits=2)), xticks )
        
        ax.xticklabelcolor = :black 
        ax.xticks = (xticks, xticklabels)
        ax.xlabel = "Period [days]"
        ax.xticklabelrotation = 45.0
        if (j == 1) ax.ylabel = "PSD [MW²]" end
        if (i != ny)  hidexdecorations!(ax) end
    end

    return fig
end