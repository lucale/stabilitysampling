
function plot_power_flow(B::SparseMatrixCSC, x::Vector{<:Real}, y::Vector{<:Real}, p::Vector{<:Real}, f::Vector{<:Real})

    p_lims = maximum(abs.(extrema(p))); p_lims = (-p_lims, p_lims)
    fabs_lims = (0., maximum(abs.(f)))
    cmap_p = [:darkblue, :blue, :skyblue, :beige, :tomato,  :red, :darkred]
    cmap_f = [:darkseagreen1, :palegreen, :chartreuse2, :forestgreen, :darkgreen]
    #cmap_f = [:springgreen1, :lime, :limegreen, :seagreen, :darkgreen]

    layout = (x,y)
    fig = Figure(size=(550, 450))
    #ax = Axis(fig[1,1])
    ax = geo_ax(fig[1,1], x,y)
    u = getindex.(argmax(B, dims=1)[1,:], 1)
    v = getindex.(argmin(B, dims=1)[1,:], 1)
    for l=1:size(B,2)
        xu = layout[1][ u[l] ]
        xv = layout[1][ v[l] ]
        yu = layout[2][ u[l] ]
        yv = layout[2][ v[l] ]
        linesegments!( [xu,xv], [yu,yv], color=abs(f[l]), colormap=cmap_f, colorrange=fabs_lims, linewidth=2)
    end

    mx = ( layout[1][u] .+ layout[1][v] ) ./ 2
    my = ( layout[2][u] .+ layout[2][v] ) ./ 2
    dx = layout[1][u] .- layout[1][v]
    dy = layout[2][u] .- layout[2][v]
    r = atan.(dx, -dy) .+ ( f .<= 0.0 ) .* π
    scatter!(mx, my, color=abs.(f), colormap=cmap_f, colorrange=fabs_lims, markersize=10, marker=:utriangle, rotation=r)

    #node_plt = scatter!(ax,layout[1], layout[2], color=:black, markersize=10)
    node_plt = scatter!(ax,layout[1], layout[2], color=:black, markersize=11)
    node_plt = scatter!(ax,layout[1], layout[2], color=p, colormap=cmap_p, colorrange=p_lims, markersize=10)
    Colorbar(fig[1,2], colormap=cmap_p, colorrange=p_lims,label="Power [MW]")
    Colorbar(fig[1,3], colormap=cmap_f, colorrange=fabs_lims, label="Flow [MW]")

    return fig
end


function plot_relative_power_flow(B::SparseMatrixCSC, x::Vector{<:Real}, y::Vector{<:Real}, p::Vector{<:Real}, f::Vector{<:Real}, pmin::Vector{<:Real}, pmax::Vector{<:Real}, c::Vector{<:Real})

    p_rel = (p .- pmin) ./ (pmax .- pmin)
    f_rel = abs.(f) ./ c

    f_max = maximum(f_rel)
    has_line_failure = (f_max > 1.0)
    Cred = minimum( c .- abs.(f))

    cmap_f_fail = [:tomato, :red, :darkred]
    cmap_f_safe = [:gray90, :gray85, :gray80, :gray75, :gray70, :gray50, :gray30, :gray15]
    cmap_p = [:cyan2, :cyan, :lightcyan2, :lightcyan, :beige,:beige, :mistyrose1, :mistyrose2, :lightsalmon, :salmon2]

    
    layout = (x,y)
    fig = Figure(size=(550, 450))
    ax = geo_ax(fig[1:2,1], x,y)
    ax.title = "Redundant capacity Cred = $(round(Cred, sigdigits=6)) MW"
    u = getindex.(argmax(B, dims=1)[1,:], 1)
    v = getindex.(argmin(B, dims=1)[1,:], 1)
    for l=1:size(B,2)
        xu = layout[1][ u[l] ]
        xv = layout[1][ v[l] ]
        yu = layout[2][ u[l] ]
        yv = layout[2][ v[l] ]
        colormap = (f_rel[l] > 1.0) ? cmap_f_fail : cmap_f_safe
        colorrange = (f_rel[l] > 1.0) ? (1.0, f_max) : (0.0, 1.0)  
        linewidth = (f_rel[l] > 1.0) ? 3 : 1.5
        linesegments!( [xu,xv], [yu,yv]; color=f_rel[l], colormap, colorrange, linewidth)       
    end

    mx = ( layout[1][u] .+ layout[1][v] ) ./ 2
    my = ( layout[2][u] .+ layout[2][v] ) ./ 2
    dx = layout[1][u] .- layout[1][v]
    dy = layout[2][u] .- layout[2][v]
    r = atan.(dx, -dy) .+ ( f .<= 0.0 ) .* π
    failed_lines = f_rel .> 1.0
    scatter!(mx[failed_lines], my[failed_lines], color=f_rel[failed_lines], colormap=cmap_f_fail, colorrange=(1.0, f_max), markersize=8, marker=:utriangle, rotation=r[failed_lines])
    scatter!(mx[.!failed_lines], my[.!failed_lines], color=f_rel[.!failed_lines], colormap=cmap_f_safe, colorrange=(0.0, 1.0), markersize=8, marker=:utriangle, rotation=r[.!failed_lines])
    
    node_plt = scatter!(ax,layout[1], layout[2], color=:black, markersize=11)
    node_plt = scatter!(ax,layout[1], layout[2], color=p_rel, colormap=cmap_p, colorrange=(0.0, 1.0), highclip=:darkred, lowclip=:darkblue, markersize=10)
    Colorbar(fig[1:2,2], colormap=cmap_p, highclip=:darkred, lowclip=:darkblue, colorrange=(0.0, 1),label="Injection relative to operational bounds")
    if has_line_failure
        Colorbar(fig[1,3], colormap=cmap_f_fail, colorrange=(1, f_max), label="Relative Overload")
        Colorbar(fig[2,3], colormap=cmap_f_safe, colorrange=( 0, 1), label="Relative line saturation")
    else
        Colorbar(fig[1:2,3], colormap=cmap_f_safe, colorrange=( 0, 1), label="Relative line saturation")
    end
    return fig
end 