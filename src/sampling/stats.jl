
function clean_chain(chain::Vector, burnin::Float64 = 0.5, thinning::Float64 = 0.75)
    n_chain = length(chain)
    n_burnin = floor(Int, burnin * n_chain) 
    idx_thinning = round.(Int, range(n_burnin, n_chain, length=max(2, ceil(Int, (1-thinning) * (n_chain - n_burnin) )) ) )
    return chain[idx_thinning]
end

function clean_chain(chain::Matrix, burnin::Float64 = 0.5, thinning::Float64 = 0.75)
    n_chain = size(chain, 1)
    n_burnin = floor(Int, burnin * n_chain)
    idx_thinning = round.(Int, range(n_burnin, n_chain, length=max(2, ceil(Int, (1-thinning) * (n_chain - n_burnin) )) ) )
    return chain[idx_thinning, :]
end


# statistics of chains
function autocor(x::Vector{T}, lag::Int) where {T <: Real}
    n = length(x); μ = mean(x) ; σ² = std(x)^2
    ac = mean( (x[1+lag:end] .- μ) .* (x[1:end-lag] .- μ) ) ./ σ²
    return ac
end
function autocor(x::Vector{T}, lags::Vector{<:Integer}=collect(1:length(x)-1)) where {T<:Real}
    n = length(x); μ = mean(x) ; σ² = std(x)^2
    return map( lag -> mean( (x[1+lag:end] .- μ) .* (x[1:end-lag] .- μ) ) ./ σ², lags)
end
