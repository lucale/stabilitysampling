
function rce_rwm(logp_background::Lp_b, logp_target::Lp_t, 
    n_sample::Int, x0::Vector, β::Real = -1e-3;
        save_every::Int = 1,
        lower_bounds = fill(-Inf, length(x0)), 
        upper_bounds = fill(Inf, length(x0)),
        rng::AbstractRNG = Random.default_rng(), 
        stepsize = ones(length(x0)),
        proposal!::Function=randn!,
        progress = Progress(n_sample)
    ) where {Lp_b <: Function, Lp_t <: Function}

    # Evaluate log posterior for first sample
    x0 = clamp.( copy(x0), lower_bounds, upper_bounds)
    lpb_0 = logp_background(x0)
    lpt_0 = logp_target(x0)
    lp_0 = lpb_0 + β * lpt_0

    # Allocate storage for the samples
    storage_length = n_sample ÷ save_every + (n_sample % save_every > 0 ? 1 : 0)
    X = zeros(eltype(x0), storage_length, length(x0))
    accepted = zeros(Bool, storage_length)
    lp_target = zeros(Float64, storage_length)
    lp_background = zeros(Float64, storage_length)

    # Save the first sample
    X[1, :] .= copy(x0)
    lp_target[1] = lpt_0
    lp_background[1] = lpb_0
    accepted[1] = true

    # Allocate state storage 
    x = copy(x0) ; δx = zeros(length(x0))
    for k = 2:n_sample

        # propose a new sample
        δx .= stepsize .* proposal!(rng, δx)  
        y = x .+ δx
        
        # ensure that proposal is within box 
        v = min.(0,  y .- lower_bounds) .+ max.(0, y .- upper_bounds)
        y = clamp.( y .- 2 * v, lower_bounds, upper_bounds)

        # evaluate proposal
        lpb = logp_background(y)
        lpt = logp_target(y)
        lp = lpb + β * lpt

        # standard Metropolis-Hastings accept/reject step
        α = min(0.0, lp - lp_0)
        accept = log(rand(rng)) ≤ α
        if accept
            x .= copy(y)
            lpb_0 = lpb
            lpt_0 = lpt
            lp_0 = lp
        end

        # Save the sample
        if (k-1) % save_every == 0
            idx = (k-1) ÷ save_every + 1
            accepted[ idx ] = accept        
            X[idx, :] .= copy(x)
            lp_target[idx] = lpt_0
            lp_background[idx] = lpb_0
        end

        # Update progress bar
        if !isnothing(progress) next!(progress) end
    end
    chain = (; X,
                background = lp_background, 
                target = lp_target, 
                accepted = BitVector(accepted))
    return chain
end

#=
## Adaptive RCE RWM sampler
# Caveat : This sampler doesn't work well for non normal target distributions :'(
function adaptive_rce_rwm(logp_background::Lp_b, logp_target::Lp_t, 
    n_sample::Int, x0, β::Real = -1e-3;
        lower_bounds = fill(-Inf, length(x0)), 
        upper_bounds = fill(Inf, length(x0)),
        rng::AbstractRNG = Random.default_rng(), 
        proposal!::Function=randn!, 
        C::Matrix = Matrix(I(length(x0))), ε::Real = 1.0, #1e-2,
        progress = Progress(n_sample)
    ) where {Lp_b <: Function, Lp_t <: Function}

    s = (2.4)^2 / length(x0)

    # Evaluate log posterior for first sample
    x0 = clamp.(x0, lower_bounds, upper_bounds)
    lpb_0 = logp_background(x0)
    lpt_0 = logp_target(x0)
    lp_0 = lpb_0 + β * lpt_0
    x_mean = copy(x0)

    # Allocate storage for the samples
    X = zeros(eltype(x0), length(x0), n_sample)
    accepted = zeros(Bool, n_sample)
    lp_target = zeros(Float64, n_sample)
    lp_background = zeros(Float64, n_sample)

    # Save the first sample
    X[:, 1] .= x0
    # Evaluate log posterior for first sample
    x0 = clamp.(x0, lower_bounds, upper_bounds)
    lpb_0 = logp_background(x0)
    lpt_0 = logp_target(x0)
    lp_0 = lpb_0 + β * lpt_0

    # Allocate storage for the samples
    X = zeros(eltype(x0), length(x0), n_sample)
    accepted = zeros(Bool, n_sample)
    lp_target = zeros(Float64, n_sample)
    lp_background = zeros(Float64, n_sample)

    # Save the first sample
    X[:, 1] .= x0
    lp_target[1] = lpt_0
    lp_background[1] = lpb_0
    lp_target[1] = lpt_0
    lp_background[1] = lpb_0

    C = Matrix(diagm((upper_bounds .- lower_bounds) ./ 1_000_000 ))

    println("RCE β = $β chain of length $n_sample with RWM sampler")
    @time for k = 2:n_sample
        x = X[:,k-1]

        # propose a new sample
        δx = zeros(length(x)); 
        proposal!(rng, δx)  
        y = x + C * δx
        

        # ensure that proposal is w .*ithin box 
        v = min.(0,  y .- lower_bounds) .+ max.(0, y .- upper_bounds)
        #println( "Distance to bounds " , sum(v .^ 2) )
        y = y .- 2 * v
        y = clamp.(y, lower_bounds, upper_bounds)

        lpb = logp_background(y)
        lpt = logp_target(y)
        lp = lpb + β * lpt

        # standard Metropolis-Hastings accept/reject step
        α = min(0.0, lp - lp_0)
        if log(rand(rng)) ≤ α
            accepted[k] = true
            x = y
            lpb_0 = lpb
            lpt_0 = lpt
            lp_0 = lp
        end

        # Save the sample
        X[:,k] = x
        lp_target[k] = lpt_0
        lp_background[k] = lpb_0

        # Update the covariance
        C = (k - 1)/k .* C + s .* x_mean * x_mean'
        x_mean = (k-1) / k .* x_mean .+ 1 / k .* x  
        C = C .+ s / k .* ( - (k+1) .* x_mean * x_mean' .+ x * x' .+ ε .* I(length(x))) 

        # Update progress bar
        next!(progress)
    end
    println("Trained C ", C)
    println("Acceptance rate ", round(100 .* mean(accepted), sigdigits=4), " % ")
    chain = RCERWMChain(Matrix(X'), lp_background, lp_target, β, BitVector(accepted))
    return chain
end
=#