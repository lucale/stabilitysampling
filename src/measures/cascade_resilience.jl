using SparseArrays
using LinearAlgebra
using IterativeSolvers
using EmbeddedGraphs
using IterTools

#redundant_capacity(g::LinearPowerGrid, f) = redundant_capacity( g.capacities, f )

function reduce_edge!(
        B::SparseMatrixCSC, S::BitVector, c::AbstractVector, k::AbstractVector, edge::Integer; 
        δc_abs = 0.0, δc_rel = 0.0, δk_abs = k[edge]/c[edge]*δc_abs, δk_rel = δc_rel
    )

    # reduce capacity and coupling
    c[edge] = max(0, c[edge] * (1-δc_rel) - δc_abs )
    k[edge] = max(0, k[edge] * (1-δk_rel) - δk_abs )

    # remove completely if overloaded  
    if c[edge] == 0 || k[edge] == 0
        c[edge] = 0
        k[edge] = 0
        B.nzval[B.colptr[edge]] = 0
        S[edge] = false
    end
end

function reduce_edge!(B::SparseMatrixCSC, S::BitVector, c::AbstractVector, k::AbstractVector, edges::AbstractVector{<:Integer}; kwargs...)
    for edge in edges
        reduce_edge!(B,S,c,k,edge; kwargs...)
    end
end

function cascade_failure!(B::SparseMatrixCSC, S::BitVector, c::AbstractVector, k::AbstractVector, p::AbstractVector; kwargs...)
    f = get_flow_matrix(B, k) * p
    overloaded = abs.(f) > c 
    
    for edge in findall(overloaded .&& S)
        reduce_edge!(B, S, c, k, edge; kwargs...)        
    end
    return any(overloaded), f
end

function cascade_resilience(
        B::SparseMatrixCSC, c::AbstractVector, k::AbstractVector, p::AbstractVector;
        m::Integer = 1, attack = (δc_abs = 100.0,), react = (δc_rel=1.0,),
        measure::Function = (has_cascade,s,c,k,f) -> has_cascade ? length(s) - sum(s) : redundant_capacity(c, f) 
    )
    
    hasmethod(measure, (Bool, BitVector, Vector, Vector, Vector) ) || @warn "Method signature of measure invalid."


    # save initial values
    B_initial = B
    c_initial = c
    k_initial = k
    S_initial = BitVector(ones(size(B,2)))

    # loop over subsets 
    line_subsets = subsets(1:size(B,1), m)
    q = zeros(length(line_subsets))
    for (i,line_subsets) in enumerate(line_subsets)
        
        # reset to initial
        B = copy(B_initial)
        c = copy(c_initial)
        k = copy(k_initial)
        S = copy(S_initial)

        # attack 
        reduce_edge!(B, S, c, k, line_subsets; attack...)
        
        # cascade 
        failing, f = cascade_failure!(B,S,c,k,p; react...)
        has_cascade = failing 
        while any(S) && failing  
            failing, f = cascade_failure!(B,S,c,k,p; react...)
        end

        # evaluate quantity 
        q[i] = measure(has_cascade,S,c,k,f)
    end
    return mean(q)
end
#cascade_resilience(measure::Function, args... ; kwargs...) = cascade_resilience(args... ; measure, kwargs...)

"""
    cascade_resilience(g::LinearPowerGrid, p::Vector; [measure], [m], [attack], [react] )

Evaluate the resilience of the `measure` to a failure cascade resulting from attacking `m` edges. 

The measure should have the following signature
```
    measure(
        has_cascade::Bool, stable_edges::BitVector, 
        capacity::Vector, coupling::Vector, flow::Vector
    ) :: Real
```

The `attack`` and `react` parameters should be named tuples that are fed into the `reduce_edge!` function.

"""
function cascade_resilience(n::Network, p::AbstractVector; kwargs...)
    B = - incidence_matrix( get_graph(n), oriented=true )
    cascade_resilience(B, get_capacities(n), get_couplings(n), p)
end
#=
function cascade_resilience(l::LinearPowerGrid, p::AbstractVector; kwargs...)
    B = - incidence_matrix(l.graph, oriented=true)
    cascade_resilience(B, l.capacities, l.couplings, p; kwargs...)
end
=#