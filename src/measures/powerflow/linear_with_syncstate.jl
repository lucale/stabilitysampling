function linear_params(n::Network, p::AbstractVector{<:Real}; 
    usync = synchronous_state(n, p))

    m_inv = 1 ./ get_inertias(n)
    d = get_dampings(n)
    k = get_couplings(n)
    B = -incidence_matrix(Graph(n), oriented=true)
    
    # correct linear parameters for non-linear synchronous state
    ϕ0 = usync[1:nv(n)] # assuming synchronous state has been found 
    k = k .* cos.( B' * ϕ0 )
    p = p .- mean(p) .- B * diagm(k) * sin.( B' * ϕ0 )
    return m_inv,d,k,B,p,usync
end

function linear_system_matrix(n::Network, p::AbstractVector{<:Real}; 
    params = linear_params(n, p))

    N = nv(n)
    m_inv,d,k,B,p,usync = params

    # initialize linear system matrix A
    A = zeros(Float64, 2N, 2N)    

    # diagonal blocks
    A[1:N,1:N] .= zeros(N,N)
    for i=1:N
        A[i,N+i] = 1.0
        A[N+i,N+i] = - d[i] / m[i]
    end
    A[ N+1:2N, 1:N ] .= - diagm( m_inv ) * B * diagm(k) * B'
    
    return A
end


# we call F = KBᵀ(BKBᵀ)⁻¹ the flow matrix (then f = F * (p-p₀) ) 
function get_flow_matrix(n::Network, p::AbstractVector{<:Real}; 
    params = linear_params(n, p)) 

    # parameters
    m_inv,d,k,B,p,usync = params

    K = sparse( diagm(k) )
    KBt = K * B'
    F = KBt * pinv( Matrix(B * KBt) )
    return F 
end


function redundant_capacity(c::AbstractVector{<:Real}, f::AbstractVector{<:Real})
    mismatch = c .- abs.(f)
    return minimum(mismatch)
end 

function redundant_capacity(c::AbstractVector{<:Real}, f::AbstractMatrix{<:Real})
    mismatch = c' .- abs.(f)
    overloaded = any(mismatch .< 0, dims=2)[:,1]
    return minimum(mismatch, dims=2)[:,1]
end

function redundant_capacity(n::Network, p::Union{AbstractVector{<:Real}, AbstractMatrix{<:Real}})
    c = get_capacities(n)
    f = get_flow(n, p)
    return redundant_capacity(c, f)
end