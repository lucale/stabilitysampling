function get_capacities(n::Network, safety_margin=0.1)
    lines = get_lines(n)
    
    c = n.lines.s_nom_opt[ indexin(lines, n.lines.Line) ]
    return c .* (1.0 + safety_margin)
    

    #=
    p_t = get_net_injection_timeseries(n)
    u0_t = syncronous_state(n, p_t)
    F = get_flow_matrix(n)
    f_t = p_t * F'
    
    return maximum(abs.(f_t), dims=1)[1,:] .* (1.0 + safety_margin)
    =#
end


function get_couplings(n::Network)
    lines = get_lines(n)
    x = n.lines.x[ indexin(lines, n.lines.Line) ]
    return 380^2  ./ x ## voltage units 
end

function get_inertias(n::Network)
    ω_grid = 2π * 50.0 # Hz
    I_0 = 4e-2 # MW/s

    relative_inertia = Dict(
        "nuclear" => 7.0,
        "coal" => 4.0,
        "lignite" => 4.0,
        "CCGT" => 4.0,
        "OCGT" => 4.0,
        "oil" => 3.0,
        "biomass" => 2.0,
        "onwind" => 2.0,
        "offwind-ac" => 2.0,
        "offwind-dc" => 2.0,
        "solar" => 1.0,
        "ror" => 2.0,
    )

    buses = get_buses(n)
    inertias = zeros(length(buses))
    generator_buses = indexin(n.generators.bus, buses)
    for (u,bus) in enumerate(buses)
        mask = generator_buses .== u
        p_per_carrier = n.generators.p_nom_opt[mask]
        p_total = sum(p_per_carrier) 
        carriers = n.generators.carrier[mask]
        weights = map(c->relative_inertia[c], carriers)
        inertias[u] += sum( weights .* p_per_carrier ./ p_total )
    end
    
    return 2 .* inertias ./ ω_grid 
end
 
function get_dampings(n::Network)
    ω_grid = 2π * 50.0 # Hz
    buses = get_buses(n)
    dampings = .5 .* ones(length(buses))
    return dampings ./ ω_grid
end



