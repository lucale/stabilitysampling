
function linear_system_eigspace(n; 
    u0 = zeros(2*nv(n)),
    A = linear_system_matrix(n; usync)) 
    λ,W = eigen(A)
    W_inv = inv(W)
    return λ,W,W_inv
end

function linear_system_pinv(n::Network; 
    usync = synchronous_state(n, p),
    eigA = linear_system_eigspace(n; usync))

    λ,W,W_inv  = eigA
    i0 = argmin(abs.(λ)) # find eigenvalue closest to zero (=0 except for numerical errors)
    λ_pinv = vcat( 1 ./ λ[1:i0-1], 0.0, 1 ./ λ[i0+1:end] )
    A_pinv = W * diagm(λ_pinv) * W_inv
    return A_pinv
end

#=
function linear_synchronous_state(n::Network, p::AbstractVector{<:Real})
    B = -incidence_matrix(Graph(n), oriented=true)
    k = get_couplings(n)
    d = get_dampings(n)
    ω_grid = 2π * 50.0

    L = B * diagm(k) * B'
    p = p .- ω_grid .* d
    ϕ = ( pinv(L) * p ) ./ d 
    return vcat(ϕ, zeros(nv(n)))
end
=#

function linear_solution(n::Network, p::AbstractVector{<:Real}, 
    u0::AbstractVector{<:Real}, t::Real = 10.0;
    eigA = linear_system_eigspace(n))
    
    # constant term
    m_inv, d, k, B, p = linear_params(n, p)
    b = vcat(zeros(nv(n)), m_inv .* p )

    # eigen decomposition
    λ,W,W_inv = eigA

    # pseudo inverse
    i0 = argmin(abs.(λ)) 
    λ_pinv = vcat( 1 ./ λ[1:i0-1], 0.0, 1 ./ λ[i0+1:end] )
    A_pinv = W * diagm(λ_pinv) * W_inv
        
    # evaluate solution
    expλt = exp.(λ .* t)
    u_init = W * diagm( expλt ) * W_inv * u0  
    u_force = W * diagm( λ_pinv .*  ( expλt .- 1.0 ) ) * W_inv * b  
    u = real.(u_init .+ u_force)
    return u
end

function sync_norm(n::Network, p::AbstractVector{<:Real}, u0::AbstractVector{<:Real}; 
    eigA = linear_system_eigspace(n))
    m = get_inertias(n)
    d = get_dampings(n)
    N = nv(n)
    
    # constant term
    ω_grid = 2π * 50.0
    p = p .- ω_grid .* d
    b = vcat(zeros(nv(n)), p ./ m )

    # eigen decomposition
    λ,W,W_inv = eigA

    CᵀC =  zeros(2N, 2N) 
    CᵀC[1+N:2N , 1+N:2N] .= ( diagm(ones(N)) .- ones(N,N) ./ N ) ./ N


    

    # pseudo inverse

    ϵ = 1e-6

    W2ᵀCᵀCW2 = W2' * CᵀC * W2
    W2ᵀb_λ2 = (W2' * b) ./ λ2 
    condition2 = abs( W2ᵀb_λ2' * W2ᵀCᵀCW2 * W2ᵀb_λ2 ) < ϵ
    if !condition2 return Inf end

    # lyapunov equation
    v = W2' * u0 + W2ᵀb_λ2
    X = lyapc( diagm(λ2), W2ᵀCᵀCW2)

    W2ᵀv_λ2 = ( W2 * v ) ./ λ2
    
    S² = v' * X * v +  W2ᵀb_λ2' * W2ᵀCᵀCW2 * W2ᵀv_λ2
    return real(S²)
end