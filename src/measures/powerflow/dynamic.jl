function swing_ode_params(n::Network, p::AbstractVector{<:Real})
    g = Graph(n)
    B = -incidence_matrix(g, oriented=true)
    k = get_couplings(n) 
    m_inv = 1 ./ get_inertias(n)
    d = get_dampings(n)
    p = p .- mean(p) # ignore synchronization frequency, so ∑p = 0
    return (; m_inv, d, k, B, p)
end

function swing_dynamics!(du, u, params, t)
    @unpack m_inv,d,k,B,p = params
    N = length(u) ÷ 2

    # extract state
    @views begin 
        ϕ = u[1:N]
        ω = u[N+1:2N]
        dϕ = du[1:N]
        dω = du[N+1:2N]
    end

    # swing equation 
    dϕ  .= ω
    dω  .= m_inv .* ( - d .* ω .- B * ( k .* sin.( B' * ϕ ) ) .+ p )
    return du
end

# Find the synchronous state, ie. steady state of the swing equation
function syncstate_loss(u, params)
    N = length(u) ÷ 2
    dₜu = zeros(eltype(u), length(u))
    swing_dynamics!(dₜu, u, params, 0.0)
    dₜu[1:N] .-= mean(dₜu[1:N])
    return dₜu[2:end]
end
function synchronous_state(n::Network, p::AbstractVector{<:Real}; 
    params = swing_ode_params(n, p),
    initial_guess = zeros(2*nv(n))) 

    # apply Newton solver 
    problem = NonlinearProblem(syncstate_loss, initial_guess, params)
    solution = solve(problem, NewtonRaphson())
    if solution.retcode != :success
        println("Synchronous state solver not converged.")
        println("Sol ", solution.u)
        return nothing
    end
    usync = solution.u

    # check if synchronous state is found
    ω² = sum( usync[nv(n)+1:end].^2 )
    if ω² > 1.0 
        println("Found unsteady state.")
        return nothing
    end
 
    return usync
end

# Solve the swing equation numerically
swing_ode_problem(n::Network, p::AbstractVector{<:Real}, 
    u0::AbstractVector{<:Real}; 
    params = swing_ode_params(n, p),
    t_final::Real=120.0
) = ODEProblem(swing_dynamics!, u0, (0,t_final), params)


function solve_swing_ode(n::Network, p::AbstractVector{<:Real}, 
    u0::AbstractVector{<:Real}; 
    params = swing_ode_params(n, p),
    t_final::Real=120.0, 
    save_trajectory=false, solver_args...)

    # construct and solve the problem
    prob = swing_ode_problem(n, p, u0; t_final)
    sol = solve(prob, Tsit5(); solver_args...)
    
    # return the solution
    if save_trajectory
        return sol.t, stack(sol.u, dims=1)
    else
        return sol.u[end]
    end
end

#=
function build_ensemble_problem(g::AbstractGraph, k::AbstractVector, d::AbstractVector, m::AbstractVector, p::AbstractVector; 
    u0 = synchronous_state(g, p), perturbation::Matrix{<:Real} )

    prob = build_ode_problem(g,k,d,m,p; u0)
    ensembleprob = EnsembleProblem()
    for i in size(perturbation,1)
        #prob. #...
    end
end
=#

