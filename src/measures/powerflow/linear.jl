function linear_params(n::Network, p::AbstractVector{<:Real}) 

    m_inv = 1 ./ get_inertias(n)
    d = get_dampings(n)
    k = get_couplings(n)
    B = -incidence_matrix(Graph(n), oriented=true)
    return m_inv,d,k,B,p,usync
end

function linear_system_matrix(n::Network)

    # parameters
    N = nv(n)
    m_inv = 1 ./ get_inertias(n)
    d = get_dampings(n)
    k = get_couplings(n)
    B = -incidence_matrix(Graph(n), oriented=true)

    # initialize linear system matrix A
    A = zeros(Float64, 2N, 2N)    

    # diagonal blocks
    #A[1:N,1:N] .= zeros(N,N)
    A[1:N,N+1:2N]    .= diagm( ones(N) )
    A[N+1:2N,N+1:2N] .= - diagm( d .* m_inv )
    A[ N+1:2N, 1:N ] .= - diagm( m_inv ) * B * diagm(k) * B'
    
    return A
end


# we call F = KBᵀ(BKBᵀ)⁻¹ the flow matrix (then f = F * (p-p₀) ) 
function get_flow_matrix(n::Network) 

    # parameters
    k = get_couplings(n)
    B = -incidence_matrix(Graph(n), oriented=true)

    K = sparse( diagm(k) )
    KBt = K * B'
    F = KBt * pinv( Matrix(B * KBt) )
    return F 
end

function get_power_matrix(n::Network)
    B = -incidence_matrix(Graph(n), oriented=true)
    return B'
end


function redundant_capacity(c::AbstractVector{<:Real}, f::AbstractVector{<:Real})
    mismatch = c .- abs.(f)
    return minimum(mismatch)
end 

function redundant_capacity(c::AbstractVector{<:Real}, f::AbstractMatrix{<:Real})
    mismatch = c' .- abs.(f)
    overloaded = any(mismatch .< 0, dims=2)[:,1]
    return minimum(mismatch, dims=2)[:,1]
end

function redundant_capacity(n::Network, p::Union{AbstractVector{<:Real}, AbstractMatrix{<:Real}})
    c = get_capacities(n)
    f = get_flow(n, p)
    return redundant_capacity(c, f)
end