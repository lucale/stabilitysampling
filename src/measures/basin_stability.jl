

function synchronous_phase_coherence(n::Network, p::AbstractVector{<:Real}; 
    syncstate::AbstractVector{<:Real} = synchronous_state(n, p) )

    edgs = collect( edges( get_graph(n) ) )
    u = src.(edgs); v = dst.(edgs)
    return mean( cos.( syncstate[u] .- syncstate[v] ) )
end



function basin_stability( n::Network, p::AbstractVector{<:Real}; 
        n_sample = 1, t_final = 20.0, tolerance=0.1, threshold=50.0,
    )

    N = length(p)
    params = swing_ODE_params(n,p)
    print("Calculating syncstate ")
    @time syncstate = synchronous_state(n, p; params)
    
    perturbations = rand(n_sample, 2N)

    # create ensemble problem
    prob = swing_ODE_problem(n, p, syncstate; t_final)
    function prob_func(ode, i, repeat) 
        ode.u0 .+= perturbations[i, :]
        println(" - Started prob $i")
        return ode
    end
    function output_func(sol, i)
        println(" - Solved prob $i" )
        ω = sol.u[end][N+1:2N]
        has_converged = all( abs.(ω .- syncstate[N+1:2N]) .< tolerance )
        return ( has_converged , false) # don't rerun
    end 
    
    function break_condition(u, t, integrator)
        ω = u[N+1:2N]
        has_converged = all( abs.(ω .- syncstate[N+1:2N]) .< tolerance )
        has_desynced = any( abs.(ω .- syncstate[N+1:2N]) .> threshold )
        return has_converged || has_desynced
    end
    cb = DiscreteCallback(break_condition, terminate!)

    #=
    function reduction_func(u, data, I)
        append!(u, data)
        println("I ", I)
        println("u ", u)
        println("data ", data)
        ω0 = u[1][N+1:2N]
        ω = u[end][N+1:2N]
        has_converged = all( abs.(ω .- ω0) .< tolerance )
        has_desynced = any( abs.(ω .- ω0) .> threshold )
        return u, has_converged || has_desynced 
    end
    =#
    ensembleprob = EnsembleProblem(prob; output_func, prob_func)

    println("Solving ensemble problem")
    solver = RK4(); solver_args = (; reltol=1e-2, abstol=1.0, adaptive = true, callback=cb)
    sol = solve(ensembleprob, solver, EnsembleThreads(); trajectories=n_sample, solver_args...)
    return mean(sol.u) 
end
