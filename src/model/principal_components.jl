
"""
    principal component analysis (PCA)
"""
struct PCA{T <: Real} 
    signal::Matrix{T}
    mean::Vector{T}
    covar::Matrix{T}
    eigvals::Vector{T}
    eigvecs::Matrix{T}
    relevance::Vector{T}
end


function _size_check(pca::PCA)
    spatial_check = size(pca.signal, 2) == length(pca.mean) == size(pca.covar, 1) == size(pca.covar, 2) == size(pca.eigvecs, 1) 
    eigenspace_check = size(pca.eigvecs, 2) == length(pca.eigvals) == length(pca.relevance)
    return spatial_check && eigenspace_check    
end
Base.length(pca::PCA) = length(pca.eigvals)

"""
    PCA(x::Matrix; relevance=1.0) -> pca::PCA

    Perform principal component analysis on `x` with a relevance level of `relevance`
    meaning that the dimension is limited while ensuring that still this fraction of the variance is explained.
"""
function PCA(x::Matrix)

    # covariance matrix
    m = mean(x, dims=1)
    y = x .- m
    C = y' * y
    
    # diagonalize
    λ,V = eigen(C,sortby=x->-abs(x))
    λ = sqrt.(abs.(real.(λ)))

    # flip so that tendency towards correlated with +1.0 
    c = y * V' 
    stochastic_flip = mean(c, dims=1)[1,:] .< 0
    V[:, stochastic_flip] .*= -1

    r = cumsum( λ ./ sum(λ) )
    return PCA(x, m[1,:], C, λ, V, r)
end
function Base.show(io::IO, pca::PCA)
    println(io, "PCA with dimension $(length(pca))")
    for ratio = [0.5, 0.75, .95]
        println(io, "  $(ratio*100)% variance explained by $(relevant_dims(pca, ratio)) components ")
    end
end

"""
    relevant_dims(pca::PCA, relevance_level=0.95) -> dims::Integer

    Return the number of relevant dimensions to explain `relevance_level` of the variance.
"""

relevant_dims(pca::PCA, relevance_level=0.95) = relevance_level >= 1 ? length(pca) : findfirst(pca.relevance .>= relevance_level)

""" 
    transform(pca::PCA, θ::Vector; relevance=1.0) -> x::Vector
    transform(pca::PCA, θ::Vector; relevance=1.0, dims::Integer) -> x::Vector
    
    Transform `θ` to `x`, optionally limit to first `dims` principal components or `relevance` level.
"""
function transform(pca::PCA, θ::Vector{<:Real})
    dims = min(length(pca), length(θ))
    return pca.mean .+ pca.eigvecs[:,1:dims] * θ[1:dims]
end
function transform(pca::PCA, θ::Matrix{<:Real})
    dims = min(length(pca), size(θ,2))
    return pca.mean' .+ θ[:, 1:dims] * pca.eigvecs[:, 1:dims]'
end 

"""
    project(pca::PCA, x::Vector; relevance=1.0) -> θ::Vector

    Project `x` to component space `θ`. Similar to Karhunen-Loève expansion.
    Note that the dimension of `x` must match the dimension of the original data. 
"""
project(pca::PCA, x::Vector{<:Real}; relevance=1.0, kwargs...) = (pca.eigvecs' * (x .- pca.mean) )[1:relevant_dims(pca, relevance)]
project(pca::PCA, x::Matrix{<:Real}; kwargs...) = stack([project(pca, x[i,:]; kwargs...) for i=1:size(x,1)], dims=1 )