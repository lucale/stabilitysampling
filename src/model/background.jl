# Komolgorov-Smirnov distance between data and known distribution
function ksdist(x::AbstractVector{T}, d::UnivariateDistribution) where T<:Real
    n = length(x)
    cdfs = cdf.(Ref(d), sort(x))
    δp = maximum((1:n) / n - cdfs)
    δn = -minimum((0:n-1) / n - cdfs)
    δ = max(δn, δp)
    #(n, δ, δp, δn)
    return δ
end


# compute supremum of differences between empirical cdfs.
function ksdist(x::AbstractVector{T}, y::AbstractVector{S}) where {T<:Real, S<:Real}
    n_x, n_y = length(x), length(y)
    all_values = [x; y]
    sort_idx = sortperm(all_values)
    δ_y = 1 / n_y
    δ_x = 1 / n_x
    δ = δp = δn = zero(δ_y)

    for i in 1:(n_x + n_y)
        if sort_idx[i] > n_x
            δ -= δ_y
        else
            δ += δ_x
        end

        # only update δp/δn if the value is about to change or at the last step.
        if i == n_x + n_y || all_values[sort_idx[i]] != all_values[sort_idx[i + 1]]
            if δ > δp
                δp = δ
            elseif δ < δn
                δn = δ
            end
        end
    end
    δ = max(δp, -δn)
    #(n_x, n_y, δ, δp, -δn)
    return δ
end   


#=
struct MvProduct{S<:ValueSupport} <: MultivariateDistribution{S}
    components::Vector{ Union{UnivariateDistribution{S}, MultivariateDistribution{S}} } 
end
Base.length(m::MvProduct) = sum( length.(m.components) )
=#


function background_igmm(pca; dim_cutoff = 0.8, min_std=1.0)

    # principal injection components 
    α = project(pca, pca.signal)

    # bounds of coefficient time series 
    a = minimum(α, dims=1)[1,:]
    b = maximum(α, dims=1)[1,:]

    # fit distribution 
    model = UnivariateDistribution[]
    T,N = size(α)
    
    @showprogress for i=1:N

        # default truncated normal
        #if #std(α[:,i]) .<= min_std || i > 0.8 * N 
        if b[i] - a[i] .< min_std || std(α[:,i]) .<= min_std || i > dim_cutoff * N
            s = sqrt( T / (T-1) ) * std(α[:,i]) # empirical std, unbiased estimator
            g = Truncated( Normal(mean( α[:, i] ), s), a[i], b[i] )
            push!(model, g)
        
        else
            
            #Heuristics for Gaussian Mixture hyperparameters 
            # nmix : number of mixtures
            # Count peaks in Kernel Density Estimate   
            rel_peak_cutoff=0.001
            #kdist = kde(α[:,i]) #, boundary=(a[i], b[i]), bandwidth=(b[i]-a[i])/100) 
            kdist = kde(α[:,i], boundary=(a[i],b[i]), bandwidth=(b[i]-a[i])/150)
            kdist_dx = kdist.density[2:end] .- kdist.density[1:end-1]
            kdist_m = 0.5 .* ( kdist.density[1:end-1] .+ kdist.density[2:end]  )
            is_local_max = kdist_dx[1:end-1] .> 0.0 .&& kdist_dx[1:end-1] .* kdist_dx[2:end] .<= 0.0 
            push!(is_local_max, false)
            is_not_cutoff = kdist_m .> rel_peak_cutoff .* maximum(kdist.density)
            nmix_kde_peaks = sum(is_local_max .&& is_not_cutoff)    

            # Fit Truncated Gaussian Mixture Models with Expectation Maximization 
            try
                mvgmm = fit_gmm( Matrix( α[:, i]' ), nmix_kde_peaks, [ a[i] ], [ b[i]] ; cov=:diag, tol=1e-3); 
                gmm = MixtureModel( map( c -> Truncated( Normal(mean(c.normal)[1], sqrt(cov(c.normal)[1]) ), c.a[1], c.b[1] ), mvgmm.components) , mvgmm.prior )
                push!(model, gmm)
            catch e 
                # fallback to truncated normal
                s = sqrt( T / (T-1) ) * std(α[:,i]) 
                g = Truncated( Normal(mean( α[:, i] ), s ), a[i], b[i] )
                push!(model, g)
            end
        end

    end
    return Product([model...])
end


function background_mvgmm(pca::PCA, ncomp = 64; relevance = 0.8, maxiters = 10_000)

    # principal components
    x = pca.signal
    α = project(pca, x)

    # bounds of coefficient time series
    a = minimum(α, dims=1)[1,:]
    b = maximum(α, dims=1)[1,:]

    print("Fitting GMM ")
    nDim = relevant_dims(pca, relevance)
    
    io_buffer = IOBuffer()  
    logger = SimpleLogger(io_buffer) ; trials = 1
    ψ = nothing
    with_logger(logger) do
        gmm = GMM(ncomp, α[:, 1:nDim], kind=:diag)
        while sum(gmm.w .== 0) > 0 && trials < maxiters
            trials += 1
            gmm = GMM(ncomp, α[:, 1:nDim], kind=:diag)
        end
        ψ = MixtureModel(gmm)
    end
    @info "MvGMM $nDim dims, $ncomp components needed $trials iterations to converge (maxiters = $maxiters)"    
    
    
    return ψ
end

const MvGMM = MixtureModel{Multivariate, Continuous, <:Union{FullNormal, DiagNormal}, <:Distributions.Categorical{<:Real}}
function marginals(ψ::MvGMM)
    d = length(ψ); n = length(ψ.components)
    μ = mean.(ψ.components)
    Σ = cov.(ψ.components)
    R = inv.(Σ)

    m = MixtureModel[]
    for i=1:d
        idxX = [1:i-1 ... , i+1:d ... ]

        m_i = Normal[]
        for k=1:n
            μ_i = μ[k][i]
            R_ii = R[k][i, i]
            R_iX = R[k][i, idxX]
            R_XX = R[k][idxX, idxX]
            R_ii = R_ii - R_iX' * inv(R_XX) * R_iX
            σ_i = sqrt( 1/R_ii )
            N_k = Normal(μ_i, σ_i)
            push!(m_i, N_k)
        end
        m_i = MixtureModel( m_i, ψ.prior )
        push!(m, m_i)
    end
    return m
end

function marginals2D(ψ::MvGMM)
    d = length(ψ); n = length(ψ.components)
    μ = mean.(ψ.components)
    Σ = cov.(ψ.components)
    R = inv.(Σ)

    ms = []
    for i=1:d
        m = []
        for j=i+1:d
            μ_2 = map(μ -> μ[[i,j]], μ)
            R_22 = map( R -> R[ [i,j], [i,j] ], R )
            idx_1 = [1:i-1 ... , i+1:j-1 ... , j+1:d ... ]
            R_11 = map( R -> R[idx_1, idx_1], R )
            R_12 = map( R -> R[idx_1, [i,j]], R )
            R_2 = R_22 .- [ R_12[i]' * inv(R_11[i]) * R_12[i] for i=1:n ]
            Σ_2 = inv.( R_2 ); Σ_2 = .5 .* (Σ_2 .+ transpose.(Σ_2))
            m_ij = MixtureModel(MvNormal.(μ_2, Σ_2), ψ.prior.p)
            push!(m, m_ij)
        end
        push!(ms, m)
    end
    return ms
end

function conditional(ψ::MvGMM, i::Int, t::Real)
    d = length(ψ); n = length(ψ.components)
    μ = mean.(ψ.components)
    Σ = cov.(ψ.components)
    R = inv.(Σ)
    
    idX = [1:i-1 ... , i+1:d ... ]
    R_XX = map(R -> R[idX, idX], R)
    R_Xi = map(R -> R[idX, i], R)
    R_ii = map(R -> R[i,i], R)
    μ_i  = map(μ -> μ[i], μ)
    μ_X  = map(μ -> μ[idX], μ)
    
    R_XX_inv = inv.(R_XX); R_XX_inv = .5 .* (R_XX_inv .+ transpose.(R_XX_inv))   
    println( det.(R_XX))
    μ_cond = [ μ_X[k] .+ R_XX_inv[k] * R_Xi[k] * (t .- μ_i[k]) for k=1:n]
    return MixtureModel( MvNormal.(μ_cond, R_XX_inv), ψ.prior )
end