module StabilitySampling

using AdaptiveMCMC
using CairoMakie 
using Dates
using DataFrames
using DifferentialEquations
using Distributions
using FileIO
using FFTW
using GaussianMixtures
using GeoMakie
using Graphs
using GraphMakie
using HaltonSequences
using IterativeSolvers
using IterTools
using KernelDensity
using LinearAlgebra
using Logging: with_logger, NullLogger, SimpleLogger
using Makie
using MatrixEquations
using NonlinearSolve
using OrdinaryDiffEq
using Parameters
using Primes
using ProgressMeter
using Random
using Serialization
using SparseArrays
using Statistics 
using Base.Threads
using TruncatedGaussianMixtures


# (re-)export functions
import DataFrames: transform 
export transform
import Graphs: nv, ne, Graph, incidence_matrix
export Graph, nv, ne, incidence_matrix
import EmbeddedGraphs: EmbeddedGraph
export EmbeddedGraph 

## Power flow and swing dynamics

include("dynamics/powerflow.jl")
export flow_matrix
export redundant_capacity

include("dynamics/swingeq.jl")
export swing_dynamics!
export solve_trajectory
export survives, survivability

include("dynamics/syncstate.jl")
export synchronous_state
export phase_coherence

include("dynamics/syncnorm.jl")
export linear_system_matrix
export lyapunov_matrix
export linear_fit
export syncnorm
export linear_syncnorm
export linear_trajectory

## Statistical model 
include("model/principal_components.jl")
export PCA, relevant_dims, project, transform

include("model/background.jl")
export background_igmm
export background_mvgmm
export marginals, marginals2D
export conditional


## Sampling
include("sampling/chains.jl")
export rce_rwm, adaptive_rce_rwm

include("sampling/stats.jl")
export autocor
export clean_chain

## Plotting with Makie =============

include("makie_plots/geoax.jl")
export geo_ax, geo_limits
export countries, coastlines

include("makie_plots/shiftedpie.jl")
export shiftedpie, shiftedpie!

#=
include("makie_plots/production.jl")
export plot_yearly_production
export plot_production_capacities
=#

include("makie_plots/power_flow.jl")
export plot_power_flow
export plot_relative_power_flow

include("makie_plots/pairplot.jl")
export pairplot, pairplot!

include("makie_plots/pca.jl")
export plot_pca_eigvals
export plot_pca_pairplot
export plot_pca_modes
export plot_pca_coef_daytime
export plot_pca_coef_psd


end 
