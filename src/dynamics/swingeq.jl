function swing_dynamics!(du, u, params, t)
    @unpack B,k,p,d,m_inv = params
    N = length(u) ÷ 2

    # extract state
    @views begin 
        ϕ = u[1:N]
        ω = u[N+1:2N]
        dϕ = du[1:N]
        dω = du[N+1:2N]
    end

    # swing equation 
    dϕ .= ω
    dω .= m_inv .* ( - d .* ω .- B * ( k .* sin.(B' * ϕ) ) .+ p )
    return du
end


function solve_trajectory(
    B::SparseMatrixCSC, k::Vector{<:Real}, p::Vector{<:Real},
    u0::Vector{<:Real}; t::Real=15.0, 
    d::Vector{<:Real}=ones(size(B,1)), m_inv::Vector{<:Real}=ones(size(B,1)),
    solver=RK4(), 
    onlylast=false, progress=false, adaptive=true, kwargs... )
    
    params = (; B,k,p,d,m_inv )
    prob = ODEProblem(swing_dynamics!, u0, (0,t), params) 
    sol = solve(prob, solver; adaptive, progress, kwargs...)

    if onlylast 
        return sol.u[end] 
    else 
        t = sol.t; u = stack(sol.u, dims=1)
        return t,u
    end
end


function solve_trajectory(
    B::SparseMatrixCSC, k::Vector{<:Real}, p::Vector{<:Real},
    u0::Matrix{<:Real}; t::Real=15.0, 
    d::Vector{<:Real}=ones(size(B,1)), m_inv::Vector{<:Real}=ones(size(B,1)),
    solver=RK4(), abstol=1e-2, reltol=0.1,
    ensemble_solver = EnsembleThreads(),   
    onlylast=false, progress=true, adaptive=true, kwargs... )

    params = (; B,k,p,d,m_inv )
    prob1 = ODEProblem(swing_dynamics!, u0[1,:], (0,t), params) 
    
    emsemble_prob = EnsembleProblem(prob1, 
        prob_func = (prob, i, repeat) -> ( prob.u0 .= u0[i, : ]; prob ),
        output_func = onlylast ? (sol, i) -> (sol[end, :], false) : (sol, i) -> (sol, false),
    ) 
    sols = solve(emsemble_prob, solver, ensemble_solver; 
        trajectories = size(u0,1), adaptive, abstol, reltol, progress, kwargs...)
    u = map(sol -> stack(sol.u, dims=1), sols.u )
    t = map(sol -> sol.t, sols)
    return t,u
end

function survives(u::Vector{<:Real}, ε::Real = 1.0)
    ω = u[length(u) ÷ 2:end]
    return all( abs.(ω) .< ε )
end

function survives(u::Matrix{<:Real}, ε::Real = 1.0)
    ω = u[:, size(u,2) ÷ 2:end]
    return all( abs.(ω) .< ε, dims=2 )[:,1]
end

survivability(B,k,p,u0; kwargs...) = 
    survives(solve_trajectory(B,k,p,u0; kwargs...)[2])


#= 
# Linearized swing equation  ∂ₜu(t) = A u(t) 
#   with A = [   0    I   
#              -M⁻¹L -M⁻¹D ] 
#  where D = diag(d), M⁻¹ = diag(m_inv)
#    and L = B K diag(cos(Bᵀϕ₀)) Bᵀ
#
function linear_system_matrix(B::SparseMatrixCSC, k::Vector{<:Real}, 
    usync::Vector{<:Real} = zeros(2*size(B,1));
    d = ones(size(B,1)), m_inv = ones(size(B,1)) )
   
    N = size(B,1)
    L = B * diagm(k .* sin.(B' * usync[1:N])) * B'
    #L = B * diagm( k ) * B'
    Minv = diagm(m_inv)
    DMinv = diagm(m_inv .* d)
    A = [ spzeros(N,N) I(N) ; 
            -Minv * L  -DMinv ]
    return A

end


function linear_trajectory(B::SparseMatrixCSC, k::Vector{<:Real}, p::Vector{<:Real},
    u0::Vector{<:Real}, t::Union{<:Real, Vector{<:Real}}; 
    d::Vector{<:Real}=ones(size(B,1)), m_inv::Vector{<:Real}=ones(size(B,1)),
    usync::Vector{<:Real} = synchronous_state(B, k, p; d, m_inv) )


    N = size(B,1)
    #A = linear_system_matrix(B, k .* cos.(B' * usync[1:N]); d, m_inv)
    A = linear_system_matrix(B, k, usync; d, m_inv)
    λ,W = eigen(Matrix(A))
    W_inv = inv(W)

    
    δ0 = u0 .- usync
    v = W_inv * δ0

    mask = ones(length(λ))
    mask[ argmin( abs.(λ) ) ] = 0.0
    v[ argmin( abs.(λ) ) ] = 0.0

    δ = W * ( mask .* exp.(λ .* t')) .* v 
    u = real.( usync .+ δ )
    if typeof(t) <: Real 
        u = u[:,1]
    else
        u = Matrix(u') 
    end
    return u
end
=#