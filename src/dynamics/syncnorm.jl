function linear_fit(t::Vector{<:Real}, y::Vector{<:Real})
    A = stack([t, ones(length(t))], dims=2)
    AᵀA_inv = inv(A' * A) 
    x = AᵀA_inv * A' * y
    return x
end

function bounded_growth_loss!(du, u, p)
    t,y = p 
    C,λ = u
    expλt = exp.(-λ .* t)
    z  = C .* (1 .- expλt )
    d₁z = (1 .- expλt )
    d₂z = C .* λ .* expλt
    dz = hcat(d₁z, d₂z)
    du = sum( (y .- z) .^ 2 )
end
function fit_bounded_growth(t::Vector{<:Real}, y::Vector{<:Real})
    nkernel = 2
    k = pdf(Normal(0, 5.0), range(-nkernel,nkernel,2*nkernel+1) ); k = k ./ sum(k)   
    y = vcat( fill( y[1], nkernel), y, fill( y[end], nkernel) )
    y = [ dot(k, y[i:i+2*nkernel]) for i=1:length(y)-2*nkernel ]

    dy = y[2:end] .- y[1:end-1]
    dt = t[2:end] .- t[1:end-1]
    a,b = linear_fit( (t[1:end-1] .+ t[2:end]) ./ 2, log.(dy ./ dt)) 
    λ = -a; C = -exp(b) / a
    prob = NonlinearProblem(bounded_growth_loss!, [C,λ], [t,y])
    sol = solve(prob, NewtonRaphson(); maxiters=300, verbose=false)  
    C,λ = sol.u
    C = y[end] / (1 - exp(-λ * t[end])) # force interpolation condition at the end
    return C,λ
end

# Synchronization norm 
#   S² = ∫ 1/N ∑ᵢ ( ωᵢ(t) - 1/N ∑ⱼωⱼ(t) )² dt 
#       for i ∈ {1 … N}   from t = 0 to t → ∞
function syncnorm(t::Vector{<:Real}, u::Matrix{<:Real})
    N = size(u,2) ÷ 2
    ω = u[:, N+1:end]
    ω² = mean( ( ω .- mean(ω, dims=2) ) .^ 2, dims=2)[:,1]
    Δt  = t[2:end] .- t[1:end-1]
    tₘ = ( t[2:end] .+ t[1:end-1] ) ./ 2
    ω²ₘ = ( ω²[2:end] .+ ω²[1:end-1] ) ./ 2
    S² = cumsum( ω²ₘ .* Δt )
    C,λ = fit_bounded_growth(tₘ, S²)
    return C
end


#=
function lyapunov_matrix(B::SparseMatrixCSC, k::Vector{<:Real}, usync::Vector{<:Real}; d = ones(size(B,1)), m_inv=ones(size(B,1)))
    N = size(B,1)
    A = linear_system_matrix(B, k .* cos.(B' * usync[1:N]); d, m_inv)
    A = Matrix(A)
    CᵀC = zeros(2*N,2*N)
    CᵀC[N+1:2N,N+1:2N] .= 1 ./ N * I(N) #diagm( 1 ./ N  .* ones(N))
    λ,W = eigen(A)
    W_inv = inv(W)
    
    nz = abs.(λ) .> 1e-6
    X = zeros(2N, 2N)    

    P = diagm(λ[nz])
    Q = W[:,nz]' * CᵀC * W[:,nz]
    println("Solving Lyapunov equation")
    try 
        X_nz = lyapc(P, Q)
        X[nz,nz] .= X_nz
    catch e
        X .= NaN
    end  
    
    return X
end
=#

function grounded_jacobian(B::SparseMatrixCSC, k::Vector{<:Real}, usync::Vector{<:Real}; d = ones(size(B,1)), m_inv=ones(size(B,1)))
    N = size(B,1)

    # grounded jacobian
    M_inv = Diagonal(m_inv)
    ϕ_fp = usync[1:N] .- usync[1]
    J_ωϕ_1 = -M_inv * B * Diagonal(k .* cos.(B' * ϕ_fp) ) * B'
    J_ωϕ = J_ωϕ_1[1:end, 2:end]
    J_ωω = Diagonal(-m_inv .* d)
    J_ϕω = [-1. * ones(N - 1) I(N - 1)]
    J_ϕϕ = 0. * I(N - 1)
    J = [J_ϕϕ  J_ϕω ; J_ωϕ  J_ωω]
    return J
end

function linear_trajectory(B::SparseMatrixCSC, k::Vector{<:Real}, p::Vector{<:Real},
    u0::Vector{<:Real}, t::Union{<:Real, Vector{<:Real}}; 
    d::Vector{<:Real}=ones(size(B,1)), m_inv::Vector{<:Real}=ones(size(B,1)),
    usync::Vector{<:Real} = synchronous_state(B, k, p; d, m_inv) )

    N = size(B,1)
    J = grounded_jacobian(B, k, usync; d, m_inv)
    λ,W = eigen(Matrix(J))

    δ0 = u0 .- usync
    grounding = δ0[1]
    δ0 = vcat( δ0[2:N] .- δ0[1], δ0[N+1:2N] )
    v = W' * δ0
    δ = W * ( exp.(λ .* t') .* v )

    δ = vcat(zeros(length(t))', δ[1:N-1, :] .+ grounding, δ[N:2N-1, :])
    u = δ .+ usync
    u = real.(u)
    if length(t) == 1
        u = u[1,:]
    else 
        u = Matrix(u')
    end
    return u
end

# Lyapunov equation: A X + X Aᵀ = CᵀC
#   ⇔ X = ∫ exp(At) CᵀC exp(Aᵀt) dt from t = 0 to t → ∞
#
#  and thus ⟹ S² = u₀ᵀ X u₀
#  with CᵀC = [ 0         0  
#               0   1/N ⋅ I ]
#
# Caveat! A is the grounded Laplacian, so X is a ℝ⁽ᴺ⁻¹⁾ˣ⁽ᴺ⁻¹⁾ matrix
#
function lyapunov_matrix(B::SparseMatrixCSC, k::Vector{<:Real}, usync::Vector{<:Real}; 
    d = ones(size(B,1)), m_inv=ones(size(B,1)))

    N = size(B,1)
    J = grounded_jacobian(B, k, usync; d, m_inv)

    # solve Lyapunov equation for subset
    CᵀC = zeros(2N-1,2N-1); CᵀC[N:2N-1,N:2N-1] = I(N) ./ N - ones(N,N) ./ N^2
    X = lyapc(Matrix(J)', CᵀC)
    return X
end

#= ## Iterative solver for Lyapunov equation, unfortunately lyapci does not support initial guess
function lyapunov_matrix(B::SparseMatrixCSC, k::Vector{<:Real}, usync::Vector{<:Real}; 
    d = ones(size(B,1)), m_inv=ones(size(B,1)), X_initial::Matrix{<:Complex})

    N = size(B,1)
    J = grounded_jacobian(B, k, usync; d, m_inv)

    # solve Lyapunov equation for subset
    CᵀC = zeros(2N-1,2N-1); CᵀC[N:2N-1,N:2N-1] = I(N) ./ N - ones(N,N) ./ N^2
    X = lyapci(Matrix(J)', CᵀC; x0=X_initial) # iterative solver starting from initial guess 
    return X
end
=#

function linear_syncnorm(B::SparseMatrixCSC, k::Vector{<:Real}, 
    usync::Vector{<:Real}, δΩ::Real = sqrt(3); 
    d = ones(size(B,1)), m_inv = ones(size(B,1)))

    N = size(B,1)
    X = lyapunov_matrix(B, k, usync; d, m_inv)
    S² = δΩ^2 / 3 * mean( real.( diag( X[N:end, N:end])  )) 
    return S²
end

function linear_syncnorm(B::SparseMatrixCSC, k::Vector{<:Real}, 
    usync::Matrix{<:Real}, δΩ::Real = sqrt(3); nthreads = Threads.nthreads(),
    d = ones(size(B,1)), m_inv = ones(size(B,1)))

    N = size(B,1); n = size(usync,1)
    trX = zeros(n)

    if nthreads == 1 ## Single thread
        @showprogress for i=1:n 
            X = lyapunov_matrix(B, k, usync[i,:]; d, m_inv)
            trX[i] = mean( real.( diag( X[N:end, N:end])  ))  
        end
    else ## Multi threaded
        n_per_thread = n ÷ nthreads ; n_rest = n - n_per_thread * nthreads
        progress = Progress(n)
        @threads for i=1:nthreads
            idx1 = (i-1) * n_per_thread + 1
            idx2 =    i  * n_per_thread + (i==nthreads ? n_rest : 0)
            for j=idx1:idx2
                X = lyapunov_matrix(B, k, usync[j,:]; d, m_inv)
                trX[j] = mean( real.( diag( X[N:end, N:end])  )) 
                next!(progress)
            end
        end
    end
    #= ## Iterative solver for Lyapunov equation, unfortunately lyapci does not support initial guess
    if nthreads == 1 ## Single thread
        progress = Progress(n)
        X = lyapunov_matrix(B, k, usync[1,:]; d, m_inv)
        trX[i] = mean( real.( diag( X[N:end, N:end])  ))
        next!(progress)
        for i=1:n
            X = lyapunov_matrix(B, k, usync[i,:]; d, m_inv, X_initial=X)
            trX[i] = mean( real.( diag( X[N:end, N:end])  )) 
            next!(progress)
        end
    else ## Multi threaded
        n_per_thread = n ÷ nthreads ; n_rest = n - n_per_thread * nthreads
        progress = Progress(n)
        @threads for i=1:nthreads
            idx1 = (i-1) * n_per_thread + 1
            idx2 =    i  * n_per_thread + (i==nthreads ? n_rest : 0)
            X = lyapunov_matrix(B, k, usync[idx1,:]; d, m_inv)
            trX[idx1] = mean( real.( diag( X[N:end, N:end])  ))
            next!(progress)
            for j=idx1+1:idx2
                X = lyapunov_matrix(B,k, usync[j,:]; d, m_inv, X_initial=X)
                trX[j] = mean( real.( diag( X[N:end, N:end])  )) 
                next!(progress)
            end
        end
    end
    =#
    S² = δΩ^2 / 3 .* trX
    return S²
end

