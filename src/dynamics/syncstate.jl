# Reduce dimension to N using
#       ω = u[1] ⋅ [1 … 1] 
#       ϕ = [0, u[2] … u[N] ]
function kuramoto_fixpoint!(res, u, params)
    @unpack B,k,p,d,m_inv = params
    ϕ = copy(u)
    ω = u[1]; ϕ[1] = 0.
    res .= m_inv .* ( - d .* ω .- B * (k .* sin.(B' * ϕ) ) .+ p )
end

function synchronous_state(
    B::SparseMatrixCSC, k::Vector{<:Real}, p::Vector{<:Real};
    d = ones(size(B,1)), m_inv = ones(size(B,1)), 
    initial_guess = zeros(2*size(B,1))) 
    N = size(B,1) 

    # map to reduced dimension space 
    u0 = vcat( [ mean(initial_guess[N+1:2N]) ], initial_guess[2:N] .- initial_guess[1] )

    # apply Newton solver in reduced dimensions
    params = (; B,k,p,d,m_inv)
    problem = NonlinearProblem(kuramoto_fixpoint!, u0, params)
    solution = solve(problem, NewtonRaphson(); verbose=false)
    usync = solution.u

    # map back to full dimension space
    usync = vcat( [0], usync[2:N] , ones(N) .* usync[1] )
    usync[1:N] .-= mean(usync[1:N])

    # check if fixed point was found
    #println("Solver status: ", solution.retcode)
    
    #= 
    if ! SciMLBase.successful_retcode(solution)
        #=
        println("Warning : Fixed point not found.")
        println("  ϕ = ", usync[1:N])
        println("  ω = ", usync[N+1:2N])
        println("Warning : Fixed point not found.")
        =#
        return fill(NaN, 2*N)
    end
    =#

    return usync
end




function synchronous_state(B::SparseMatrixCSC, k::Vector{<:Real}, p::Matrix{<:Real};
    keepsync = true, nthreads = Threads.nthreads(),
    d = ones(size(B,1)), m_inv = ones(size(B,1)), 
    initial_guess = zeros(2*size(B,1)),
    ω_tol = 1e-1, ϕ_tol = 1e3)
    n,N = size(p)

    syncstates = zeros(n, 2*N)
    
    if nthreads == 1  ## Single thread
        if keepsync
            # start from initial guess
            u0 = synchronous_state(B, k, p[1,:]; d, m_inv, initial_guess=zeros(2*N))
            syncstates[1,:] .= u0
            if any(isnan.(u0)) || maximum( abs.(u0[N+1:end]) ) > ω_tol || maximum( abs.(u0[1:N]) ) > ϕ_tol
                u0 .= initial_guess 
            end

            # use previous state as initial guess
            @showprogress for i=2:n
                u1 = synchronous_state(B, k, p[i,:]; d, m_inv, initial_guess=u0)
                syncstates[i,:] .= u1
                if any(isnan.(u1)) || maximum( abs.(u0[N+1:end]) ) > ω_tol || maximum( abs.(u0[1:N]) ) > ϕ_tol
                    u0 .= initial_guess 
                else
                    u0 .= u1
                end
            end

        else # just use same initial guess for all 
            syncstates = zeros(n, 2*N)
            @showprogress for i=1:n
                u0 = synchronous_state(B, k, p[i,:]; d, m_inv, initial_guess)
                syncstates[i,:] .= u0
            end
        end
    
    else ## Multi threaded
        n_per_thread = n ÷ nthreads ; n_rest = n - n_per_thread * nthreads
        prog = Progress(n)
        @threads for i=1:nthreads
            idx1 = (i-1) * n_per_thread + 1
            idx2 =    i  * n_per_thread + (i==nthreads ? n_rest : 0)
            if keepsync
                u0 = synchronous_state(B, k, p[idx1,:]; d, m_inv, initial_guess)
                syncstates[idx1, :] = u0
                if any(isnan.(u0)) || maximum( abs.(u0[N+1:end]) ) > ω_tol || maximum( abs.(u0[1:N]) ) > ϕ_tol
                    u0 .= initial_guess 
                end
                next!(prog)
                
                for j=idx1+1:idx2
                    u1 = synchronous_state(B, k, p[j,:]; d, m_inv, initial_guess=u0)
                    syncstates[j, :] = u1
                    if any(isnan.(u1)) || maximum( abs.(u0[N+1:end]) ) > ω_tol || maximum( abs.(u0[1:N]) ) > ϕ_tol
                        u0 .= initial_guess
                    else
                        u0 .= u1
                    end
                    next!(prog)
                end
            else
                for j=idx1:idx2
                    u0 = synchronous_state(B, k, p[j,:]; d, m_inv, initial_guess)
                    syncstates[j, :] .= u0
                    next!(prog)
                end
            end
        end    
    end
    return syncstates
end

        
# Phase coherence of a angular state, eg of synchronous state 
phase_coherence(B::SparseMatrixCSC, ϕ::Vector{<:Real}) = mean( cos.(B' * ϕ[1:size(B,1)]) )
phase_coherence(B::SparseMatrixCSC, ϕ::Matrix{<:Real}) = mean( cos.(ϕ[:,1:size(B,1)] * B) , dims=2 )[:,1] 
