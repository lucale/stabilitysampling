using StabilitySampling
using Test
using Distributions
using Random
using LinearAlgebra
using CairoMakie

# Test Problem 
background = Product([Uniform(0, 2), Uniform(-1,2)])
target = MvNormal( [1.0, .5], [0.1  0.0; 0.0 1.0])
lower_bounds = [0,0]
upper_bounds = [1,1]


# Chain parameters
x0 = [-1e-3, 0] #slightly out of bounds
lp_background = x -> logpdf(background, x)
lp_target = x->logpdf(target, x)
stepsize = ones(2) .* 1e-2

# Run chains
β1 = 5.0 
chain1 = rce_rwm(lp_background, lp_target, 100_000, x0, β1; 
    lower_bounds, upper_bounds, stepsize)

β2 = 0.05
chain2 = rce_rwm(lp_background, lp_target, 100_000, x0, β2; 
    lower_bounds, upper_bounds, stepsize)

# Test that all other samples are in bounds
@test all( lower_bounds' .<= chain1.X[2:end, :] .<= upper_bounds' )
@test all( lower_bounds' .<= chain2.X[2:end, :] .<= upper_bounds' )

# Visualize
begin 
    fig = Figure(); ax = Axis(fig[1,1]) 
    t1 = truncated(Normal(1.0, 0.1), lower_bounds[1], upper_bounds[1])
    b1 = truncated(Uniform(0,2), lower_bounds[1], upper_bounds[1])
    y1 = rand(t1, 1_000_000); z1 = rand(b1, 1_000_000)
    density!(ax, y1, label="target", color=(:white,0), strokewidth=1.5, strokecolor=(:blue, .9), bandwidth=.1)
    density!(ax, z1, label="background", color=(:white,0), strokewidth=1.5, strokecolor=(:green, .9), bandwidth=.1)
    density!(ax, chain1.X[100:end,1], label="chain β=$β1", color=(:white,0), strokewidth=1.5, strokecolor=(:cyan, .9), bandwidth=.1)
    density!(ax, chain2.X[100:end,1], label="chain β=$β2", color=(:white,0), strokewidth=1.5, strokecolor=(:lightgreen, .9), bandwidth=.1)
    axislegend(ax)
    save("test_normal.png", fig)
end