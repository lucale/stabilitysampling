# Import necessary packages
using Random
using Statistics
using CairoMakie
using Distributions
using Test

using Revise
using StabilitySampling


# Define log target function (standard normal distribution)
function logp_target(x::Vector)
    return -sum(x.^2) / 2
end

# Define a dummy log background function (for now, we assume a flat background)
function logp_background(x::Vector)
    return 0.0  # Flat prior
end

# Set parameters
n_samples = 10_000   # Number of samples
x0 = [0.0]           # Initial position (1D)
stepsize = [0.5]     # Step size for the random walk
save_every = 1       # Save every sample
rng = Random.default_rng()

# Call the rce_rwm function to sample from the standard normal distribution
chain = rce_rwm(
    logp_background, logp_target, n_samples, x0, 1.0;
    stepsize=stepsize, save_every=save_every, rng=rng
)

# Extract the samples from the chain
samples = chain.X[:, 1]

@test all( chain.target .== - samples .^ 2 ./ 2 ) # Check that the target values are correct

# Plot histogram of samples
fig = Figure(); ax = Axis(fig[1, 1])
hist!(ax, samples, bins=50, normalization=:pdf, label="Sampled Distribution", color=:blue)

# Plot the true standard normal distribution for comparison
x = collect( range(-4, 4, length=500) )
pdf_x = pdf.(Normal(0, 1), x)
lines!(ax, x, pdf_x, label="True Normal Distribution", color=:red, linewidth=2)
display(fig)