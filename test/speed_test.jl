#=
using Graphs
using Statistics, Distributions
using CairoMakie
using LinearAlgebra
using Test
using Revise 
using StabilitySampling
using ProgressMeter
using Base.Threads


# Paths to the data
pypsa_folder = "$(@__DIR__)/../data/pypsa-results"
scenario = "de-elec-conventional"
network = "elec_s_100_ec_lv1.0_5h"
println("Loading network ", scenario, " ", network)
@time n = Network(joinpath(pypsa_folder, scenario, "networks", network * ".nc"))

println("Loading parameters " )
@time begin 
    k = get_couplings(n)
    c = get_capacities(n)
    d = get_dampings(n)
    m_inv = 1 ./ get_inertias(n)
    B = incidence_matrix(n)
    F = flow_matrix(B,k)
    p = get_power_timeseries(n)
end 
=#

using Parameters 
using Serialization
using Test
using StabilitySampling
using Base.Threads

@unpack k,c,d,m_inv,B,p = deserialize(joinpath(dirname(@__DIR__), "data", "pypsa-data", "serialized_networks", "conventional.serial")) 


println("Fitting PCA")
@time pca = PCA(p)

println("Calculating sync states on $(nthreads()) threads")
@time u0 = synchronous_state(B,k,p; d, m_inv, keepsync=true);

println("Calculating linear syncnorm on $(nthreads()) threads")
@time linear_syncnorm(B,k,u0; d, m_inv);

println("Calculating sync states single thread")
@time u0 = synchronous_state(B,k,p; d, m_inv, keepsync=true, nthreads=1); 

println("Calculate linear syncnorm on single thread")
@time linear_syncnorm(B,k,u0; d, m_inv, nthreads=1);


