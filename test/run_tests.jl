using Graphs
using Statistics, Distributions
using CairoMakie
using LinearAlgebra
using Test
using Revise 
using StabilitySampling
using ProgressMeter



# Paths to the data
pypsa_folder = "$(@__DIR__)/../data/pypsa-results"

# Load a network for testing
scenario = "de-elec-renewable"
network = "elec_s_50_ec_lvopt_25h"
#scenario = "de-elec-conventional"
#network = "elec_s_50_ec_lv1.0_25h"
n_path = joinpath(pypsa_folder, scenario, "networks", network * ".nc")

println("Loading network: ", scenario, " / ", network)
@time n = Network(n_path)
@test typeof(n) == Network

println("\n# Scale of network:")

B = incidence_matrix(n)
println("Graph size N = $(size(B,1)), L = $(size(B,2))")
μ = eigen(Matrix(B * B')).values
λ2_alg = minimum( μ[μ .> 1e-3])
println("Algebraic connectivity ", λ2_alg)

p = get_power_timeseries(n)
println("Power injections p = ", mean(p) , " ± ", std(p), " ∈ [", minimum(p), ", ", maximum(p), "]")

f = get_flow_timeseries(n); absf = abs.(f)
println("Flow |f| = ", mean(absf) , " ± ", std(absf), " ∈ [", minimum(absf), ", ", maximum(absf), "]")

k = get_couplings(n)
println("Couplings k = ", mean(k), " ± ", std(k), " ∈ [", minimum(k), ", ", maximum(k), "]")

c = get_capacities(n)
println("Capacities c = ", mean(c), " ± ", std(c), " ∈ [", minimum(c), ", ", maximum(c), "]")

m = get_inertias(n); m_inv = 1 ./ m
println("Inertias m = ", mean(m), " ± ", std(m), " ∈ [", minimum(m), ", ", maximum(m), "]")

d = get_dampings(n)
println("Dampings d = ", mean(d), " ± ", std(d), " ∈ [", minimum(d), ", ", maximum(d), "]")

println("\n# Consistency checks ")
@test network_consistency_check(n)
println("\n")

pca = PCA(p)
α = project(pca, p)
@test all( abs.(mean(α, dims=1)[1,:]) .< 1e-3 )

pca_flow = PCA(f)
β = project(pca_flow, f)
@test all( abs.(mean(β, dims=1)[1,:]) .< 1e-3 )

println("\n# Quantities of interest on data as reference value")
Cred = redundant_capacity(c, f)
println("Data Cred = ", mean(Cred), " ± ", std(Cred), " ∈ [", minimum(Cred), ", ", maximum(Cred), "]")

N = size(B,1)
ndata = length(n.snapshots)
println("Calculating $(ndata) data sync states ")
usync = synchronous_state(B, c, p[1:10:end,:]; d, m_inv, keepsync=true)
synced =  .! any(isnan.(usync), dims=2 )[:,1]
println("Found data sync states in ", round(100*mean(synced), sigdigits=3), " % of the cases")

runi = phase_coherence(B,usync)
println("Data r_uni = ", mean(runi), " ± ", std(runi), " ∈ [", minimum(runi), ", ", maximum(runi), "]")


println("\n# Test the stochastic model")

println("Calculate background distribution model")
n50 = relevant_dims(pca, 0.5)
@time ψ = background_distribution(n)
println("Model statistics ")
μ_ψ = mean.(ψ.v[1:n50]); σ_ψ = std.(ψ.v[1:n50])
for i=1:n50
    println("  α_$i = ", μ_ψ[i], " ± ", σ_ψ[i])
end

F = flow_matrix(B, k)
α_sample = Matrix(rand(ψ, 100)')
p_sample = transform(pca, α_sample)
f_sample = p_sample * F'
Cred_sample = redundant_capacity(c, f_sample)
println("Simple Model Cred = ", mean(Cred_sample), " ± ", std(Cred_sample), " ∈ [", minimum(Cred_sample), ", ", maximum(Cred_sample), "]")


println("Calculate MvGMM model")
@time mvgmm = background_mvgmm(pca, relevance=0.5)
α_mvgmm = Matrix(rand(mvgmm, 100)')
p_mvgmm = transform(pca, α_mvgmm)
f_mvgmm = p_mvgmm * F'
Cred_mvgmm = redundant_capacity(c, f_mvgmm)
println("MvGMM Model statistics ")
μ_ψ = mean(α_mvgmm, dims=1)[1,:]; σ_ψ = std(α_mvgmm, dims=1)[1,:]
for i=1:4
    println("  α_$i = ", μ_ψ[i], " ± ", σ_ψ[i])
end
println("MvGMM Model Cred = ", mean(Cred_mvgmm), " ± ", std(Cred_mvgmm), " ∈ [", minimum(Cred_mvgmm), ", ", maximum(Cred_mvgmm), "]")
usync_mvgmm = synchronous_state(B, c, p_mvgmm; d, m_inv, keepsync=false)
runi_mvgmm = phase_coherence(B,usync_mvgmm)
println("MvGMM Model r_uni = ", mean(runi_mvgmm), " ± ", std(runi_mvgmm), " ∈ [", minimum(runi_mvgmm), ", ", maximum(runi_mvgmm), "]")