include("../data/utils.jl")

force_redraw = true
renewable_scenario = false

if !renewable_scenario
    scenario = "de-elec-conventional"
    network = "elec_s_100_ec_lv1.0_5h"
else 
    scenario = "de-elec-renewable"
    network = "elec_s_100_ec_lvopt_5h"
end     
n = load_network(scenario, network)
pca = PCA(get_power_timeseries(n))
α_data = project(pca, pca.signal)
bounds_a = minimum(α_data, dims=1)[1,:]
bounds_b = maximum(α_data, dims=1)[1,:]

all_chains = filter( f->startswith(f, "chain_Cred"), readdir( joinpath(cache_folder, scenario, network, "chains")))

model = map(f->split(f, ".")[2], all_chains)
ndim  = parse.(Int, replace.( map(f->split(f, ".")[3], all_chains ), "dims" => "" ) )
params  =  join.(replace.( map(f->split(f, ".")[4:end-2], all_chains ), "dims" => "" ), ".")
β  =  parse.(Float64, map(f->split(f, "_")[2], params ))
s  =  parse.(Float64, map(f->split(f, "_")[4], params ))
nchain = parse.(Int, replace.( map(f->split(f, ".")[end-1], all_chains ), "chain" => "" ) )

output = ""
progress = Progress( length(unique(model)) * length(unique(ndim)) * length(unique(β)) * length(unique(s)) + 1) ; next!(progress)
for _model in unique(model)
    for _ndim in unique(ndim)
        if _model == "igmm"
            fpath = fpath = joinpath(cache_folder, scenario, network, "models", "model.igmm.jld2")
            if !isfile(fpath) continue end
            m = deserialize(fpath)
            m = Product(m.v[1:_ndim])
        elseif _model == "mvgmm"
            fpath = joinpath(cache_folder, scenario, network, "models", "model.$(_model).$(_ndim)dims.jld2")
            if !isfile(fpath) continue end
            m = deserialize( fpath)
        end

        for _β in unique(β)
            for _s in unique(s)
                output *= "\n\n$(uppercase(_model)) $_ndim dims, β = $_β, s = $_s "
                mask = _model .== model .&& _ndim .== ndim .&& _β .== β .&& _s .== s
                if sum(mask) == 0 continue end 

                Cred = vcat( map( f -> clean_chain( deserialize(joinpath(cache_folder, scenario, network, "chains", f)).Cred ) , all_chains[mask]) ... )
                α = vcat( map( f -> clean_chain( deserialize(joinpath(cache_folder, scenario, network, "chains", f)).α ), all_chains[mask]) ... )

                #@time logp = [ logpdf(m, α[i,:]) for i=1:size(α,1) ]
                #logp_chain = logp .+ _β .* Cred 
                #logp_chain = logp_chain .- maximum(logp_chain) 

                w = exp.( _β .* Cred .- maximum( _β .* Cred) )
                w = w ./ mean(w)
                failure_prob = mean( w .* (Cred .< 0) ) 
                failure_err = sqrt( mean( w .* ( ( Cred .< 0 ) .- failure_prob) .^ 2 ) / ( length(Cred) - 1) ) 


                output *= "\n Failure probabilility $(failure_prob * 100) ± $(failure_err  * 100) %"
                next!(progress)

            end
        end
    end
end
println(output)
