
using Graphs
using Statistics, Distributions
using CairoMakie
using LinearAlgebra
using Test
using Revise 
using StabilitySampling
using ProgressMeter
using Serialization
using Random, Primes, HaltonSequences
using FileIO



# Load a network for testing
is_renewable = false
if is_renewable
    scenario = "de-elec-conventional"
    network = "elec_s_100_ec_lv1.0_5h"
else 
    scenario = "de-elec-renewable"
    network = "elec_s_100_ec_lvopt_5h"
end


println("Loading network $(scenario) / $(network) ")
@time n = Network( joinpath(dirname(@__DIR__), "data", "pypsa-results", scenario, "networks", network * ".nc"))


# Parameter of the network 
N = nv(n)
k = get_couplings(n)
B = incidence_matrix(n)
m_inv = 1 ./ get_inertias(n) 
d = get_dampings(n)

# Random power injection pattern
p_t = get_power_timeseries(n)
p = p_t[rand(1:size(p_t,1)),:] .+ 1e3 .* randn(N)  
p = p .- mean(p)

println("Finding sync state")
@time u0 = synchronous_state(B,k,p; d,m_inv)
runi = phase_coherence(B, u0)
println("rᵤₙᵢ = ", runi)   


#Δϕ = π/180 * (rand() * 2 - 1 ) * 5.0 # ± 5°
#Δω = (rand() * 2 - 1) * 5.0 * 2π     # ± 5 Hz
Δϕ = 0.0 ; Δω = sqrt(1/3)
i_atk = argmin(m_inv)
println("Attacking node $i_atk")
Δu = zeros(2N)
Δu[  i_atk] = Δϕ ; Δu[N+i_atk] = Δω

T_final = 50.0
println("Solve nonlinear trajectory to T = $T_final")
@time t,u = solve_trajectory(B,k,p,u0+Δu; t=T_final, d,m_inv)   
S2 = syncnorm(t,u)
println("Nonlinear S² = ", S2)

println("Evalulating linearized trajectory at $(length(t)) points")
@time u_lin = linear_trajectory(B,k,p,u0+Δu, t; usync=u0, d, m_inv) 
u_lin[N+1:end] .= u_lin[N+1:end] .* 2.155
S2_lin = syncnorm(t,u_lin)
println("Trajectory linear S² = ", S2_lin)


# Calculate sync norm with Lyapunov equation
X = lyapunov_matrix(B,k,u0; d,m_inv)
v = vcat( Δu[2:N] .- Δu[1], Δu[N+1:2N] )
S2_lyap = v' * X * v
println("Lyapunov linear syncnorm S² = ", S2_lyap)


println("Plotting finite time sync norm")
@time begin 

    fig = Figure(size=(900, 300))
    ax = Axis(fig[1,1])
    Δt = t[2:end] .- t[1:end-1]
    T = cumsum(Δt)
    Tₘ = ( T[2:end] .+ T[1:end-1] ) ./ 2

    ω = u[:,N+1:2N]
    ω² = mean( ( ω .- mean(ω, dims=2) ) .^ 2, dims=2)[:,1]
    Δt  = t[2:end] .- t[1:end-1]
    tₘ = ( t[2:end] .+ t[1:end-1] ) ./ 2
    ω²ₘ = ( ω²[2:end] .+ ω²[1:end-1] ) ./ 2
    S² = cumsum( ω²ₘ .* Δt )
    C,λ = StabilitySampling.fit_bounded_growth(T[2:end], S²[2:end])
    plt = lines!(T, S², label="non-linear S² = $(round(C, sigdigits=5))")
    lines!(ax, T, C .* (1 .- exp.(- λ .* T )), linestyle=:dot, color = plt.color[])
    hlines!(ax, C, linestyle=:dash, color=plt.color[])
    #text!(ax, -.5, C, text="$(round(C, sigdigits=5))",  color=plt.color[], align = (:center, :top))


    ω = u_lin[:,N+1:2N]
    ω² = mean( ( ω .- mean(ω, dims=2) ) .^ 2, dims=2)[:,1]
    Δt  = t[2:end] .- t[1:end-1]
    tₘ = ( t[2:end] .+ t[1:end-1] ) ./ 2
    ω²ₘ = ( ω²[2:end] .+ ω²[1:end-1] ) ./ 2
    S² = cumsum( ω²ₘ .* Δt )
    C,λ = StabilitySampling.fit_bounded_growth(T[2:end], S²[2:end])
    plt = lines!(T, S², label="linearized S² = $(round(C, sigdigits=5))")
    lines!(ax, T, C .* (1 .- exp.(- λ .* T )), linestyle=:dot, color = plt.color[])
    hlines!(ax, C, linestyle=:dash, color=plt.color[])
    #text!(ax, -.5, C, text="$(round(C, sigdigits=5))",  color=plt.color[], align = (:center, :bottom))


    hlines!(ax, S2_lyap, linestyle=:dashdot, color=:slategray, label="Lyapunov S² = $(round(S2_lyap, sigdigits=5))")
    #text!(ax, 10, S2_lyap, text="$(round(S2_lyap, sigdigits=5))",  color=:slategray, align = (:center, :bottom))

    ax.xlabel = "Integration Time T [s]" 
    #ax.ylabel = L"Finite time sync norm $ \mathcal{S}_T$"
    ax.ylabel = "Finite time sync norm S"
    #Label(fig[0,1], "Finite time sync norm  \n S = 1/N ∑ᵢ ∫₀ᵀ ( ωᵢ(x) - ⟨ω⟩(t) )² dt  for  T < ∞", tellwidth=false)
    #Label(fig[0,1], L"Finite time sync norm $ \mathcal{S}_T = \frac{1}{N} \int_0^T \sum_{i=1}^{N} ( \ \omega_i (t) - \bar{\omega} (t)\  )^2 dt $ ", tellwidth=false)
    axislegend(ax, position=(:right, :bottom))
    display(fig)

    save("trajectory_syncnorm.png", fig)
end 

# Plot 
println("Plotting trajectory")
@time begin 
    fig = Figure(size=(600, 400))
    Label(fig[0,1], "Solution of Non-linear Swing Equation", tellwidth=false)

    # Frequency plot
    ax = Axis(fig[1,1])
    t_ax = vcat([-1, 0], t)
    ω = u[:,N+1:2N]
    ω0 = u0[N+1:2N]
    ω = vcat( stack([ω0, ω0], dims=1), ω )
    for i=1:N
        lines!(ax, t_ax, ω[:,i], alpha=.85)
    end
    a,b = extrema(ω)
    if (b-a <= 0.1) 
        m = (a+b)/2
        a,b = m-1,m+1
    end 
    ylims!(ax, a,b)
    hlines!(ax, [-1, 1], color=(:slategray, 0.4), linestyle=:dash)
    xlims!(-.9, maximum(t))
    ax.ylabel = "Frequency ω [Hz]"

    # Phase plot
    ax = Axis(fig[2,1])
    ϕ = u[:,1:N]
    ϕ0 = u0[1:N]
    ϕ = vcat( stack([ϕ0, ϕ0], dims=1), ϕ )
    for i=1:N
        lines!(ax, t_ax, ϕ[:,i], alpha=.85)
    end
    xlims!(-.9, maximum(t))
    a,b = extrema(ϕ)
    if (b-a <= π/2) 
        m = (a+b)/2
        a,b = m-π/4,m+π/4
    end 
    ylims!(ax, a-1,b+1)
    ax.xlabel = "Time t [s]"
    ax.ylabel = "Phase ϕ [rad]"
    display(fig)

    save("trajectory_nonlinear.png", fig)
end

