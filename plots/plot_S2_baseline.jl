
using Statistics 
using Distributions
using CairoMakie
using Parameters
using ProgressMeter
using Serialization
using DataFrames

using Revise
using StabilitySampling

force_redraw = true

for scenario in [ "conventional", "renewable" ] 

    # Load general data
    data_path = joinpath(dirname(@__DIR__), "data", "pypsa-data", "serialized_networks", scenario * ".serial")
    @unpack B,k,d,m_inv,p,x,y = deserialize(data_path)
    pca_path = joinpath(dirname(@__DIR__), "data", "cache", scenario, "models", "pca.serial")
    pca = deserialize(pca_path)

    # Base path for QoI data
    qoi_path = joinpath(dirname(@__DIR__), "data", "cache", scenario, "qoi", "qoi_sync.DATA.serial")

    # Loop over all safety margins
    all_safetymargins = unique( map(f->parse(Float64, split(join(split(f, ".")[2:end-1],"."), "_")[end]), filter(f -> occursin("safetymargin",f), readdir(dirname(qoi_path))) ) )
    for safety_margin in all_safetymargins

        # Create paths
        qoi_files = readdir(dirname(qoi_path))
        qoi_files = filter(f -> occursin("safetymargin_$(safety_margin)", f), qoi_files)

        igmm_files = filter(f -> occursin("igmm", f), qoi_files)
        igmm_dims = map( f -> parse(Int, replace(split(split(f, ".")[2], "_")[2], "dims"=>"")), igmm_files)
        sort!(igmm_dims)

        mvgmm_files = filter(f -> occursin("mvgmm", f), qoi_files)
        mvgmm_dims = map( f -> parse(Int, replace(split(split(f, ".")[2], "_")[2], "dims"=>"")), mvgmm_files)
        sort!(mvgmm_dims)

        # Load the serial data
        qoi_data = deserialize( replace(qoi_path, "DATA" => "data") )
        qoi_igmm = [ deserialize(replace(qoi_path, "DATA" => "igmm_$(ndim)dims_baseline_safetymargin_$(safety_margin)")) for ndim in igmm_dims ]
        qoi_mvmm = [ deserialize(replace(qoi_path, "DATA" => "mvgmm_$(ndim)dims_baseline_safetymargin_$(safety_margin)")) for ndim in mvgmm_dims ]

        # Plotting utitlities
        light_green = [0.4, 1.0, 0.4]
        dark_green  = [0.0, 0.4, 0.0]
        light_blue  = [0.6, 0.8, 1.0]
        dark_blue   = [0.0, 0.0, 0.4]
        colors_igmm = [ RGBAf( (light_blue .* t .+ dark_blue .* (1 - t) ) ... ) for t in range(0,1, length(igmm_dims)) ]
        colors_mvgmm = [ RGBAf( (light_green .* t .+ dark_green .* (1 - t) ) ... ) for t in range(0,1, length(mvgmm_dims)) ]

        # Figure for S2
        begin 
            bandwidth = 1e-4
            fig = Figure()
            ax = Axis(fig[1,1])
            ax.xlabel = "Linear Synchronization Norm"
            ax.ylabel = "Density"
            ax.title = "$(uppercasefirst(scenario)) scenario with " * (safety_margin == Inf ? "no" : "$(round(100 * safety_margin, sigdigits=4))%") * " operational bounds" 
            density!(ax, qoi_data.S2; color=(:white, 0.0), strokecolor=(:black, 1.0), strokewidth=1.5, linestyle=:dash,  bandwidth, npoints=1000) 
            for (i,ndim) in enumerate(igmm_dims)    
                density!(ax, qoi_igmm[i].S2; color=(:white, 0.0), strokecolor=(colors_igmm[i], 0.7), strokewidth=1.0, bandwidth, npoints=1000)
            end 
            for (i,ndim) in enumerate(mvgmm_dims)    
                density!(ax, qoi_mvmm[i].S2; color=(:white, 0.0), strokecolor=(colors_mvgmm[i], 0.7), strokewidth=1.0, bandwidth, npoints=1000)
            end
            xlims!(ax, scenario == "renewable" ? (0.019, 0.026) :  (0.039, 0.047))
            
            Legend(fig[1,2], 
                [ 
                    [LineElement(color=:black, linewidth=1, linestyle=:dash)],
                    [LineElement(color=colors_igmm[i], linewidth=3) for i in 1:length(igmm_dims) ],
                    [LineElement(color=colors_mvgmm[i], linewidth=3) for i in 1:length(mvgmm_dims) ],
                ],
                [ 
                    ["Data"],
                    [ "$(ndim) dims" for ndim in igmm_dims ], 
                    [ "$(ndim) dims" for ndim in mvgmm_dims ], 
                ], ["", "IGMM", "MvGMM"],
            )
            display(fig)
            save(joinpath(@__DIR__, "images", scenario, "S2_baseline_safetymargin_$(safety_margin).png"), fig)
        end

        # Figure for runi
        begin 
            bandwidth = 1e-2

            fig = Figure()
            ax = Axis(fig[1,1])
            ax.xlabel = "Synchronous Phase Coherence"
            ax.ylabel = "Density"
            ax.title = "$(uppercasefirst(scenario)) scenario with " * (safety_margin == Inf ? "no" : "$(round(100 * safety_margin, sigdigits=4))%") * " operational bounds" 
            xlims!(ax, 0.75, 1.0)
            density!(ax, qoi_data.runi; color=(:white, 0.0), strokecolor=(:black, 1.0), strokewidth=1.5, linestyle=:dash, bandwidth, npoints=1000) 
            for (i,ndim) in enumerate(igmm_dims)    
                density!(ax, qoi_igmm[i].runi; color=(:white, 0.0), strokecolor=(colors_igmm[i], 0.7), strokewidth=1.0, bandwidth, npoints=1000)
            end 
            for (i,ndim) in enumerate(mvgmm_dims)    
                density!(ax, qoi_mvmm[i].runi; color=(:white, 0.0), strokecolor=(colors_mvgmm[i], 0.7), strokewidth=1.0, bandwidth, npoints=1000)
            end

            Legend(fig[1,2], 
                [ 
                    [LineElement(color=:black, linewidth=1, linestyle=:dash)],
                    [LineElement(color=colors_igmm[i], linewidth=3) for i in 1:length(igmm_dims) ],
                    [LineElement(color=colors_mvgmm[i], linewidth=3) for i in 1:length(mvgmm_dims) ],
                ],
                [ 
                    ["Data"],
                    [ "$(ndim) dims" for ndim in igmm_dims ],
                    [ "$(ndim) dims" for ndim in mvgmm_dims ], 
                ], ["", "IGMM", "MvGMM"],
            )
            save(joinpath(@__DIR__, "images", scenario, "runi_baseline_safetymargin_$(safety_margin).png"), fig)
            display(fig)
        end
    end
end