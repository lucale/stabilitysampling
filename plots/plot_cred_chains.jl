using Statistics 
using Distributions
using CairoMakie
using Parameters
using ProgressMeter
using Serialization
using DataFrames

using Revise
using StabilitySampling

force_redraw = false

function get_chain_paths(scenario::String)
    cache_folder = joinpath(dirname(@__DIR__), "data", "cache")
    chain_folder = joinpath(cache_folder, scenario, "chains")
    model_folder = joinpath(cache_folder, scenario, "models")
    chain_files = filter(f->startswith(f, "chain_cred"), readdir(chain_folder)); sort!(chain_files)
    return joinpath.([chain_folder], chain_files)
end

function read_chain_data(chain_path::String)
    chain = deserialize(chain_path)
    α = chain.α; Cred = chain.Cred
    lags = length(Cred) .÷ ( 2 .^ collect(0: floor(Int, log2(length(Cred)))) ) 
    ac = autocor(Cred, lags)
    corrlength = findfirst( ac .< 0.01 )
    β = chain.parameters.β
    w = -β .* Cred ; w = w .- maximum(w); w = exp.(w) ; w = w ./ sum(w)    
    params = chain.parameters
    return (;
        acc_rate = mean(chain.accepted),        
        corr_length = isnothing(corrlength) ? Inf : corrlength,
        mean_cred =  sum( w .* Cred),
        std_cred = sqrt( sum( w .* (Cred .- mean(Cred)).^2)),
        mean_cred_β = mean(Cred),
        std_cred_β = std(Cred),
        P_fail = sum( w .* (Cred .< 0)),
        params ... 
    )

end

function get_chain_data()
    data = DataFrame()
    conventional_files = get_chain_paths("conventional")
    renewable_files    = get_chain_paths("renewable")
    data[!, :scenario] = vcat(repeat(["conventional"], length(conventional_files)), repeat(["renewable"], length(renewable_files)))
    data[!, :path] = vcat(conventional_files, renewable_files)

    println("Reading all chain data")
    @time chain_data = read_chain_data.(data.path)
    for field in keys(chain_data[1])
        data[!, field] = getfield.(chain_data, field)
    end
    return data
end

data = get_chain_data()
all_paths = data.path[ data.chain .== 1 ]
#test_mask = data.P_fail .> 0 .&& data.chain .== 1 .&& data.scenario .== "renewable" .&& data.ndim .== 42
#all_paths = [ data.path[test_mask][3] ]
for (ipath, path) in enumerate(all_paths)
    chains = [ deserialize(replace(path, "chain1"=>"chain$i")) for i=1:9 ]
    params = chains[1].parameters
    info = "Chains for $(params.scenario): $(uppercase(params.model)) $(params.ndim) dims with β = $(params.β), stepsize = $(100 * params.s)%, safety_margin $(params.safety_margin) "    
    println("\n## ", ipath, " / ",  length(all_paths), " ", info)

    savepath = joinpath(@__DIR__, "images", params.scenario, "chains", "$(params.model)_$(params.ndim)dims", "beta_$(params.β)_step_$(params.s)_safetymargin_$(params.safety_margin)")
    if !isdir(savepath) mkpath(savepath) end

    if isfile(joinpath(savepath, "info.txt")) && !force_redraw
        println("Skipping $(savepath)")
        continue
    end

    @unpack B,k,c,x,y = deserialize( joinpath(dirname(@__DIR__), "data", "pypsa-data", "serialized_networks", params.scenario * ".serial") )
    F = flow_matrix(B,k)
    pca = deserialize( joinpath(dirname(@__DIR__), "data", "cache", params.scenario, "models", "pca.serial") ) 
    
    α_t = project(pca, pca.signal)
    bounds_a = minimum(α_t, dims=1)[1,:]
    bounds_b = maximum(α_t, dims=1)[1,:]

    # Load model
    modelpath = joinpath(dirname(@__DIR__), "data", "cache", params.scenario, "models", "MODEL.serial")
    if params.model == "igmm"
        ψ = deserialize( replace(modelpath, "MODEL" => "igmm") )
        ψ = Product(ψ.v[1:params.ndim])
    elseif params.model == "mvgmm"
        ψ = deserialize( replace(modelpath, "MODEL" => "mvgmm.$(params.ndim)dims") )
    end

    # Acceptance rates
    acc_rates = map( chain -> mean(chain.accepted), chains ) 
    info *= "\nAcceptance $(round( 100*mean(acc_rates), sigdigits=4)) ± $(round( 100*std(acc_rates), sigdigits=4)) %"
    
    # States, target and probabilities 
    α = map( chain -> chain.α, chains )
    Cred = map( chain -> chain.Cred, chains )
    @time prob = map( i->[ pdf(ψ, α[i][j,:]) for j=1:size(α[i],1)], 1:length(chains) )
    

    # Calculate redundant capacity statistics
    μ = mean.(Cred) ; σ = std.(Cred)
    info *= """
    Redundant Capacity 
        Mean $(round(mean(μ), sigdigits=4)) ± $(round(std(μ), sigdigits=4)) MW
        Std $(round(mean(σ), sigdigits=4)) ± $(round(std(σ), sigdigits=4)) MW
    """
    
    Cred_all = vcat( Cred... )
    w = -params.β .* Cred_all; w = exp.( w .- maximum(w) ); w = w ./ sum(w)
    P_fail = sum( w .* (Cred_all .< 0) )
    P_fail_std = sqrt( sum( w .* ( ((Cred_all .< 0) .- P_fail) .^ 2)) )        
    println("P_fail = ", 100 * P_fail, " ± ", 100 * P_fail_std, "%")

    # Calculate overload probability
    vol = prod( bounds_b[1:params.ndim] .- bounds_a[1:params.ndim] )

    P_overload = zeros(length(chains))
    R_overload = zeros(length(chains))
    for i=1:length(chains)
        w = -params.β .* Cred[i]; w = exp.( w .- maximum(w) ); w = w ./ sum(w)
        P_overload[i] = sum( w .* ( Cred[i] .< 0) ) 
        println("P_$i = ", P_overload[i])
        
        R_overload[i] = mean(Cred[i] .< 0)
    end
    info *= """
    Overload 
        Probability $(round( 100*P_fail, sigdigits=4)) ± $(round( 100*P_fail_std, sigdigits=4)) %
        Probability $(round( 100*mean(P_overload), sigdigits=4)) ± $(round( 100*std(P_overload), sigdigits=4)) %
        Sample ratio $(round( 100*mean(R_overload), sigdigits=4)) ± $(round( 100*std(R_overload), sigdigits=4)) %
        Conditional probability per line: 
    """

    
    # Calculate conditional overload probability
    println("Evaluating probabilities")
    most_likely_failures = map( i -> argmax( prob[i] .* (Cred[i] .< 0) ), 1:length(chains) )
    
    α_overload = vcat( map( i->α[i][ Cred[i] .< 0, :], 1:length(chains) ) ... )

    if size(α_overload,1) == 0 
        overload_probs = zeros(size(B,2))
    else 
        w = - params.β .* vcat( map( i->Cred[i][ Cred[i] .< 0], 1:length(chains) ) ... )
        w = exp.( w .- maximum(w) ); w = w ./ sum(w)

        overload_probs = sum( w .* ( (c' .- transform(pca, α_overload) * F' ) .< 0), dims=1)[1,:]    
        for overload_idx in findall(overload_probs .> 0)
            info *= "     Line $overload_idx: $(overload_probs[overload_idx])\n"
        end
    end

    # Plotting utilties
    chain_colors = [ RGBAf(0.5 + 0.5 * t, 0.5 * t, 1 - t )  for t in range(0,1,step=1/length(chains))]
    lags = ceil.(Int, 2 .^ collect( 1:.5:ceil(Int, log2(length(Cred[1]))) ))


    # Save info
    @time begin 
        println("Saving info")
        open(joinpath(savepath, "info.txt"), "w") do f 
            write(f, info)
        end
    end


    # Plot trajectories
    @time begin 
        println("Plotting trajectories")
        fig = Figure(); ax = Axis(fig[1,1])
        ax.xlabel = "Iteration"
        ax.ylabel = "Redundant Capacity [MW]"
        niters = collect(1:length(Cred[1])) .* params.save_every .+ length(Cred[1])
        xlims!(ax, extrema(niters))
        for i=1:length(chains)
            lines!(ax, niters, Cred[i]; color=(chain_colors[i], 0.75))
        end
        ax.title = "Acceptance rate $(round( 100*mean(acc_rates), sigdigits=4)) ± $(round( 100*std(acc_rates), sigdigits=4)) %"
        save(joinpath(savepath, "trajectory.png"), fig)
    end


    # Plot densities
    @time begin
        println("Plotting densities")
        fig = Figure(); ax = Axis(fig[1,1])
        ax.xlabel = "Redundant Capacity [MW]"
        ax.ylabel = "Density"
        for i=1:length(chains)
            density!(ax, Cred[i]; color=(:white,0.0), strokecolor=(chain_colors[i], 0.75), strokewidth=1.0)
        end 
        ax.title = "Mean Redundant Capacity $(round(mean(μ), sigdigits=4)) ± $(round(std(μ), sigdigits=4)) MW"
        save(joinpath(savepath, "density.png"), fig)
    end


    # Plot PCA space
    @time begin 
        println("Plotting PCA coefs")
        α_data = project(pca, pca.signal)
        ndim = min(size(chains[1].α,2),  5)
        lims = extrema(α_data[:,1:ndim], dims=1)[1,:]
        labels = "PC" .* string.(1:ndim)
        fig = pairplot( α_data[:, 1:ndim], labels)
        
        α_reduced = vcat( map(α -> α[ size(α,1)÷2:end, 1:ndim ] , α) ... )
        if size(α_reduced,1) > 10_000
            reducestep = size(α_reduced, 1) ÷ 5000 
            α_reduced = α_reduced[1:reducestep:end, :]
        end
        pairplot!(fig, α_reduced, labels; lims, densitycolor=:gold2, histcolor=(:white, 0.0), 
                        contourcolormap=[:khaki, :yellow, :gold], hexbincolormap=[(:white,0.0)])

        if size(α_overload,1) > 0
            if size(α_overload,1) > 10_000 
                reducestep = size(α_reduced,1)÷1000
                α_overload_reduced = α_overload[1:reducestep:end, 1:ndim]
            else
                α_overload_reduced = α_overload[:, 1:ndim]
            end
            pairplot!(fig, α_overload_reduced, labels; lims, densitycolor=:darkred, histcolor=(:white, 0.0), 
                            contourcolormap=[:tomato, :red, :darkred], hexbincolormap=[(:white,0.0)])
        end     
        save(joinpath(savepath, "pcaspace.png"), fig)
    end


    # Plot overload probability per line
    @time begin
        println("Plotting overload probability")        
        fig = Figure()
        ax = geo_ax(fig[1,1],x,y)
        scatter!(ax, x,y, markersize=5, color=:black)
        edgsrc = getindex.(argmax(B .== 1, dims=1)[1,:],1)
        edgdst = getindex.(argmax(B .==-1, dims=1)[1,:],1)
        for (e,(u,v)) in enumerate(zip(edgsrc,edgdst))
            has_overload = overload_probs[e] > 0.0
            lines!(ax, [ x[u], x[v] ], [ y[u], y[v] ],
                color = has_overload ? overload_probs[e] : :slategray,
                colormap = has_overload ? [:pink, :tomato, :darkred] : nothing,
                colorrange = has_overload ? extrema(overload_probs) : nothing,
                linewidth = has_overload ? 2.5 : 0.75,    
                #linestyle = has_overload ? (:dot, :dense) : :solid
            )
        end
        if any(overload_probs .> 0)
            Colorbar(fig[1,2], label="Conditional overload probability", 
                colormap = [:pink, :tomato, :darkred],
                colorrange = extrema(overload_probs)
            )
            ax.title = "Overload probability $(round(100 * P_fail, sigdigits=4))%"
                #$(round( 100*mean(P_overload), sigdigits=4)) ± $(round( 100*std(P_overload), sigdigits=4)) %"
        end
        save(joinpath(savepath, "overload.png"), fig)
    end


    @time begin
        println("Plotting most likely failures")
        for i=1:length(chains)
            p = transform( pca, α[i][most_likely_failures[i], :] )
            f = F * p
            p_t = pca.signal
            pmin = minimum(p_t, dims=1)[1,:]; pmax = maximum(p_t, dims=1)[1,:]
            pdiff = pmax .- pmin
            pmin = pmin .- params.safety_margin .* pdiff
            pmax = pmax .+ params.safety_margin .* pdiff
            #fig = plot_power_flow(B,x,y, p, f)
            fig = plot_relative_power_flow(B,x,y,p,f,pmin,pmax,c)
            save(joinpath(savepath, "most_likely_failure_chain$i.png"), fig)
        end 
    end
end