include("utils.jl")

using HaltonSequences
using Primes
using Random

# Load a network for testing
scenario = "de-elec-conventional"
network = "elec_s_100_ec_lv1.0_5h"

# Load network 
println("Loading $(scenario) / $(network) ")
@time n = load_network(scenario, network)

# Network parameters
N = nv(n)
B = incidence_matrix(n)
k = get_couplings(n)
m_inv = 1 ./ get_inertias(n)
d = get_dampings(n)

# Time series
p_t = get_power_timeseries(n)
f_t = get_flow_timeseries(n)

# Load sync states from cache
fpath = joinpath(cache_folder, scenario, network, "qoi_data.jld2") 
qoi_data = deserialize(fpath)
u0_t = qoi_data.u0


# Sample pattern
j = 1677
p = p_t[j, :]
u0 = u0_t[j, :]

runi = phase_coherence(B, u0)
println("r_uni = ", runi)

# QMC perturbations
nsample = 500
P = shuffle(primes(10))[1:2]
ξ = stack(Halton.(P, length=nsample), dims=2)
ξ = 2 .* ξ .- 1 

i = 42
Δu = zeros(nsample, 2N)
Δu[:,i] = ξ[:,1] .* 180 / π ; Δu[:,N+i] = ξ[:,2] .* 0.5
println("Solving ensemble for $(nsample) perturbations ")
@time ts,us = solve_trajectory(B,k,p, u0' .+ Δu; d,m_inv)
ω_max = map( u -> maximum(abs.(u[:, 1+N:end])), us )

begin 
    fig,ax,plt = hist(ω_max, color=:slategray)
    density!(ω_max, color=(:white, 0.0), strokecolor=(:black, 1.0), strokewidth=1.0)
    ax.xlabel = "Maximal Frequency Deviation"
    display(fig)
end

begin
    ω = stack( map( u -> u[end, 1+N:end], us ), dims=2 )
    ω_max = maximum(abs.(ω), dims=2)[:,1]
    fig,ax,plt = scatter(Δu[:,i] .* 1e-3, Δu[:,N+i] .* 1e-2, color=ω_max, colormap=:viridis)
    ax.xlabel = "Δϕ"
    ax.ylabel = "Δω"
    Colorbar(fig[1,2], colormap=:viridis, colorrange=extrema(ω_max))
    display(fig)
end