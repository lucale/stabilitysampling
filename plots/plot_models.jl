include("../data/utils.jl")


force_redraw = false
show_figures = false
renewable_scenario = true

if !renewable_scenario
    scenario = "de-elec-conventional"
    network = "elec_s_100_ec_lv1.0_5h"
else 
    scenario = "de-elec-renewable"
    network = "elec_s_100_ec_lvopt_5h"
end    

# Load network and data cache (=> run `fit_models.jl` first)
println("Loading $(scenario) / $(network) ")
@time n = load_network(scenario, network)
modelpath = joinpath(cache_folder, scenario, network, "models")

qoi_data = deserialize(joinpath(cache_folder, scenario, network, "qoi", "qoi.data.jld2")) 

# Train PCA
println("Fitting PCA model ")
@time pca = PCA(get_power_timeseries(n))

# Load models
igmm = deserialize(joinpath(cache_folder, scenario, network, "models", "model.igmm.jld2"))
mvgmms = deserialize.(joinpath.([modelpath], filter(f->startswith(f, "model.mvgmm"), readdir(modelpath) ) ))
mvgmms = mvgmms[ sortperm(length.(mvgmms)) ]

println("Calculating marginals ")
@time begin 
    margs1D_mvgmms = [ marginals(mvgmm) for mvgmm in mvgmms ]
    margs2D_mvgmms = [ marginals2D(mvgmm) for mvgmm in mvgmms ]
end


# === Statistical analysis
# Calculate KS distances
α = project(pca, qoi_data.p)
ksδ_igmm = [ 
    StabilitySampling.ksdist([α[i,j] for i=1:size(α,1)], igmm.v[j])
    for j=1:length(igmm.v)
]
println("KS distances IGMM: $(mean(ksδ_igmm)) ± $(std(ksδ_igmm)) ")

ksδ_mvgmm = Vector{Float64}[]
for k=1:length(mvgmms) 
    margs1D = margs1D_mvgmms[k]
    ksδ = vcat( [
        StabilitySampling.ksdist(α[:,j], m)
        for (j,m) in enumerate(margs1D)
    ]) 
    println("KS distances MvGMM $(length(mvgmms[k])) dims: $(mean(ksδ)) ± $(std(ksδ)) ")
    push!(ksδ_mvgmm, ksδ)
end

# KS distances
fpath = joinpath(plots_folder, scenario, network, "models_marginal_ksdist.png")
if !isfile(fpath) || force_redraw
    @time begin 
        println("Plotting KS Dist")
        fig = Figure(size = (450, 420))
        ax = Axis(fig[1,1])
        ax.ylabel = "KS distance"
        ax.xlabel = "Principal Component"
        N = length(pca)
        scatter!(ax, 1:N, ksδ_igmm, color=:blue, label="IGMM")
        for k=1:length(mvgmms)
            scatter!(ax, 1:length(ksδ_mvgmm[k]), ksδ_mvgmm[k], color=k, colorrange=(1,length(mvgmms)), colormap=[:darkgreen, :lightgreen], label="MvGMM ($(length(mvgmms[k])) dims)")
            vlines!(ax, length(mvgmms[k]), color=k, colorrange=(1,length(mvgmms)), colormap=[:darkgreen, :lightgreen], linestyle=:dash, linewidth=1)
        end
        axislegend(ax, valign=:top, halign=:center)
        save(fpath, fig)
        show_figures && display(fig)
    end
end

println("# Calculating KS-distance for QoIs")

println("IGMM")
igmm_files = filter(f -> startswith(f, "qoi.igmm"), readdir(joinpath(cache_folder, scenario, network, "qoi")))
dims = map(f -> parse(Int, replace(split(f, ".")[3], "dims"=>"")), igmm_files )
idx = sortperm(dims); igmm_files = igmm_files[idx]; dims = dims[idx] 
igmm_files = map(f -> joinpath(cache_folder, scenario, network, "qoi", f), igmm_files) 
qoi_igmms = deserialize.(igmm_files)
#idxs = [1, length(qoi_igmms)]; qoi_igmms = qoi_igmms[idxs]; dims = dims[idxs]

@time ksδ_qoi_igmm = [
    StabilitySampling.ksdist(qoi.Cred, qoi_data.Cred) for qoi in qoi_igmms
]

println("MvGMM")
mvgmm_files = filter(f -> startswith(f, "qoi.mvgmm"), readdir(joinpath(cache_folder, scenario, network, "qoi")))
nDims = map(f -> parse(Int, replace(split(f, ".")[3], "dims"=>"")), mvgmm_files )
#nComp = map(f -> parse(Int, replace(split(f, ".")[3], "comps"=>"")), mvgmm_files )
idx = sortperm(nDims); igmm_files = igmm_files[idx]; nDims = nDims[idx]; #nComp = nComp[idx] 
mvgmm_files = map(f -> joinpath(cache_folder, scenario, network, "qoi", f), mvgmm_files) 
qoi_mvgmms = deserialize.(mvgmm_files)
#idxs = [1, length(qoi_mvgmms)]; qoi_mvgmms = qoi_mvgmms[idxs]; nDims = nDims[idxs]; #nComp = nComp[idxs]

@time ksδ_qoi_mvgmm = [
    StabilitySampling.ksdist(qoi.Cred, qoi_data.Cred) for qoi in qoi_mvgmms
]

fpath = joinpath(plots_folder, scenario, network, "models_Cred_ksdist.png")
if !isfile(fpath) || force_redraw
    @time begin 
        println("Plotting KS Dist for QoI")
        fig = Figure(size = (450, 420))
        ax = Axis(fig[1,1])
        ax.ylabel = "KS distance"
        ax.xlabel = "PCA Dimensions"
        scatter!(ax, dims, ksδ_qoi_igmm, color=:blue, label="IGMM")
        scatter!(ax, nDims, ksδ_qoi_mvgmm, color=:green, label="MvGMM")
        #axislegend(ax, valign=:top, halign=:center)
        Legend(fig[1,1], [LineElement(color=c) for c in [:blue, :green]],
                         ["IGMM", "MvGMM"],
                        valign=:top, halign=:right, tellheight=false, tellwidth=false)
        ax.xscale = log10
        ax.xticks = ( unique(vcat(dims, nDims)), string.(unique(vcat(dims, nDims))) )
        save(fpath, fig)
        show_figures && display(fig)
    end
end


# Pairplot for IGMM
fpath = joinpath(plots_folder, scenario, network, "pca_pairplot.igmm.png")
if !isfile(fpath) || force_redraw
    @time begin 
        println("Plotting IGMM pairplot")
        nDim = 7
        lims = extrema(α, dims=1)[1,1:nDim]
        fig = pairplot(α[:,1:nDim], ["PC$i" for i=1:nDim]; lims)
        t = map( lim -> collect(range(lim..., 100)), lims)
        #p_t = map( (t,ψ) -> pdf.(ψ,t), zip( igmm.v, t) )
        p_t = [ pdf.(igmm.v[i], t[i]) for i=1:nDim ]

        for i=1:nDim

            # Diagonal univariate densities
            ax = Axis(fig[i,i])
            lines!(ax, t[i], p_t[i], color=:red)
            xlims!(ax, lims[i])
            if (i!=1)    hideydecorations!(ax) end
            if (i!=nDim) hidexdecorations!(ax) end

            # Off-diagonals product densities
            for j=i+1:nDim
                ax = fig[j,i]
                p_ij = p_t[i] .* p_t[j]'
                contour!(ax, t[i], t[j], p_ij, colormap=[:orange, :red], 
                    linewidth=1.5, alpha=0.6, levels=7)
            end
        end
        save(fpath, fig)
        show_figures && display(fig)
    end
end 

# Pairplot for MvGMM
fpath = joinpath(plots_folder, scenario, network, "pca_pairplot.mvgmm.png")
if !isfile(fpath) || force_redraw
    @time begin 
        println("Plotting MvGMM pairplot")
        
        nDim = 5
        α = project(pca, pca.signal)
        lims = extrema(α, dims=1)[1,1:nDim]
        fig = pairplot(α[:,1:nDim], ["PC$i" for i=1:nDim]; lims)
        t = map( lim -> collect(range(lim..., 300)), lims)
    
        mvgmm = mvgmms[end]
        margs1D = margs1D_mvgmms[end]
        margs2D = margs2D_mvgmms[end]
        for i=1:nDim
            p_t = [ pdf( margs1D[i], t[i][j]) for j=1:length(t[i]) ]
            ax = Axis(fig[i,i])
            lines!(ax, t[i], p_t, color=:red)
            xlims!(ax, lims[i])            

            if i < nDim 
                for j=i+1:nDim
                    t_i = repeat(t[i], inner=1, outer=length(t[j]))
                    t_j = repeat(t[j], inner=length(t[i]), outer=1)
                    t_ij = map(tpl -> [tpl...],  zip(t_i,t_j) )

                    p_ij = map(x->pdf(margs2D[i][j-i],x), t_ij)
                    p_ij = reshape(p_ij, length(t[i]), length(t[j]))
                    ax = fig[j,i]
                    if maximum(p_ij) > minimum(p_ij) + 1e-10
                        contour!(ax, t[i], t[j], p_ij, colormap=[:orange, :red], 
                            linewidth=1.5, alpha=0.6, levels=8)
                    end                   
                end
            end
        end
        save(fpath, fig)
        show_figures && display(fig)
    end
end 

@time begin 
    igmm_files = filter(f -> startswith(f, "qoi.igmm"), readdir(joinpath(cache_folder, scenario, network, "qoi")))
    dims = map(f -> parse(Int, replace(split(f, ".")[3], "dims"=>"")), igmm_files )
    idx = sortperm(dims); igmm_files = igmm_files[idx]; dims = dims[idx] 
    igmm_files = map(f -> joinpath(cache_folder, scenario, network, "qoi", f), igmm_files) 
    qoi_igmms = deserialize.(igmm_files)
    idxs = [1, length(qoi_igmms)]; qoi_igmms = qoi_igmms[idxs]; dims = dims[idxs]

    mvgmm_files = filter(f -> startswith(f, "qoi.mvgmm"), readdir(joinpath(cache_folder, scenario, network, "qoi")))
    nDims = map(f -> parse(Int, replace(split(f, ".")[3], "dims"=>"")), mvgmm_files )
    #nComp = map(f -> parse(Int, replace(split(f, ".")[3], "comps"=>"")), mvgmm_files )
    idx = sortperm(nDims); igmm_files = igmm_files[idx]; nDims = nDims[idx]; #nComp = nComp[idx] 
    mvgmm_files = map(f -> joinpath(cache_folder, scenario, network, "qoi", f), mvgmm_files) 
    qoi_mvgmms = deserialize.(mvgmm_files)
    idxs = [1, length(qoi_mvgmms)]; qoi_mvgmms = qoi_mvgmms[idxs]; nDims = nDims[idxs]; #nComp = nComp[idxs]

    colors_igmm = [RGBAf(0.5*i,0.5*i,0.5*i + 0.5) for i=range(0,1,length(dims)) ]
    colors_mvgmm = [RGBAf(0.5*i,0.5*i+0.5,0.5*i) for i=range(0,1,length(nDims)) ] 

    fpath = joinpath(plots_folder, scenario, network, "qoi_runi.models.png")
    if !isfile(fpath) || force_redraw
        println("Plotting QoI: runi ")
        @time begin 
            args = (;bandwidth = 0.001, npoints = 10_000, color=(:white, 0.0), strokewidth=2 )
            fig = Figure(size=(550, 600))
            ax = Axis(fig[1,1])
            hist!(ax, qoi_data.runi, color=(:slategray, 0.5),  normalization=:pdf )
            
            for (i,qoi_igmm) in enumerate(qoi_igmms)
                density!(ax, qoi_igmm.runi; strokecolor=colors_igmm[i], args...)
            end 
            for (i,qoi_mvgmm) in enumerate(qoi_mvgmms)
                density!(ax, qoi_mvgmm.runi; strokecolor=colors_mvgmm[i], args...)
            end 
            
            xlims!(ax, ( renewable_scenario ? 0.84 : 0.9, 0.99))
            Legend(fig[1,1], [[LineElement(linewidth=2.0, color=c) for c in colors_igmm],
                            [LineElement(linewidth=2.0, color=c) for c in colors_mvgmm]],
                            [["$nDim dims" for nDim in dims],
                            ["$nDim dims" for nDim in nDims]], 
                            ["IGMM", "MvGMM"], tellwidth=false, tellheight=false,
                            valign=:center, halign=:left)
            
            ax.xlabel = "Sync Phase Coherence"
            ax.ylabel = "Density"
            save(fpath, fig)
            show_figures && display(fig)
        end
    end

    fpath = joinpath(plots_folder, scenario, network, "qoi_Cred.models.png")
    if !isfile(fpath) || force_redraw
        println("Plotting QoI: Cred ")
        @time begin 
            args = (;bandwidth = 3.0, npoints = 10_000, color=(:white, 0.0), strokewidth=2 )
            fig = Figure(size=(550, 600))
            ax = Axis(fig[1,1])
            hist!(ax, qoi_data.Cred, color=(:slategray, 0.5),  normalization=:pdf )
            
            for (i,qoi_igmm) in enumerate(qoi_igmms)
                density!(ax, qoi_igmm.Cred; strokecolor=colors_igmm[i], args...)
            end 
            for (i,qoi_mvgmm) in enumerate(qoi_mvgmms)
                density!(ax, qoi_mvgmm.Cred; strokecolor=colors_mvgmm[i], args...)
            end 
    
            xlims!(ax, -300, 500)
            Legend(fig[1,1], [[LineElement(linewidth=2.0, color=c) for c in colors_igmm],
                            [LineElement(linewidth=2.0, color=c) for c in colors_mvgmm]],
                            [["$nDim dims" for nDim in dims],
                            ["$nDim dims" for nDim in nDims]], 
                            ["IGMM", "MvGMM"], tellwidth=false, tellheight=false,
                            valign=:center, halign=:left)
            ax.xlabel = "Redundant Capacity [MW]"
            ax.ylabel = "Density"
            save(fpath, fig)
            show_figures && display(fig)
        end
    end

    fpath = joinpath(plots_folder, scenario, network, "qoi_S2.models.png")
    if !isfile(fpath) || force_redraw
        println("Plotting QoI: S2 ")
        @time begin 
            args = (;bandwidth = 1e-5, npoints = 10_000, color=(:white, 0.0), strokewidth=1 )
            fig = Figure(size=(550, 600))
            ax = Axis(fig[1,1])
            hist!(ax, qoi_data.S2, color=(:slategray, 0.5),  normalization=:pdf )
            
            for (i,qoi_igmm) in enumerate(qoi_igmms)
                density!(ax, qoi_igmm.S2; strokecolor=colors_igmm[i], args...)
            end 
            for (i,qoi_mvgmm) in enumerate(qoi_mvgmms)
                density!(ax, qoi_mvgmm.S2; strokecolor=colors_mvgmm[i], args...)
            end 
            ax.xlabel = "Sync Norm"
            ax.ylabel = "Density"
            #xlims!( ax, extrema(qoi_data.S2) )
            Legend(fig[1,1], [[LineElement(linewidth=2.0, color=c) for c in colors_igmm],
                            [LineElement(linewidth=2.0, color=c) for c in colors_mvgmm]],
                            [["$nDim dims" for nDim in dims],
                            ["$nDim dims" for nDim in nDims]], 
                            ["IGMM", "MvGMM"], tellwidth=false, tellheight=false,
                            valign=:center, halign=:left)
            save(fpath, fig)
            show_figures && display(fig)
        end
    end

end