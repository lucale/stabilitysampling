include("../data/utils.jl")

show_figures = true
force_redraw = true
renewable_scenario = false

println("# Plotting $(renewable_scenario ? "renewable" : "conventional") scenario.")
if !renewable_scenario
    scenario = "de-elec-conventional"
    network = "elec_s_100_ec_lv1.0_5h"
else 
    scenario = "de-elec-renewable"
    network = "elec_s_100_ec_lvopt_5h"
end    

# Initialize output, data cache and images 
cachepath = joinpath(cache_folder, scenario, network)
savepath = joinpath(plots_folder, scenario, network)
if !isdir(savepath) mkpath(savepath) end

# Utility for creating figures
function create_figure(figfunc::F, path::String, params... ; force=false, show=false, kwargs...) where {F<:Function}
    if !isfile(path) || force
        fig = figfunc(params... ; kwargs... )
        save(path, fig)
        show && display(fig)
    elseif isfile(path) && show 
        img = rotr90(load(path))
        fig = Figure(; size=size(img))
        ax = Axis(fig[1,1])
        image!(ax, img)
        display(fig) 
    else 
        fig = nothing 
    end
    return fig
end

println("\nLoading $(scenario) / $(network) ")
@time n = load_network(scenario, network)

# Power Production 
println("# Plotting Power Production")
kwargs = (; force=force_recompute, show=show_figures)
@time create_figure( plot_yearly_production, joinpath(savepath, "production_yearly.png"), n; kwargs... )
@time create_figure( plot_production_capacities, joinpath(savepath, "production_capacities.png"), n; kwargs... )

# Load timeseries and train PCA
p = get_power_timeseries(n)
f = get_flow_timeseries(n)
p_mean = mean(p, dims=1)[1, :]
f_mean = mean(f, dims=1)[1, :]
@time create_figure( plot_power_flow, joinpath(savepath, "time_average_power_flow.png"), n, p_mean, f_mean; kwargs... )

print("Fitting PCA models ")
@time pca = PCA(p)
println("Plotting PCA results ")
@time create_figure( plot_pca_eigvals, joinpath(savepath, "pca_eigvals.png"), pca; kwargs... )
@time create_figure( plot_pca_pairplot, joinpath(savepath, "pca_pairplot.png"), pca; kwargs... )
@time create_figure( plot_pca_modes, joinpath(savepath, "pca_modes.png"), n, pca; kwargs... )
@time create_figure( plot_pca_coef_daytime, joinpath(savepath, "pca_coef.daytime.png"), pca, n.snapshots; kwargs... )
@time create_figure( plot_pca_coef_psd, joinpath(savepath, "pca_coef.psd.png"), pca, n.snapshots; kwargs... )

print("Fitting PCA models for flow")
@time pca_flow = PCA(f)
@time create_figure( plot_pca_pairplot, joinpath(savepath, "flow_pca_pairplot.png"), pca_flow; kwargs... ) 
@time create_figure( plot_pca_eigvals, joinpath(savepath, "flow_pca_eigvals.png"), pca_flow; kwargs... )
@time create_figure( plot_pca_coef_daytime, joinpath(savepath, "flow_pca_coef.daytime.png"), pca_flow, n.snapshots; kwargs... )
@time create_figure( plot_pca_coef_psd, joinpath(savepath, "flow_pca_coef.psd.png"), pca_flow, n.snapshots; kwargs... )


# Load QoI
println("# Plotting QoIs ")
qoi = deserialize(joinpath(cachepath, "qoi", "qoi.data.jld2"))
@time begin 
    
    fpath = joinpath(savepath, "qoi_Cred.data.png")
    if !isfile(fpath) || force_redraw
        fig,ax,plt = hist(qoi.Cred, bins=25, color=(:slategray, 0.5), normalization=:pdf)
        density!(ax, qoi.Cred; 
            color=(:white, 0.0), strokecolor=:black, strokewidth=1.5,
            bandwidth = 5.0, npoints = 10000,
            boundary = extrema(qoi.Cred) .+ (-100, 100)
        )
        xlims!(ax, -10.0, maximum(qoi.Cred) * 1.2)
        ax.xlabel = "Redundant Capacity [MW]"
        ax.ylabel = "Density"
        show_figures && display(fig)
        save(fpath, fig)
    end

    fpath = joinpath(savepath, "qoi_runi.data.png")
    if !isfile(fpath) || force_redraw
        fig,ax,plt = hist(qoi.runi, bins=25, color=(:slategray, 0.5), normalization=:pdf)
        density!(ax, qoi.runi; 
            color=(:white, 0.0), strokecolor=:black, strokewidth=1.5,
            bandwidth = 0.005, npoints = 10000,
            boundary = extrema(qoi.runi) .+ (-0.1, 0.1)
        )
        xlims!(ax, 0.85, 1.0)
        ax.xlabel = "Phase Coherence"
        ax.ylabel = "Density"
        show_figures && display(fig)
        save(fpath, fig)
    end

    fpath = joinpath(savepath, "qoi_S2.data.png")
    if !isfile(fpath) || force_redraw
        fig,ax,plt = hist(qoi.S2, bins=25, color=(:slategray, 0.5), normalization=:pdf)
        density!(ax, qoi.S2; 
            color=(:white, 0.0), strokecolor=:black, strokewidth=1.5,
            bandwidth = 1e-6, npoints = 10000,
            boundary = extrema(qoi.S2) .+ (-3e-6, 3e-6)
        )
        #xlims!(ax, 0.0, 1000.0)
        ax.xlabel = "Sync Norm"
        ax.ylabel = "Density"
        show_figures && display(fig)
        save(joinpath(savepath, "qoi_S2.data.png"), fig)
    end
end

