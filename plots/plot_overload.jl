include("../data/utils.jl")

force_redraw = true


function load_chain(renewable_scenario::Bool, model::String, dims::Int, β::Real, s::Real, idx::Int=1)
    if !renewable_scenario
        scenario = "de-elec-conventional"
        network = "elec_s_100_ec_lv1.0_5h"
    else 
        scenario = "de-elec-renewable"
        network = "elec_s_100_ec_lvopt_5h"
    end    
    fname = "chain_Cred.$(model).$(dims)dims.beta_$(β)_step_$(s).chain$(idx).jld2"
    fpath = joinpath(cache_folder, scenario, network, "chains", fname)
    if !ispath(fpath)
        error("File $fpath not found!")
    end
    @time chain = deserialize(fpath)
    n = size(chain.α,1)
    burnin = 0.5 ; thinning = 0.75 
    n0 = round(Int, n * burnin )
    idx = round.(Int, range(n0, n, round(Int, thinning * (n-n0))) )

    chain = (; 
        α = chain.α[idx, :], Cred = chain.Cred[idx],
        accepted=chain.accepted[idx], 
        β = chain.β, s = chain.s, 
    )
    return chain 
end

function load_network(renewable_scenario::Bool)
    if !renewable_scenario
        scenario = "de-elec-conventional"
        network = "elec_s_100_ec_lv1.0_5h"
    else 
        scenario = "de-elec-renewable"
        network = "elec_s_100_ec_lvopt_5h"
    end    
    load_network(scenario, network)    
end
function get_savepath(renewable_scenario::Bool, fname::String)
    if !renewable_scenario
        scenario = "de-elec-conventional"
        network = "elec_s_100_ec_lv1.0_5h"
    else 
        scenario = "de-elec-renewable"
        network = "elec_s_100_ec_lvopt_5h"
    end    
    return joinpath(plots_folder, scenario, network, fname)
end

function plot_overloads(n::Network, chains::Vector{<:NamedTuple})

    pca = PCA(get_power_timeseries(n))
    k = get_couplings(n)
    c = get_capacities(n)
    B = incidence_matrix(n)
    F = flow_matrix(B,k)


    Cred = getfield.(chains, :Cred)
    overload_rates = map(c -> mean(c .< 0.0), Cred) 
    println("Overload rate ", mean(overload_rates) * 100, " ± " , std(overload_rates) * 100, "%")
    
    println("Calculate overload frequencies")
    @time begin     
        α_overload = cat(map( c -> c.α[ c.Cred .< 0, :], chains )..., dims=1 )
        overload_probs = mean( ( c' .- transform(pca, α_overload) * F' ) .< 0, dims=1)[1,:]
    end
    
    println("Plotting overloads")
    @time begin
        fig = Figure()
        ax = geo_ax(fig[1,1],n)
        coords = get_bus_coords(n)
        scatter!(ax, coords[1], coords[2], markersize=5, color=:black)
        edgsrc = getindex.(argmax(B .== 1, dims=1)[1,:],1)
        edgdst = getindex.(argmax(B .==-1, dims=1)[1,:],1)
        for (e,(u,v)) in enumerate(zip(edgsrc,edgdst))
            has_overload = overload_probs[e] > 0.0
            lines!(ax, [ coords[1][u], coords[1][v] ], [ coords[2][u], coords[2][v] ],
                color = has_overload ? overload_probs[e] : :slategray,
                colormap = has_overload ? [:pink, :tomato, :darkred] : nothing,
                colorrange = has_overload ? extrema(overload_probs) : nothing,
                linewidth = has_overload ? 2.5 : 0.75,    
                #linestyle = has_overload ? (:dot, :dense) : :solid
            )
        end
        Colorbar(fig[1,2], label="Overload Probability", 
            colormap = [:pink, :tomato, :darkred],
            colorrange = extrema(overload_probs)
        )
        
    end
    return fig
end

function failure_probs(n::Network, model, chains::Vector{<:NamedTuple})
    Cred = vcat( getfield.(chains, :Cred) ... )
    α = vcat( getfield.(chains, :α) ... )
    overload = Cred .< 0
    pdf.()
    #overload = map(c -> c .< 0.0, Cred) 
end


println("\n#Renewable scenario")
println("Loading network")
@time begin
    n = load_network(true)
end

models = Dict(
    "mvgmm" => [
        (-0.001, 9) ,
        (-0.001, 55),
    ],
    "igmm" => [
        (-0.001, 9) ,
        (-0.001, 100),
    ]
)


for model in keys(model)
    for (β, ndim) in zip(models[model])
        chains = [ load_chain(true, model, ndim, β, 0.01, i) for i=1:5 ]
        joinpath(cache_folder, scenario, network, )
        #chains = [ load_chain(true, "igmm", 9, -0.001, 0.01, i) for i=1:5 ]
        fpath = get_savepath(true, "overloads.$(model).$(ndim).beta_$(β).png")
        if !ispath(fpath) || force_redraw
            fig = plot_overloads(n, chains)
            save(fpath, fig)
        end
    end
end


println("\n#Conventional scenario")
println("Loading network")
@time begin
    n = load_network(false)
end
chains = [ load_chain(false, "igmm", 25, -0.008, 0.01, i) for i=1:5 ]
fpath = get_savepath(false, "overloads.png")
if !ispath(fpath) || force_redraw
    fig = plot_overloads(n, chains)
    save(fpath, fig)
end
  