using Statistics 
using Distributions
using CairoMakie
using Parameters
using ProgressMeter
using Serialization
using DataFrames

using Revise
using StabilitySampling


scenario = "conventional"

data_path = joinpath(dirname(@__DIR__), "data", "pypsa-data", "serialized_networks", scenario * ".serial")
@unpack B,k,p,x,y = deserialize(data_path)

pca_path = joinpath(dirname(@__DIR__), "data", "cache", scenario, "models", "pca.serial")
pca = deserialize(pca_path)

println("\nCoordinates of buses ")
for i = 1:length(x)
    println(" Bus ", i, " : ", x[i], " ", y[i])
end

println("\nPrincipal components ")
ndim = 16
for i = 1:ndim 
    println(" PC ", i, " : ", pca.eigvecs[i,:])
end
