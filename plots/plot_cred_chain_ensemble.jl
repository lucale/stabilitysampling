using Statistics 
using Distributions
using CairoMakie
using Parameters
using ProgressMeter
using Serialization
using DataFrames

using Revise
using StabilitySampling


function get_chain_paths(scenario::String)
    cache_folder = joinpath(dirname(@__DIR__), "data", "cache")
    chain_folder = joinpath(cache_folder, scenario, "chains")
    model_folder = joinpath(cache_folder, scenario, "models")
    chain_files = filter(f->startswith(f, "chain_cred"), readdir(chain_folder)); sort!(chain_files)
    return joinpath.([chain_folder], chain_files)
end

function read_chain_data(chain_path::String)
    chain = deserialize(chain_path)
    α = chain.α; Cred = chain.Cred
    
    # Recalculate Cred with the correct model
    scenario = occursin("renewable", chain_path) ? "renewable" : "conventional"
    @unpack B,k,c = deserialize(joinpath(dirname(@DIR), "pypsa-data", "serialized_networks", "network.serial"))
    F = flow_matrix(B, k)
    pca = deserialize(joinpath(dirname(@DIR), "data", "cache", scenario, "models", "pca.serial"))
    Cred = redundant_capacity(Cred, transform(pca, α) * F')
    
    lags = length(Cred) .÷ ( 2 .^ collect(0: floor(Int, log2(length(Cred)))) ) 
    ac = autocor(Cred, lags)
    corrlength = findfirst( ac .< 0.01 )
    β = chain.parameters.β
    w = -β .* Cred ; w = w .- maximum(w); w = exp.(w) ; w = w ./ sum(w)    
    params = chain.parameters
    return (;
        acc_rate = mean(chain.accepted),        
        corr_length = isnothing(corrlength) ? Inf : corrlength,
        mean_cred =  sum( w .* Cred),
        std_cred = sqrt( sum( w .* (Cred .- mean(Cred)).^2)),
        mean_cred_β = mean(Cred),
        std_cred_β = std(Cred),
        P_fail = sum( w .* (Cred .< 0)),
        params ... 
    )

end

function get_chain_data()
    data = DataFrame()
    conventional_files = get_chain_paths("conventional")
    renewable_files    = get_chain_paths("renewable")
    data[!, :scenario] = vcat(repeat(["conventional"], length(conventional_files)), repeat(["renewable"], length(renewable_files)))
    data[!, :path] = vcat(conventional_files, renewable_files)

    println("Reading all chain data")
    @time chain_data = read_chain_data.(data.path)
    for field in keys(chain_data[1])
        data[!, field] = getfield.(chain_data, field)
    end
    return data
end

data = get_chain_data()


light_blue  = [0.4, 0.7, 1.0]
dark_blue   = [0.0, 0.2, 0.6]
light_green = [0.4, 1.0, 0.7]
dark_green  = [0.0, 0.4, 0.2]

for safety_margin in [0.05] #unique(data.safety_margin)
    for scenario in ["renewable", "conventional"]

    @unpack B,k,c = deserialize(joinpath(dirname(@DIR), "pypsa-data", "serialized_networks", "network.serial"))
    F = flow_matrix(B, k)
    pca = deserialize(joinpath(dirname(@DIR), "data", "cache", scenario, "models", "pca.serial"))
                
    experiment_selector = data.scenario .== scenario .&& data.safety_margin .== safety_margin .&& data.s .== 0.01
    fig_Cred = Figure(); ax_Cred = Axis(fig_Cred[1, 1])
    fig_acc = Figure(); ax_acc = Axis(fig_acc[1, 1])
    labels = Dict{String, Vector{Tuple{String,Makie.RGBAf}}}()
    β_lims = [Inf, -Inf]
    for model in ["igmm", "mvgmm"]
        all_ndim = unique(data[ experiment_selector .&& data.model .== model, :ndim]); sort!(all_ndim)
        if model == "igmm"
            colors = [ RGBAf( (light_blue .* t .+ dark_blue .* (1-t))... , 0.8) for t in range(0, 1, length(all_ndim)) ]
            labels["IGMM"] = [ zip(string.(all_ndim) .* " dims ", colors) ... ]
        elseif model == "mvgmm"
            colors = [ RGBAf( (light_green .* t .+ dark_green .* (1-t))... , 0.8) for t in range(0, 1, length(all_ndim)) ]
            labels["MvGMM"] = [ zip(string.(all_ndim) .* " dims ", colors) ... ]
        end

        for (color, ndim) in zip(colors, all_ndim) 
            model_selector = data.model .== model .&& data.ndim .== ndim 
            all_β = unique( data[ experiment_selector .&& model_selector, :β ]) ; sort!(all_β)   
            Cred_stats = (;
                mean = zeros(length(all_β)),
                std = zeros(length(all_β)),
            )
            acc_stats = (;
                mean = zeros(length(all_β)),
                std = zeros(length(all_β)),
            )
            for (i, β) in enumerate(all_β)
                selector = experiment_selector .&& model_selector .&& data.β .== β
                nchains = size(data[selector, :], 1)
                α = Matrix{Float64}[]
                Cred = Vector{Float64}[]
                acc = BitVector[]
                for i in 1:nchains
                    chain = deserialize(data[selector, :path][i])
                    push!(α, chain.α)
                    #push!(Cred, chain.Cred)
                    chain_Cred = redundant_capacity(Cred, transform(pca, chain.α) * F')
                    push!(Cred, chain_Cred)
                    push!(acc, chain.accepted)
                end

                Cred = vcat(Cred...)
                α    = vcat(α...)
                acc  = vcat(acc...)
                println("\nModel: $model, ndim: $ndim, β: $β, nchains: $nchains")
                println(" Cred = ", mean(Cred), " ± ", std(Cred))
                println(" acc = ", mean(acc), " ± ", std(acc))
                Cred_stats.mean[i] = mean(Cred)
                Cred_stats.std[i] = std(Cred)
                acc_stats.mean[i] = mean(acc)
                acc_stats.std[i] = std(acc)
            end
            band!(ax_Cred, all_β, Cred_stats.mean .- Cred_stats.std, Cred_stats.mean .+ Cred_stats.std; color = (color, 0.25))
            lines!(ax_Cred, all_β, Cred_stats.mean; color)
            #band!(ax_acc, all_β, acc_stats.mean .- acc_stats.std, acc_stats.mean .+ acc_stats.std; color = (color, 0.125))
            lines!(ax_acc, all_β, acc_stats.mean; color)
            β_lims[1] = min(β_lims[1], minimum(all_β))
            β_lims[2] = max(β_lims[2], maximum(all_β))
        end    
    end


    β_lims[1] = max( scenario == "renewable" ? -0.03 : -0.5,  β_lims[1])
    β_lims[2] = min( scenario == "renewable" ? -1e-3 : -1e-2,  β_lims[2])
    #pseudlogscale_tol = 1e-9
    #pseudolog10(x) = sign(x) * log10(abs(x) + pseudlogscale_tol)
    #pseudolog10_inv(x) = sign(x) * (10^abs(x) - pseudlogscale_tol)
    #psudolog10_scale = Makie.ReversibleScale(pseudolog10, pseudolog10_inv)

    pseudolog_scaling = 10
    pseudolog10(x) = sign(x) * log10( abs(pseudolog_scaling * x) + 1)
    pseudolog10_inv(x) = sign(x) * (10^abs(x) - 1) / pseudolog_scaling
    pseudolog10_scale = Makie.ReversibleScale(pseudolog10, pseudolog10_inv, limits=(-1e3, 1e3))

    for (fig,ax) in [(fig_acc, ax_acc), (fig_Cred, ax_Cred)]
        ax.title = "$(uppercasefirst(scenario)) scenario with " * ( safety_margin == Inf ?  "no" : "$(round(100 * safety_margin, sigdigits=4))%" ) * " operational bounds"
        ax.xlabel = "Genericity β"
        ax.xscale = pseudolog10_scale
        xlims!(ax, β_lims )

        Legend(fig[1, 2], 
            [
                    [LineElement(;color) for color in last.(labels["IGMM"])],
                    [LineElement(;color) for color in last.(labels["MvGMM"])],
            ],
            [ first.(labels["IGMM"]), first.(labels["MvGMM"]) ],
            ["IGMM", "MvGMM"] )
    end
    
    plots_folder = joinpath(@__DIR__, "images", scenario, "chains")
    ax_acc.ylabel = "Acceptance rate"
    ylims!(ax_acc, 0, 1)
    display(fig_acc)
    save( joinpath(plots_folder, "acceptance.safety_margin_$(safety_margin).png"), fig_acc)
    
    ax_Cred.ylabel = "Redundant Capacity [MW]"
    display(fig_Cred)
    save( joinpath(plots_folder, "Cred.safety_margin_$(safety_margin).png"), fig_Cred)

    end  
end