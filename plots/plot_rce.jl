include("../data/utils.jl")

renewable_scenario = false

if !renewable_scenario
    scenario = "de-elec-conventional"
    network = "elec_s_100_ec_lv1.0_5h"
else 
    scenario = "de-elec-renewable"
    network = "elec_s_100_ec_lvopt_5h"
end    
#q_contrib = [0.25, 0.5, 0.75, 0.9, 0.99, 0.999]
q_contrib = [0.9, 0.99, 0.999]

# Load network and data cache (=> run `plot_data.jl` first)
println("Loading $(scenario) / $(network) ")
@time n = load_network(scenario, network)
modelpath = joinpath(cache_folder, scenario, network, "models")

# Train PCA
print("Fitting PCA model ")
@time pca = PCA(get_power_timeseries(n))
α_data = project(pca, pca.signal)
bounds_a = minimum(α_data, dims=1)[1,:]
bounds_b = maximum(α_data, dims=1)[1,:]

# Load models
igmm = deserialize(joinpath(cache_folder, scenario, network, "models", "model.igmm.jld2"))
mvgmms = deserialize.(joinpath.([modelpath], filter(f->startswith(f, "model.mvgmm"), readdir(modelpath) ) ))
mvgmms = mvgmms[ sortperm(length.(mvgmms)) ]


N = length(pca)

### Redundant capacity 
β = [0, - 10 .^ collect(range(-3,-0.25,512)) ... ]
qmc_files = filter(f -> startswith(f, "Cred.qmc"), readdir(joinpath(cache_folder, scenario, network, "qoi")) ) 
qmc_dims = parse.(Int, replace.(map(f -> split(f, ".")[3], qmc_files), "dims"=>""))
idx = sortperm(qmc_dims); qmc_files = qmc_files[idx]; qmc_dims = qmc_dims[idx] 
data_qmc = map(f -> deserialize(joinpath(cache_folder, scenario, network, "qoi", f)), qmc_files)
α_qmc = getfield.(data_qmc, :α)
#α_qmc = [ project(pca, data_qmc[i].p)[:, 1:qmc_dims[i]] for i=1:length(data_qmc) ]


# MvGMM computations
E_min = 0.0; b_min = 0.0
b_crit_mvgmm = []; E_avg_mvgmm = []; E_std_mvgmm = []
n_contrib_mvgmm = zeros(length(mvgmms), length(q_contrib), length(β))
println("Calculating MvGMM Cred RCE statistics")
@showprogress for (i_mvgmm, i_qmc) in enumerate( indexin(length.(mvgmms), qmc_dims) )
    mvgmm = mvgmms[i_mvgmm]; qoi = data_qmc[i_qmc]; α = α_qmc[i_qmc]
    E     = qoi.Cred
    lp_bg = [ logpdf(mvgmm, α[i,:] ) for i=1:size(α,1) ] 
    lp_rce = lp_bg .+ E * β'     
    #=
    probs = exp.( lp_rce .- maximum( lp_rce, dims=2 ) ./ 2 )    
    probs = probs ./ sum(probs, dims=1)
    _E_avg = sum( probs .* E, dims=1)[1,:]
    _E_std = sqrt.( sum( probs .* (E .- _E_avg') .^ 2, dims=1)[1,:] )
    =#

    # Importance sampling
    
    C = prod(bounds_b[1:size(α,2)] .- bounds_a[1:size(α,2)])
    Z = mean(exp.(lp_rce), dims=1 )[1,:] .* C
    _E_avg = mean(E .* exp.(lp_rce) ./ Z', dims=1 )[1,:] .* C
    _E_var = mean( (E .- _E_avg') .^2 .* exp.(lp_rce) ./ Z', dims=1 )[1,:] .* C
    _E_std = sqrt.(_E_var)
    

    #=
    contrib = cumsum(sort(probs,dims=1,rev=true), dims=1)
    for (j,q) in enumerate(q_contrib)
        ncontrib = getindex.(argmax(contrib .>= q, dims=1)[1,:],1)
        n_contrib_mvgmm[i_mvgmm,j,:] = ncontrib
    end
    =#

    good_indices = findall( _E_avg .>= E_min ) 
    if length(good_indices) == 0
        _b_crit = 0.0
    else
        _b_crit = β[maximum(good_indices)]
        println("$(length(mvgmm)) dims, β_crit = ", _b_crit)
    end
    global b_min = min(b_min, _b_crit)
    push!(b_crit_mvgmm, _b_crit)
    push!(E_avg_mvgmm, _E_avg)
    push!(E_std_mvgmm, _E_std)
end

# IGMM computations
b_crit_igmm = []; E_avg_igmm = []; E_std_igmm = [] 
n_contrib_igmm = zeros(length(qmc_dims), length(q_contrib), length(β))
println("Calculating IGMM Cred RCE statistics")
@showprogress for (i,nDim) in enumerate(qmc_dims)
    igmm_submodel = Product(igmm.v[1:nDim]); 
    qoi = data_qmc[i]; α = α_qmc[i]
    E     = qoi.Cred
    lp_bg = [ logpdf(igmm_submodel, α[i,:] ) for i=1:size(α,1) ] 
    lp_rce = lp_bg .+ E * β'     
    probs = exp.( lp_rce .- maximum( lp_rce, dims=2 ) ./ 2 )    
    probs = probs ./ sum(probs, dims=1)

    contrib = cumsum(sort(probs,dims=1,rev=true),dims=1)
    for (j,q) in enumerate(q_contrib)
        ncontrib = getindex.(argmax(contrib .>= q, dims=1)[1,:],1)
        n_contrib_igmm[i,j,:] = ncontrib
    end

    _E_avg = sum( probs .* E, dims=1)[1,:]
    _E_std = sqrt.( sum( probs .* (E .- _E_avg') .^ 2, dims=1)[1,:] )

    good_indices = findall( _E_avg .>= E_min ) 
    if length(good_indices) == 0
        _b_crit = 0.0
    else
        _b_crit = β[maximum(good_indices)]
        println("$(nDim) dims, β_crit = ", _b_crit)
    end
    global b_min = min(b_min, _b_crit)
    push!(b_crit_igmm, _b_crit)
    push!(E_avg_igmm, _E_avg)
    push!(E_std_igmm, _E_std)
end


@showprogress for (j,q) in enumerate(q_contrib) 
    fig = Figure(size=(550, 600))
    ax = Axis(fig[1,1])
    ax.xlabel = "Genericity β"
    ax.ylabel = "Number of $(round(q*100, sigdigits=4))% contributing samples"

    for i =1:length(mvgmms)
        lines!(ax, β, n_contrib_mvgmm[i, j, :], color=i, colorrange=(1, length(mvgmms)), colormap=[:darkgreen, :lightgreen])
    end
    for i=[1,length(qmc_dims)]
        lines!(ax, β, n_contrib_igmm[i, j, :], color=i, colorrange=(1, length(qmc_dims)), colormap=[:darkblue, :cyan])
    end
    #ax.yscale = log10
    Legend(fig[1,1], 
        [[LineElement(color=i, colorrange=(1,length(mvgmms)), colormap=[:darkgreen, :lightgreen]) for i=1:length(mvgmms)],
         [LineElement(color=i, colorrange=(1,length(qmc_dims)), colormap=[:darkblue, :cyan]) for i=[1,length(qmc_dims)] ]],
        [ string.(length.(mvgmms)) .* " dims ", 
            string.(qmc_dims[[1, length(qmc_dims)]]) .* " dims" ],
        ["MvGMM", "IGMM"], valign=:top, halign=:left, tellwidth=false, tellheight=false)
    save(joinpath(plots_folder, scenario, network, "rce_stats.Cred.effective_samples.contrib$q.png"), fig)
end

begin 
    fig = Figure(size=(550, 600));
    ax = Axis(fig[1,1])
    ax.xlabel = "Genericity β"
    ax.ylabel = "Average Redundant Capacity [MW]"
    ylims!(ax, -200, 400)
    xlims!(ax, b_min * 1.25, 0.0  )
    #ax.xscale = log10
    colors_mvgmm = [ RGBAf(.15, .15 + t * .7, .15 ) for t in range(0,1, length(mvgmms)) ]
    for (i,mvgmm) in enumerate(mvgmms)
        plt = lines!(ax, β, E_avg_mvgmm[i], color=i, colorrange=(1,length(mvgmms)), colormap=[:darkgreen, :lightgreen], linewidth=.75)
        band!(ax, β, E_avg_mvgmm[i] .- E_std_mvgmm[i], E_avg_mvgmm[i] .+ E_std_mvgmm[i], color=colors_mvgmm[i], alpha=0.125)
    end 
    colors_igmm = [ RGBAf(.15, .15, .15 + t * .7) for t in range(0,1, length(qmc_dims)) ]
    for i = [1,length(qmc_dims)]
        plt = lines!(ax, β, E_avg_igmm[i], color=i, colorrange=(1,length(qmc_dims)), colormap=[:darkblue, :cyan], linewidth=.75)
        band!(ax, β, E_avg_igmm[i] .- E_std_igmm[i], E_avg_igmm[i] .+ E_std_igmm[i], color=colors_igmm[i],  alpha=0.125)
    end
    Legend(fig[1,1], 
        [[LineElement(color=i, colorrange=(1,length(mvgmms)), colormap=[:darkgreen, :lightgreen]) for i=1:length(mvgmms)],
        [LineElement(color=i, colorrange=(1,length(qmc_dims)), colormap=[:darkblue, :cyan]) for i=[1,length(qmc_dims)] ]],
        [string.(length.(mvgmms)) .* " dims, " .* string.(length.(getfield.(mvgmms, :components))) .* " components" .*
            "\nβ_crit = " .* string.(round.( b_crit_mvgmm, sigdigits=6 )),
        string.(qmc_dims[[1, length(qmc_dims)]]) .* " dims" .* 
            "\nβ_crit = " .* string.( round.( b_crit_igmm[[1, length(qmc_dims)]], sigdigits=6)) ],
        ["MvGMM", "IGMM" ], tellwidth=false, tellheight=false, valign=:top, halign=:left
        )
    save(joinpath(plots_folder, scenario, network, "rce_stats.Cred.png"), fig)
end


### Sync Norm
β = renewable_scenario ? [0, 10 .^ collect(range(-1,3.25,512)) ... ] : [0, 10 .^ collect(range(-1,2.75,512)) ... ]
#E_min = renewable_scenario ? 0.021 : 0.04 ; b_min = 0.0


qmc_files = filter(f -> startswith(f, "qoi.qmc"), readdir(joinpath(cache_folder, scenario, network, "qoi")) ) 
qmc_dims = parse.(Int, replace.(map(f -> split(f, ".")[3], qmc_files), "dims"=>""))
idx = sortperm(qmc_dims); qmc_files = qmc_files[idx]; qmc_dims = qmc_dims[idx] 
data_qmc = map(f -> deserialize(joinpath(cache_folder, scenario, network, "qoi", f)), qmc_files)
α_qmc = [ project(pca, data_qmc[i].p)[:, 1:qmc_dims[i]] for i=1:length(data_qmc) ]

# MvGMM computations
b_crit_mvgmm = []; E_avg_mvgmm = []; E_std_mvgmm = []
n_contrib_mvgmm = zeros(length(mvgmms), length(q_contrib), length(β))
for (i_mvgmm, i_qmc) in enumerate( indexin(length.(mvgmms), qmc_dims) )
    mvgmm = mvgmms[i_mvgmm]; qoi = qoi_qmc[i_qmc]; α = α_qmc[i_qmc]
    E     = qoi.S2
    lp_bg = [ logpdf(mvgmm, α[i,:] ) for i=1:size(α,1) ] 
    lp_rce = lp_bg .+ E * β'     
    probs = exp.( lp_rce .- maximum( lp_rce, dims=2 ) )    
    probs = probs ./ sum(probs, dims=1)
    _E_avg = sum( probs .* E, dims=1)[1,:]
    _E_std = sqrt.( sum( probs .* (E .- _E_avg') .^ 2, dims=1)[1,:] )


    contrib = cumsum(sort(probs,dims=1,rev=true), dims=1)
    for (j,q) in enumerate(q_contrib)
        ncontrib = getindex.(argmax(contrib .>= q, dims=1)[1,:],1)
        n_contrib_mvgmm[i_mvgmm,j,:] = ncontrib
    end
    #=good_indices = findall( _E_avg .>= E_min ) 
    if length(good_indices) == 0
        _b_crit = 0.0
    else
        _b_crit = β[maximum(good_indices)]
    end
    global b_min = min(b_min, _b_crit)
    push!(b_crit_mvgmm, _b_crit)
    =#
    push!(E_avg_mvgmm, _E_avg)
    push!(E_std_mvgmm, _E_std)
end

# IGMM computations
b_crit_igmm = []; E_avg_igmm = []; E_std_igmm = []
n_contrib_igmm = zeros(length(qmc_dims), length(q_contrib), length(β))
for (i,nDim) in enumerate(qmc_dims)
    igmm_submodel = Product(igmm.v[1:nDim]); 
    qoi = qoi_qmc[i]; α = α_qmc[i]
    E     = qoi.S2
    lp_bg = [ logpdf(igmm_submodel, α[i,:] ) for i=1:size(α,1) ] 
    lp_rce = lp_bg .+ E * β'     
    probs = exp.( lp_rce .- maximum( lp_rce, dims=2 )  )    
    probs = probs ./ sum(probs, dims=1)
    _E_avg = sum( probs .* E, dims=1)[1,:]
    _E_std = sqrt.( sum( probs .* (E .- _E_avg') .^ 2, dims=1)[1,:] )

    contrib = cumsum(sort(probs,dims=1,rev=true),dims=1)
    for (j,q) in enumerate(q_contrib)
        ncontrib = getindex.(argmax(contrib .>= q, dims=1)[1,:],1)
        n_contrib_igmm[i,j,:] = ncontrib
    end
    #=good_indices = findall( _E_avg .>= E_min ) 
    if length(good_indices) == 0
        _b_crit = 0.0
    else
        _b_crit = β[maximum(good_indices)]
    end
    global b_min = min(b_min, _b_crit)
    push!(b_crit_igmm, _b_crit) 
    =#
    push!(E_avg_igmm, _E_avg)
    push!(E_std_igmm, _E_std)
end



begin 
    fig = Figure(size=(550, 600));
    ax = Axis(fig[1,1])
    ax.xlabel = "Genericity β"
    ax.ylabel = "Average Sync Norm"
    #ylims!(ax, 0.6, 1.0)
    #xlims!(ax, 0, renewable_scenario ? -b_min * 1.75 : 100  )
    #ax.xscale = log10
    colors_mvgmm = [ RGBAf(.15, .15 + t * .7, .15 ) for t in range(0,1, length(mvgmms)) ]
    for (i,mvgmm) in enumerate(mvgmms)
        lines!(ax, β, E_avg_mvgmm[i], color=i, colorrange=(1,length(mvgmms)), colormap=[:darkgreen, :lightgreen], linewidth=.75)
        band!(ax, β, E_avg_mvgmm[i] .- E_std_mvgmm[i], E_avg_mvgmm[i] .+ E_std_mvgmm[i], color=colors_mvgmm[i], alpha=0.125)
    end
    colors_igmm = [ RGBAf(.15, .15, .15 + t * .7) for t in range(0,1, length(qmc_dims)) ]
    for i = [1,length(qmc_dims)]
        lines!(ax, β, E_avg_igmm[i], color=i, colorrange=(1,length(qmc_dims)), colormap=[:darkblue, :cyan], linewidth=.75)
        band!(ax, β, E_avg_igmm[i] .- E_std_igmm[i], E_avg_igmm[i] .+ E_std_igmm[i], color=colors_igmm[i],  alpha=0.125)
    end
    Legend(fig[1,1], 
        [[LineElement(color=i, colorrange=(1,length(mvgmms)), colormap=[:darkgreen, :lightgreen]) for i=1:length(mvgmms)],
        [LineElement(color=i, colorrange=(1,length(qmc_dims)), colormap=[:darkblue, :cyan]) for i=[1,length(qmc_dims)] ]],
        [string.(length.(mvgmms)) .* " dims, " .* string.(length.(getfield.(mvgmms, :components))) .* " components" #.*
            , #"\nβ_crit = " .* string.(round.( .- b_crit_mvgmm, sigdigits=6 )),
        string.(qmc_dims[[1, length(qmc_dims)]]) .* " dims" # .* 
            ], #"\nβ_crit = " .* string.( round.( .- b_crit_igmm[[1, length(qmc_dims)]], sigdigits=6)) ],
        ["MvGMM", "IGMM" ], tellwidth=false, tellheight=false, valign=:top, halign=:left
        )
    save(joinpath(plots_folder, scenario, network, "rce_stats.S2.png"), fig)
end

