# StabilitySampling

Software alongside the Master Thesis 
  "Sampling Challenging Injection Patterns for the German Power Grid"

What it includes 
- Julia wrapper for PyPSA networks
- Background stochastical model PCA GMM
- Swing equations and stability measures
- Plotting utilities 

Julia package for using MCMC sampling methods to find instable power injection patterns. 



### Depedencies

Local PyPSA installation. All PyPSA computations are done here.

From Julia we might call `pypsa` from PyCall, however this will only be a light-weight PyPSA installation for reading the results.

The `*.nc` NetCDF files are created with PyPSA and loaded as `Network`.

## Installation

Install the [StabilitySampling.jl](https://gitlab.pik-potsdam.de/lucale/stabilitysampling) package by running

```julia
using Pkg; Pkg.add("https://gitlab.pik-potsdam.de/lucale/stabilitysampling")
```

## How to get input data 

# Run the PyPSA solver
In the `data/pypsa-config` folder are the `*.yaml` configuration files. 
These can be copied to the `pypsa-eur/config` folder. 

If not done so already change to the PyPSA  
```
conda activate pypsa-eur
```

Run the follow command
```
snakemake -call solve_elec_networks --configfile config/config.de-elec-conventional.yaml
```


<!-- 
add link to master thesis 
-->

## How to cite 

```
@software{ stability_sampling_2024,
  author = {Lenz, Luca},
  title = {{StabilitySampling.jl}},
  url = {https://gitlab.pik-potsdam.de/lucale/stabilitysampling},
  year = {2024}
  month = {10}
}
```
