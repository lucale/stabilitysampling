include("utils.jl")

n_qmc_Cred = 1_000_000
n_qmc_S2 = 10_000
force_recompute = true
renewable_scenario = true

if !renewable_scenario
    scenario = "de-elec-conventional"
    network = "elec_s_100_ec_lv1.0_5h"
else 
    scenario = "de-elec-renewable"
    network = "elec_s_100_ec_lvopt_5h"
end    


println("Loading $(renewable_scenario ? "renewable" : "conventional") scenario.")
@time n = load_network(scenario, network)

println("Calculating PCA")
@time begin
    p_data = get_power_timeseries(n)
    pca = PCA(p_data)
end

# Load data QoIs or recalculate them
fpath = joinpath(cache_folder, scenario, network, "qoi", "qoi.data.jld2")
if isfile(fpath) #&& !force_recompute
    qoi_data = deserialize(fpath)
else
    println("# Calculating QoIs for data")
    @time qoi_data = calculate_QoI(n, p_data, keepsync=true)
    serialize(fpath, qoi_data)
end


α = project(pca, pca.signal)
a = minimum(α, dims=1)[1,:]
b = maximum(α, dims=1)[1,:]

k = get_couplings(n)
c = get_capacities(n)
B = incidence_matrix(n)
F = flow_matrix(B,k)
d = get_dampings(n)
m_inv = 1 ./ get_inertias(n)


## Redundant capacity 
N = nv(n)
P = primes(N^2+1)[1:N]
ξ = stack(Halton.(P, length=n_qmc_Cred), dims=2)
α_qmc = ξ .* (b .- a)' .+ a'

println("\n# Sampling Redundant Capacity")
rs = [0.25, 0.5, 0.75, 0.9, 0.95, 0.99, 1.0]
for r in rs
    nDim = relevant_dims(pca, r)
    fpath = joinpath(cache_folder, scenario, network, "qoi", "Cred.qmc.$(nDim)dims.jld2")
    if ispath(fpath) && !force_recompute
        data = deserialize(fpath)
    else 
        println("Sampling for $nDim dimensions.")
        @time begin 
            α = α_qmc[:, 1:nDim]
            α = sortslices(α, dims=1)
            p = transform(pca, α)
            Cred = redundant_capacity(c, p * F')
            data = (; α, Cred )
            serialize(fpath, data)
        end
    end
end


## Linearized sync norm
N = nv(n)
P = primes(N^2+1)[1:N]
ξ = stack(Halton.(P, length=n_qmc_S2), dims=2)
α_qmc = ξ .* (b .- a)' .+ a'

println("\n# Sampling Linear Sync Norm")
for r in rs 
    nDim = relevant_dims(pca, r)
    fpath = joinpath(cache_folder, scenario, network, "qoi", "S2.qmc.$(nDim)dims.jld2")
    if ispath(fpath) && !force_recompute
        data = deserialize(fpath)
    else 
        println("Sampling for $nDim dimensions.")
        @time begin 
            α = α_qmc[:, 1:nDim]
            α = sortslices(α, dims=1)
            p = transform(pca, α)
            u0 = synchronous_state(B,k,p;d,m_inv,keepsync=true)
            S2 = zeros( size(u0,1) )
            @showprogress for i=1:length(S2)
                X = lyapunov_matrix(B,k,u0[i,:];d,m_inv)
                S2[i] = sum( diag( X[N:end, N:end] ) )
            end
            data = (; α, S2 )
            serialize(fpath, data)
        end
    end
end
