#! /bin/sh

nbatches=30
nthreads=1

echo "Finding the total number of runs."
ntotal=$(($(julia --project=.. sample_cred_chains.jl 1 0 | awk '{print $3}')))

# Calculate the number of full batches
nbatch=$(($ntotal / $nbatches))
echo "Starting tmux sessions for $ntotal runs in $nbatches batches of size $nbatch"

# Loop over the full batches
for i in $(seq 1 $nbatches); do
  i1=$((nbatch*($i-1)+1))
  i2=$((nbatch*$i))
  echo "Starting session $i: for runs $i1 to $i2"
  tmux new-session -d -s "$i" "julia --project=.. -t $nthreads sample_cred_chains.jl $i1 $i2 | tee logfiles/sample_cred_chains.batch_$i.txt"
done

# Handle remaining runs, if any
i1=$((nbatches * nbatch + 1))
i2=$ntotal
if [ $i1 -le $i2 ]; then  # Use single square brackets for sh
  i=$(($nbatches + 1))
  echo "Starting session $i: for runs $i1 to $i2"
  tmux new-session -d -s "$i" "julia --project=.. -t $nthreads sample_cred_chains.jl $i1 $i2 | tee logfiles/sample_cred_chains.batch_$i.txt"
fi
