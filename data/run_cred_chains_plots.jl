using Graphs
using Statistics, Distributions
using CairoMakie
using LinearAlgebra
using ProgressMeter
using Serialization
using Random, Primes, HaltonSequences
using FileIO
using Base.Threads
using Test

using Revise 
using StabilitySampling

force_redraw = true 

# List of all parameters
nsample = 20_000_000
nchains = nthreads()   
experiments = []
for scenario in ["renewable", "conventional"]
    for model in ["igmm", "mvgmm"]
        all_relevances = [0.25, 0.5, 0.75, 0.9]
        #if model == "igmm" append!(all_relevances, [0.9]) end
        for relevance in all_relevances
            all_β = scenario == "renewable" ? [-0.001, -0.002] : [-.0008, -0.009]
            for β in all_β
                for stepsize in [0.01]
                    push!(experiments, (; scenario, relevance, model, β, stepsize))
                end
            end
        end
    end
end

function analyze_chains(n::Network, pca::PCA, chains::Vector{<:NamedTuple}, model::String, β::Float64, relevance::Float64, stepsize::Float64, ; burnin=0.5, thinning=0.75, savepath::String)
    
    info = "Chains for $(uppercase(model)) $(relevant_dims(pca, relevance)) dims (= $(relevance * 100) covariance) with β = $(β), stepsize = $(100 * stepsize)%"
    println("Info $info")

    # clean chains     
    n_chain = length(chains[1].Cred)
    n_burnin = ceil(Int, n_chain * burnin)
    idx_clean = round.(Int, collect( range(n_burnin,n_chain, length=ceil(Int, (1-thinning)*(n_chain-n_burnin)) ) ) )

    # Acceptance rates
    acc_rates = map( chain -> mean(chain.accepted), chains ) 
    info *= "\nAcceptance $(round( 100*mean(acc_rates), sigdigits=4)) ± $(round( 100*std(acc_rates), sigdigits=4)) %"
    acc_rates_processed = map( chain -> mean(chain.accepted[idx_clean]), chains )
    info *= "\nProcessed Acc. $(round( 100*mean(acc_rates_processed), sigdigits=4)) ± $(round( 100*std(acc_rates_processed), sigdigits=4)) %"
    
    # Calculate redundant capacity statistics
    Cred = map( chain -> chain.Cred[idx_clean], chains )
    μ = mean.(Cred) ; σ = std.(Cred)
    info *= """
    Redundant Capacity 
        Mean $(round(mean(μ), sigdigits=4)) ± $(round(std(μ), sigdigits=4)) MW"
        Std $(round(mean(σ), sigdigits=4)) ± $(round(std(σ), sigdigits=4)) MW"
    """

    # Calculate overload probability
    P_overload = zeros(length(chains)); R_overload = zeros(length(chains))
    for i=1:length(chains)
        w = β .* Cred[i]; w = w .- maximum(w) ; w = exp.(w) ; w = w ./ sum(w)
        P_overload[i] = sum( w .* ( Cred[i] .< 0 ) )
        R_overload[i] = mean(Cred[i] .< 0)
    end
    info *= """
    Overload 
        Probability $(round( 100*mean(P_overload), sigdigits=4)) ± $(round( 100*std(P_overload), sigdigits=4)) %"
        Sample ratio $(round( 100*mean(R_overload), sigdigits=4)) ± $(round( 100*std(R_overload), sigdigits=4)) %"
        Conditional probability per line: 
    """

    prob = map( chain->exp.( chain.lpbg[idx_clean] ), chains )
    most_likely_failures = map( i -> argmax( prob[i] .* (Cred[i] .< 0) ), 1:length(chains) )

    # Calculate conditional overload probability
    α = map( chain -> chain.α[idx_clean, :], chains )
    α_overload = vcat( map( i->α[i][ Cred[i] .< 0, :], 1:length(chains) ) ... )
    c = get_capacities(n)
    k = get_couplings(n)
    B = incidence_matrix(n)
    F = flow_matrix(B,k)
    overload_probs = mean( ( c' .- transform(pca, α_overload) * F' ) .< 0, dims=1)[1,:]
    for overload_idx in findall(overload_probs .> 0)
        info *= "     Line $overload_idx: $(overload_probs[overload_idx])\n"
    end

    # Plotting utilties
    chain_colors = [ RGBAf(0.5 + 0.5 * t, 0.5 * t, 1 - t )  for t in range(0,1,step=1/length(chains))]
    if !isdir(dirname(savepath)) mkpath(dirname(savepath)) end
    lags = ceil.(Int, 2 .^ collect( 1:.5:ceil(Int, log2(length(Cred[1]))) ))
    if !isdir(dirname(savepath)) mkpath(dirname(savepath)) end
    
    # Save info
    @time begin 
        println("Saving info")
        open(replace(savepath, "NAME" => "info") * ".txt", "w") do f 
            write(f, info)
        end
    end

    # Plot trajectories
    @time begin 
        println("Plotting trajectories")
        fig = Figure(); ax = Axis(fig[1,1])
        ax.xlabel = "Iteration"
        ax.ylabel = "Redundant Capacity [MW]"
        xlims!(ax, (0, length(Cred[1])))
        for i=1:length(chains)
            lines!(ax, 1:length(Cred[i]), Cred[i]; color=(chain_colors[i], 0.75))
        end
        ax.title = "Acceptance rate $(round( 100*mean(acc_rates), sigdigits=4)) ± $(round( 100*std(acc_rates), sigdigits=4)) %"
        save(replace(savepath, "NAME" => "trajectory") * ".png", fig)
    end
    
    # Plot densities
    @time begin
        println("Plotting densities")
        fig = Figure(); ax = Axis(fig[1,1])
        ax.xlabel = "Redundant Capacity [MW]"
        ax.ylabel = "Density"
        for i=1:length(chains)
            density!(ax, Cred[i]; color=(:white,0.0), strokecolor=(chain_colors[i], 0.75), strokewidth=1.0)
        end 
        ax.title = "Mean Redundant Capacity $(round(mean(μ), sigdigits=4)) ± $(round(std(μ), sigdigits=4)) MW"
        save(replace(savepath, "NAME" => "density") * ".png", fig)
    end

    # Plot PCA space
    @time begin 
        println("Plotting PCA coefs")
        α_data = project(pca, pca.signal)
        ndim = min(size(chains[1].α,2),  5)
        lims = extrema(α_data[:,1:ndim], dims=1)[1,:]
        labels = "PC" .* string.(1:ndim)
        fig = pairplot( α_data[:, 1:ndim], labels)
        
        reducestep = length(α[1])÷(500 * length(α)) 
        α_reduced = vcat( map(α -> α[size(α,1)÷2,1:ndim][1:reducestep:end,:], α) ... )
        pairplot!(fig, α_reduced, labels; lims, densitycolor=:red, histcolor=(:white, 0.0), 
                        contourcolormap=[:orange, :red, :darkred], hexbincolormap=[(:white,0.0)])
        save(replace(savepath, "NAME" => "pcaspace") * ".png", fig)
    end

    # Plot correlation length
    @time begin 
        println("Plotting Cred correlation lengths")
        fig = Figure(); ax = Axis(fig[1,1])
        ax.xlabel = "Lag"
        ax.ylabel = "Autocorrelation"
        for i=1:length(chains)
            ac = autocor(Cred[i], lags)
            lines!(ax, lags, ac; color=(chain_colors[i], 0.75))
        end
        ax.xscale = log10
        ax.title = "Autocorrelation: Redundant Capacity"
        xlims!(ax, extrema(lags))   
        save(replace(savepath, "NAME" => "autocorrelation_Cred") * ".png", fig)
    end
    for idim=1:ndim 
        @time begin
            println("Plotting PC $idim correlation lengths") 
            fig = Figure(); ax = Axis(fig[1,1])
            ax.xlabel = "Lag"
            ax.ylabel = "Autocorrelation"
            for i=1:length(chains)
                ac = autocor(α[i][:,idim], lags)
                lines!(ax, lags, ac; color=(chain_colors[i], 0.75))
            end
            ax.xscale = log10
            ax.title = "Autocorrelation: PC $idim"
            xlims!(ax, extrema(lags))   
            save(replace(savepath, "NAME" => "autocorrelation_PC$idim") * ".png", fig)
        end
    end

    # Plot overload probability per line
    @time begin
        println("Plotting overload probability")        
        fig = Figure()
        ax = geo_ax(fig[1,1],n)
        coords = get_bus_coords(n)
        scatter!(ax, coords[1], coords[2], markersize=5, color=:black)
        edgsrc = getindex.(argmax(B .== 1, dims=1)[1,:],1)
        edgdst = getindex.(argmax(B .==-1, dims=1)[1,:],1)
        for (e,(u,v)) in enumerate(zip(edgsrc,edgdst))
            has_overload = overload_probs[e] > 0.0
            lines!(ax, [ coords[1][u], coords[1][v] ], [ coords[2][u], coords[2][v] ],
                color = has_overload ? overload_probs[e] : :slategray,
                colormap = has_overload ? [:pink, :tomato, :darkred] : nothing,
                colorrange = has_overload ? extrema(overload_probs) : nothing,
                linewidth = has_overload ? 2.5 : 0.75,    
                #linestyle = has_overload ? (:dot, :dense) : :solid
            )
        end
        if any(overload_probs .> 0)
            Colorbar(fig[1,2], label="Conditional overload probability", 
                colormap = [:pink, :tomato, :darkred],
                colorrange = extrema(overload_probs)
            )
            ax.title = "Overload probability $(round( 100*mean(P_overload), sigdigits=4)) ± $(round( 100*std(P_overload), sigdigits=4)) %"
        end
        save(replace(savepath, "NAME" => "overload") * ".png", fig)
    end

    @time begin
        println("Plotting most likely failures")
        for i=1:length(chains)
            p = transform( pca, α[i][most_likely_failures[i], :] )
            f = F * p
            fig = plot_power_flow(n, p, f; B)
            save(replace(savepath, "NAME" => "most_likely_failure_chain$i") * ".png", fig)
        end 
    end
end



function run_experiment(; scenario, relevance, model, β, stepsize, progress=nothing, 
    burnin = 0.5, thinning = 0.75, 
    kwargs...)

    println("Running $scenario scenario on $(uppercase(model)) $(relevance)cov with β = $β, stepsize = $(100 * stepsize)%")
    
    # Skip if already done
    savepath = joinpath(dirname(@__DIR__), "plots", "experiments", scenario, "$(model)_explaining_$relevance", "beta_$(β)_step_$(stepsize)", "NAME" )
    incomplete = !isdir(dirname(savepath)) || !isfile( replace(savepath, "NAME" => "info") * ".txt" )
    for name in ["trajectory", "density", "pcaspace", "corlength", "overload"]
        incomplete = incomplete || !isfile( replace(savepath, "NAME" => name) * ".png" )
    end
    if !incomplete && !force_redraw return end 

    # Load network
    println("Load network")
    if scenario == "renewable"
        scenario = "de-elec-renewable"
        network = "elec_s_100_ec_lvopt_5h"
    elseif scenario == "conventional"
        scenario = "de-elec-conventional"
        network = "elec_s_100_ec_lv1.0_5h"
    end
    @time n = Network(joinpath(@__DIR__, "pypsa-results", scenario, "networks", network * ".nc"))

    # Construct PCA
    println("Train PCA")
    @time begin 
        pca = PCA(get_power_timeseries(n))
        α_data = project(pca, pca.signal)
    end
    ndim = relevant_dims(pca, relevance)
    lower_bounds = minimum(α_data[:,1:ndim], dims=1)[1,:]
    upper_bounds = maximum(α_data[:,1:ndim], dims=1)[1,:]

    # Load model
    modelpath =  joinpath(@__DIR__, "cache", scenario, network, "models", "model.$model" * (model == "mvgmm" ? ".$(ndim)dims" : "") * ".jld2")
    if ( !ispath(modelpath) ) println("Model `$modelpath` not found"); return; end
    ψ = deserialize(modelpath)
    if (model == "igmm") ψ = Product(ψ.v[1:ndim]) end

    # Network parameters
    c = get_capacities(n)
    k = get_couplings(n)
    B = incidence_matrix(n)
    F = flow_matrix(B,k)

    # sample chains
    logp_bg(α) = logpdf(ψ, α)
    logp_target(α) = redundant_capacity(c, F * transform(pca, α))
    α0 = zeros(ndim)
    chains = NamedTuple[]
    progress = Progress(nthreads() * nsample)
    println("Sampling Chains")
    @time @threads for i=1:nthreads()
        chain = rce_rwm(
            logp_bg, logp_target, nsample, α0, β;
            lower_bounds, upper_bounds, 
            stepsize = ( upper_bounds .- lower_bounds) .* stepsize,
            save_every = 100, progress
        ) 
        #chain = (; Cred = redundant_capacity(c, F * transform(pca, chain)), α = chain, accepted = chain.accepted )
        chain = (; 
            Cred =chain.target, 
            lpbg = chain.background,  
            α = chain.X, 
            accepted=chain.accepted,
            #β=chain.β, 
            #relevance, model, stepsize
        )
        push!(chains, chain)
    end

    # save results
    analyze_chains(n, pca, chains, model, β, relevance, stepsize; burnin, thinning, savepath)
end


# loop over all experiments
#=
experiments_per_thread = length(experiments) ÷ nthreads()
experiments_rest = length(experiments) % nthreads()
progress = Progress(length(experiments) * nsample * nchains)    
@threads for ithread = 1:nthreads()
    imin = (ithread-1) * experiments_per_thread + 1
    imax = ithread * experiments_per_thread + (ithread == nthreads() ? experiments_rest : 0)
    for i in imin:imax    
        run_experiment(; #=progress,=# experiments[i]...)
    end
end
=#

i1 = 1; i2 = length(experiments)
if length(ARGS) > 0
    i1 = parse(Int, ARGS[1])
    i2 = parse(Int, ARGS[2])
end

for i=i1:i2
    println("\n### Running experiment $i / $(length(experiments)) " * 
        ( i1!=1 || i2 != length(experiments) ? " (run reduced from $i1 to $i2)" : "" ) )
    experiment = experiments[i]
    run_experiment(; experiment...)
end


