include("utils.jl")


# === Load network 
npoints = 100
nsample_Cred = 10_000
nsample_S2   = 100
nDim = 16

force_resample = true
renewable_scenario = true

if !renewable_scenario
    scenario = "de-elec-conventional"
    network = "elec_s_100_ec_lv1.0_5h"
else 
    scenario = "de-elec-renewable"
    network = "elec_s_100_ec_lvopt_5h"
end   
println("Loading $(renewable_scenario ? "renewable" : "conventional" ) scenario.") 
@time n = load_network(scenario, network)
savepath = joinpath(cache_folder, scenario, network, "conditionals")
if !ispath(savepath) mkpath(savepath) end
modelpath = joinpath(cache_folder, scenario, network, "models")

println("Fitting PCA model")
@time pca = PCA(get_power_timeseries(n))
α_data = project(pca, pca.signal)
bounds = extrema(α_data, dims=1)[1,:]
t = [ collect(range(bnd..., npoints)) for bnd in bounds ]


# Load IGMM model (=> run `fit_models.jl` first)
igmm = deserialize(joinpath(modelpath, "model.igmm.jld2"))

fpath = joinpath(savepath, "conditionals_Cred.igmm.$(length(igmm))dims.jld2")
if isfile(fpath) && !force_recompute
    t_igmm, cond_stats_igmm = deserialize(fpath)
else 
    println("# Calculate Cred conditional statistics")
    cond_stats = (; 
        μ_Cred = zeros(nDim, npoints), σ_Cred = zeros(nDim, npoints), 
        #μ_S2   = zeros(nDim, npoints), σ_S2   = zeros(nDim, npoints),
    )
                
    N = nv(n)
    c = get_capacities(n)
    k = get_couplings(n)
    m = get_inertias(n); m_inv = 1 ./ m
    d = get_dampings(n)
    B = incidence_matrix(n)
    F = flow_matrix(B, k)
            
    @showprogress for i=0:nDim*npoints-1
        k = i ÷ npoints + 1; l = i % npoints + 1
        α_s = Matrix(rand(igmm, nsample_Cred)')
        α_s[:, k] .= t[k][l]
        p = transform(pca, α_s)

        f = p * F'
        Cred = redundant_capacity(c, f)
        cond_stats.μ_Cred[k,l] = mean(Cred)
        cond_stats.σ_Cred[k,l] = std(Cred)
    end

    #=
    println("Calculating S2 conditional statistics")
    @showprogress for i=0:nDim*npoints-1
        k = i ÷ npoints + 1; l = i % npoints + 1
        α_s = Matrix(rand(igmm, nsample_S2)')
        α_s[:, k] .= t[k][l]
        p = transform(pca, α_s)

        X = [ lyapunov_matrix(B, k, p[i,:]; d, m_inv) for i=1:size(p,1) ]
        S2 = [ X[i][N:end, N:end] for i=1:size(p,1) ]
        cond_stats.μ_S2[k,l] = mean(S2)
        cond_stats.σ_S2[k,l] = std(S2)
    end
    =#

    serialize(fpath, (t, cond_stats) )
    t_igmm = t ; cond_stats_igmm = cond_stats
end


# Load MvGMM model (=> run `fit_models.jl` first)
mvgmms = deserialize.(joinpath.([modelpath], filter(f->startswith(f, "model.mvgmm"), readdir(modelpath) ) ))
mvgmm = mvgmms[ sortperm(length.(mvgmms)) ][end]

c = conditionals(mvgmm)

#=
fpath = joinpath(cache_folder, scenario, network, "conditionals_mvgmm.$(length(mvgmm))dims.Cred.jld2")
if isfile(fpath) && !force_recompute
    t_mvgmm, cond_stats_mvgmm = deserialize(fpath)
else 
    println("Calculate QoI marginals")
    cond_stats = (; 
        μ_Cred = zeros(nDim, npoints), σ_Cred = zeros(nDim, npoints), 
        μ_S2   = zeros(nDim, npoints), σ_S2   = zeros(nDim, npoints),
    )

    N = nv(n)
    c = get_capacities(n)
    k = get_couplings(n)
    m = get_inertias(n); m_inv = 1 ./ m
    d = get_dampings(n)
    B = incidence_matrix(n)
    F = flow_matrix(B, k)
            
    println("Calculating Cred conditional statistics")
    @showprogress for k=1:nDim
        for l in 1:npoints
            α_s = Matrix(rand(igmm, nsample_Cred)')
            α_s[:, k] .= t[k][l]
            p = transform(pca, α_s)

            f = p * F'
            Cred = redundant_capacity(c, f)
            cond_stats.μ_Cred[k,l] = mean(Cred)
            cond_stats.σ_Cred[k,l] = std(Cred)
        end
    end

    println("Calculating S2 conditional statistics")
    @showprogress for k=1:nDim
        for l in 1:npoints
            α_s = Matrix(rand(igmm, nsample_S2)')
            α_s[:, k] .= t[k][l]
            p = transform(pca, α_s)

            X = [ lyapunov_matrix(B, k, p[i,:]; d, m_inv) for i=1:size(p,1) ]
            S2 = [ X[i][N:end, N:end] for i=1:size(p,1) ]
            cond_stats.μ_S2[k,l] = mean(S2)
            cond_stats.σ_S2[k,l] = std(S2)
        end
    end

    serialize(fpath, (t, cond_stats) )     
    t_mvgmm = t ; cond_stats_mvgmm = cond_stats 
end
=#