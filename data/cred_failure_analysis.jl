using Serialization
using Distributions
using Parameters
using ProgressMeter

using CairoMakie 

using Revise 
using StabilitySampling 

n_sample = 1_000_000
force_recompute = false
safety_margin = 0.05


scenario = "renewable"
println("\n## Scenario: $scenario")

# Paths 
model_path = joinpath(@__DIR__, "cache", scenario, "models", "MODEL.serial")
params_path = joinpath(@__DIR__, "pypsa-data", "serialized_networks", "$scenario.serial")
if !isfile(params_path)
    println("Parameter file '$params_path' not found. Make sure to run 'pypsa-data/serialize_networks.jl' first.")
end
    
@time begin 
    println("Preparing data")
    @unpack k,c,B,p,f = deserialize(params_path)
    F = flow_matrix(B, k)
    Cred_data = redundant_capacity(c, f)
   
    pmin = minimum(p, dims=1)[1,:] 
    pmax = maximum(p, dims=1)[1,:] 
    pdiff = pmax .- pmin
    pmin .= pmin .- safety_margin .* pdiff
    pmax .= pmax .+ safety_margin .* pdiff
    pca = deserialize(replace(model_path, "MODEL" => "pca"))
end


## IGMM stochastic model

# Load model
igmm_path = replace(model_path, "MODEL" => "igmm")
if !ispath(igmm_path) 
    println("Couldn't find IGMM model at '$igmm_path'. Make sure to run 'fit_models.jl' first.")
end
r_igmm = [0.25, 0.5, 0.75, 0.9, 0.99, 1.0]  
igmm_dims = [ relevant_dims(pca, r) for r in r_igmm ]


# Sample from model
Cred_igmm = [] ; issafe_igmm = []
igmm = deserialize(igmm_path)
@showprogress desc="Sampling IGMM" for r in r_igmm 
    ndim = relevant_dims(pca, r)
    igmm_submodel = Product(igmm.v[1:ndim])
    α = Matrix( rand(igmm_submodel, n_sample)' )
    p = transform(pca, α)
    Cred = redundant_capacity(c, p * F')
    issafe = all( pmin' .< p .< pmax', dims=2)[:,1]
    push!(Cred_igmm, Cred)
    push!(issafe_igmm, issafe)
end

## MvGMM stochastic model

# Load models
mvgmm_files = filter(f -> startswith(f, "mvgmm"), readdir(dirname(model_path)) ) 
if length(mvgmm_files) == 0
    println("Couldn't find MvGMM models. Make sure to run 'fit_models.jl' first.")
end
mvgmm_dims = [ parse(Int, replace(split(mvgmm_file, ".")[2], "dims" => "" )) for mvgmm_file in mvgmm_files ]
idx = sortperm(mvgmm_dims); mvgmm_files = mvgmm_files[idx]; mvgmm_dims = mvgmm_dims[idx] 

# Sample from models
Cred_mvgmm = [] ; issafe_mvgmm = []
@showprogress desc="Sampling MvGMM" for mvgmm_file in mvgmm_files
    mvgmm = deserialize(joinpath(dirname(model_path), mvgmm_file))
    α = Matrix( rand(mvgmm, n_sample)' )
    p = transform(pca, α)
    Cred = redundant_capacity(c, p * F')
    issafe = all( pmin' .< p .< pmax', dims=2)[:,1]
    push!(Cred_mvgmm, Cred)
    push!(issafe_mvgmm, issafe)
end
length(Cred_mvgmm)

begin 
    println("## LaTeX Tables for $scenario")

    println("\n#Probabilities of sample being within operation bounds")
    println("\\begin{table}[H]")
    println("\\centering")
    #println("\\caption{$(uppercasefirst(scenario)) scenario: Fraction of samples within $(round(100 * safety_margin, sigdigits=4))% operation bounds using $n_sample samples.}")
    println("\\begin{tabular}{l || " * repeat("c | ", length(igmm_dims)) * "}")
    println("Model / Dimensions & ", join( string.(igmm_dims), " & "), " \\\\\n\\hline")
    println("IGMM & ",  join( string.( round.( 100 .* mean.(issafe_igmm) , sigdigits=4)) .* "\\%", " & " ), " \\\\")
    println("MvGMM & ",  join( string.( round.( 100 .* mean.(issafe_mvgmm) , sigdigits=4)) .* "\\%", " & " ), " & - \\\\ ")
    println("\\end{tabular}")
    println("\\end{table}")

    println("\n#Observed probabilities of any failure")
    println("\\begin{table}[H]")
    println("\\centering")
    #println("\\caption{$(uppercasefirst(scenario)) scenario: Fraction of samples samples with failures.}")
    println("\\begin{tabular}{l || " * repeat("c | ", length(igmm_dims)) * "}")
    println("Model / Dimensions & ", join( string.(igmm_dims), " & "), " \\\\\n\\hline")
    println("IGMM & ",  join( string.( round.( 100 .* map(Cred -> mean(Cred .< 0), Cred_igmm) , sigdigits=4)) .* "\\%", " & " ), " \\\\")
    println("MvGMM & ",  join( string.( round.( 100 .* map(Cred -> mean(Cred .< 0), Cred_mvgmm) , sigdigits=4)) .* "\\%", " & " ), " & - \\\\ ")
    println("\\end{tabular}")
    println("\\end{table}")

    println("\n#Observed probabilities of failures within operation bounds")
    println("\\begin{table}[H]")
    println("\\centering")
    #println("\\caption{$(uppercasefirst(scenario)) scenario: Fraction of samples with failures within $(round(100 * safety_margin, sigdigits=4))% operation bounds using $n_sample samples.}")
    println("\\begin{tabular}{l || " * repeat("c | ", length(igmm_dims)) * "}")
    println("Model / Dimensions & ", join( string.(igmm_dims), " & "), " \\\\\n\\hline")
    println("IGMM & ",  join( string.( round.( 100 .* map((Cred, issafe) -> mean( (Cred .< 0) .&& issafe), Cred_igmm, issafe_igmm), sigdigits=4)) .* "\\%", " & " ), " \\\\")
    println("MvGMM & ",  join( string.( round.( 100 .* map((Cred, issafe) -> mean( (Cred .< 0) .&& issafe), Cred_mvgmm, issafe_mvgmm), sigdigits=4)) .* "\\%", " & " ), " & - \\\\ ")
    println("\\end{tabular}")
    println("\\end{table}")
end

plots_folder = joinpath(dirname(@__DIR__), "plots", "images", scenario)
if !isdir(plots_folder)
    mkpath(plots_folder)
end


begin 
    light_blue  = [0.4, 0.7, 1.0]
    dark_blue   = [0.0, 0.2, 0.6]
    light_green = [0.4, 1.0, 0.7]
    dark_green  = [0.0, 0.4, 0.2]

    igmm_colors  = [ RGBAf( (light_blue .* t .+ dark_blue .* (1-t))... , 0.8) for t in range(0, 1, length(r_igmm)) ]
    mvgmm_colors = [ RGBAf( (light_green .* t .+ dark_green .* (1-t))... , 0.8) for t in range(0, 1, length(mvgmm_files)) ]

    fig = Figure(width=500, height=550)
    ax = Axis(fig[1,1])

    xlimits = (-60, 450)

    bandwidth = 10
    density!(ax, Cred_data; label="Data", color=(:white, 0.0), strokecolor=:black, strokewidth=1, linestyle=:dash, bandwidth)
    for (cred, issafe, color) in zip(Cred_igmm, issafe_igmm, igmm_colors)
        density!(ax, cred[issafe]; color=(:white, 0.0), strokecolor=color, strokewidth=1, boundary=xlimits, bandwidth)
    end
    for (cred, issafe, color) in zip(Cred_mvgmm, issafe_mvgmm, mvgmm_colors)
        density!(ax, cred[issafe]; color=(:white, 0.0), strokecolor=color, strokewidth=1, boundary=xlimits, bandwidth)
    end

    ax.xlabel = "Redundant Capacity [MW]"
    ax.ylabel = "Density"
    Legend(fig[1,2], 
        [   [LineElement(color=:black, linestyle=:dash, linewidth=1)],
            [LineElement(color=c, linewidth=2) for c in igmm_colors], 
            [LineElement(color=c, linewidth=2) for c in mvgmm_colors],], 
        [ ["Data"], string.(igmm_dims) .* " dims", 
                    string.(mvgmm_dims) .* " dims" ],
        ["", "IGMM", "MvGMM"],
        #position=:lc, tellwidth=false, tellheight=false,
        #halign=:left, valign=:top, 
    )
    xlims!(ax, xlimits)
    save(joinpath(plots_folder, "cred_failure_analysis.png"), fig)
    display(fig)
end
