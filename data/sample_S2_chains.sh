#!/bin/bash

#SBATCH --job-name=s2
#SBATCH --qos=standby
#SBATCH --nodes=1
#SBATCH --cpus-per-task=1
#SBATCH --exclusive=user
#SBATCH --output=slurm_output/sample_S2_chains.task_%a.txt
#SBATCH --error=slurm_output/sample_S2_chains.task_%a.err
#SBATCH --requeue
#SBATCH --array=0-999

module load julia
julia --project=.. sample_S2_chains.jl 1000 $SLURM_ARRAY_TASK_ID