using Graphs
using Statistics, Distributions
using CairoMakie
using LinearAlgebra
using Test
using Revise 
using StabilitySampling
using ProgressMeter
using Serialization
using Random, Primes, HaltonSequences
using FileIO

renewable_scenario = true

println("Loading $(renewable_scenario ? "renewable" : "conventional") scenario.")
if !renewable_scenario
    scenario = "de-elec-conventional"
    network = "elec_s_100_ec_lv1.0_5h"
else
    scenario = "de-elec-renewable"
    network = "elec_s_100_ec_lvopt_5h"
end
@time n = Network(joinpath(@__DIR__, "pypsa-results", scenario, "networks", network * ".nc"))

print("Fitting PCA model")
@time pca = PCA(get_power_timeseries(n))
α = project(pca, pca.signal)
a = minimum(α, dims=1)[1,:]
b = maximum(α, dims=1)[1,:]

n_qmc = 1_000
ξ = stack(Halton.([2,3], length=n_qmc), dims=2)

# Load the MvGMM model
modelfolder = joinpath(@__DIR__, "cache", scenario, network, "models")
models = filter(f->startswith(f,"model.mvgmm"), readdir(modelfolder))
dims = map( f->parse(Int, split(split(f, ".")[3], "dims")[1]), models)
idx = sortperm(dims); models = models[idx]
mvgmm = deserialize(joinpath(modelfolder, models[2]))
ndim = length(mvgmm)

# Evaluate marginals
marg1D = marginals(mvgmm)
marg2D = marginals2D(mvgmm) 
I_mut = zeros(ndim, ndim)

println("Calculating Mutual Information")
@showprogress for l = 1:((ndim - 1) * ndim ÷ 2) # for i=1:ndim, j=i+1
    
    # transform linearized index `l` to cartesian indices `(i,j)`
    i = floor(Int, (-1 + sqrt(1 + 8 * (l-1))) / 2)
    pairs_before_i = (i * (i + 1)) ÷ 2
    j = l - pairs_before_i - 1
    i=ndim-i-1; j = ndim-j

    # evaluate marginals pdf
    α_i = ξ[:, 1] .* ( a[i] - b[i] )
    α_j = ξ[:, 2] .* ( a[j] - b[j] )
    lψ_i = [ logpdf( marg1D[i], α_i[k,:] )[1] for k=1:n_qmc ]
    lψ_j = [ logpdf( marg1D[j], α_j[k,:] )[1] for k=1:n_qmc ]

    # evaluate joint pdf 
    α_ij = stack([α_i, α_j], dims=2)
    lψ_ij = [ logpdf( marg2D[i][j-i], α_ij[k,:] ) for k=1:n_qmc ]
    #ψ_ij = exp.(lψ_ij .- maximum(lψ_ij)) ;  ψ_ij = ψ_ij / sum(ψ_ij)
    ψ_ij = exp.(lψ_ij) #;  ψ_ij = ψ_ij / sum(ψ_ij)

    # save mutual info
    I_mut[i,j] = I_mut[j,i] = mean( ψ_ij .* ( lψ_ij .- lψ_i .- lψ_j) ) * (b[i] - a[i]) * (b[j] - a[j])
end

begin 
    println("Mutual Information Matrix")
    println("\\begin{tabular}{c | ", join(fill("c", ndim), " "), "}")
    println( " & " * join("PC " .* string.(1:ndim), "&") * "\\\\ \n\\hline " )
    for i=2:ndim
        print("PC ", i, " & ")
        for j=1:i-1
            print( round(abs(I_mut[i,j]), sigdigits=4), " & " )
        end
        println("\\\\")
    end
    println("\\end{tabular}")
end 

#=
# Visualize total mutal information
fig = Figure(); ax = Axis(fig[1,1]); 
v = sum( abs.(I_mut + I_mut'), dims=1)[1,:] 
scatter!(ax,v)
save(joinpath(plots_folder, scenario, network, "mvgmm_mutal_info.png"), fig)
=#
