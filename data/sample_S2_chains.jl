using Serialization
using Distributions
using Parameters
using ProgressMeter

using CairoMakie 

using Revise 
using StabilitySampling 

n_sample   = 1_000_000
save_every = 50
safety_margin = 0.05
n_chains = 9
force_resample = true
ac_threshold = 0.05

# Define experimental setup by varying parameters
experiments = []
for s in [0.01]
    for model in ["igmm", "mvgmm"]
        for cov_explained in [0.25, 0.5, 0.75, 0.9]
            for scenario in ["renewable", "conventional"]
                for β in [128.0, 256.0, 512.0, 1024.0] 
                    for ichain in 1:n_chains
                        push!(experiments, (; scenario, model, cov_explained, s, β, safety_margin, ichain))
                    end
                end
            end
        end
    end
end
println("Total experiments ", length(experiments)) 


function sample_S2_chains(;
    scenario::String       = "renewable",
    model::String          = "igmm",
    cov_explained::Float64 = 0.75,
    s::Float64             = 0.01,
    β::Float64             = -1e-3,
    safety_margin::Float64 = 0.05,
    ichain::Integer        = 1)

    @time begin 
        println("# Preparing $scenario scenario")
        network_path = joinpath(@__DIR__, "pypsa-data", "serialized_networks", "$scenario.serial")
        if !isfile(network_path)
            println("Parameter file '$network_path' not found. Make sure to run 'pypsa-data/serialize_networks.jl' first.")
        end 
        @unpack B,k,c,p, m_inv,d = deserialize(network_path)
        pca_path = joinpath(@__DIR__, "cache", scenario, "models", "pca.serial")
        if !isfile(pca_path)
            println("PCA model not found. Make sure to run 'fit_models.jl' first.")
        end
        pca = deserialize(pca_path)

        # Extend the range of allowed nodal injections by safety margin in each direction.
        pmin = minimum(p, dims=1)[1,:]
        pmax = maximum(p, dims=1)[1,:]
        pdiff = pmax .- pmin
        pmin .-= safety_margin .* pdiff
        pmin .+= safety_margin .* pdiff

        α_data = project(pca, pca.signal)
        bounds_a = minimum(α_data, dims=1)[1,:]
        bounds_b = maximum(α_data, dims=1)[1,:]
    end

    @time begin 
        println("Loading model")
        ndim = relevant_dims(pca, cov_explained)
        model_path = joinpath(@__DIR__, "cache", scenario, "models")
        if model == "igmm"
            ψ = deserialize(joinpath(model_path, "igmm.serial"))
            ψ = Product(ψ.v[1:ndim])
        elseif model == "mvgmm"
            ψ = deserialize(joinpath(model_path, "mvgmm.$(ndim)dims.serial"))
        end
    end

    # Setup sampling
    lp_background = α -> logpdf(ψ, α)  
    function lp_target(α)
        p = transform(pca, α)
        if all(pmin .<= p .<= pmax)
            u0 = synchronous_state(B, k, p; d, m_inv)
            S2 = linear_syncnorm(B, k, u0; d, m_inv)
            return S2
        else
            return -Inf # instant rejection
        end
    end
    chain_folder = joinpath(@__DIR__, "cache", scenario, "chains")
    if !isdir(chain_folder) mkpath(chain_folder) end
            
    println("Sampling $scenario: $model $ndim dims, β=$β s=$(round( s * 100, sigdigits=4))% ; $(n_sample) samples, chain $ichain / $(n_chains) ")    
    @time begin 
        fpath = joinpath(chain_folder, "chain_S2.$(model)_$(ndim)dims_beta_$(β)_step_$(s)_safetymargin_$(safety_margin)_chain_$(ichain).serial")
        
        if !isfile(fpath) || force_resample

            # Sample chain
            chain = rce_rwm(lp_background, lp_target, n_sample, zeros(ndim), β; 
                stepsize=s .* (bounds_b[1:ndim] .- bounds_a[1:ndim]), 
                lower_bounds=bounds_a[1:ndim], upper_bounds=bounds_b[1:ndim], 
                save_every, progress=nothing)

            n_burnin = length(chain.target) ÷ 2
            chain = (; α = chain.X[n_burnin:end, :], S2 = chain.target[n_burnin:end], 
                       accepted = chain.accepted[n_burnin:end], β, s)
            
            # Calculate autocorrelation length
            lags = round.( Int, n_sample ÷ save_every ÷ 2 ./ exp.(range(0, stop=5, length=100)) )
            ac = autocor(chain.S2, lags)
            ac_length = findfirst(ac .< ac_threshold )
            ac_length = isnothing(ac_length) ? Inf : ac_length
            corrlength = n_burnin + ac_length * save_every

            # Calculate statistics
            println( """
                Acceptance rate $(mean(chain.accepted))
                S2 = $(mean(chain.S2)) ± $(std(chain.S2)) ∈ [$(minimum(chain.S2)) : $(maximum(chain.S2))]
                Correlation length $(corrlength)
            """)            
            serialize(fpath, chain)
        else
            println("File $fpath already exists. Skipping.")
        end
    end
end


if length(ARGS) == 0
    println("""
    Usage:
         julia sample_S2_chains.jl [i] [j]
    or   julia sample_S2_chains.jl DEBUG

    Then experiments { k * i + j : k = 0,1,2,... } will be run.
    The debug flag will run a single experiment with reduced sample size.    
    """)
    return
else
    if "DEBUG" in ARGS
        experiments = [ experiments[42] ] 
        n_sample = 200
    else 
        i = parse(Int, ARGS[1])
        j = parse(Int, ARGS[2])
        experiments = experiments[j:i:end]
    end

    for experiment in experiments
        sample_S2_chains(; experiment...)
    end
end