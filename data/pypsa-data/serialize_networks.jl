include("src/all.jl")

using Serialization

function load_network_params(scenario::String, network::String, check=false)
    n_path = joinpath(@__DIR__, "pypsa-results", scenario, "networks", network * ".nc")
    n = Network(n_path)

    check && network_consistency_check(n)

    x,y = get_bus_coords(n)
    parameters = (; 
        k = get_couplings(n),
        c = get_capacities(n),
        d = get_dampings(n),
        m_inv = 1 ./ get_inertias(n),
        B = incidence_matrix(n),
        p = get_power_timeseries(n),
        f = get_flow_timeseries(n),
        x, y
        #ϕ = get_phase_timeseries(n),
    )

    return parameters
end

println("Serializing conventional network")
@time begin 
    scenario = "de-elec-conventional"
    network  = "elec_s_100_ec_lv1.0_5h"
    params = load_network_params(scenario, network)
    serialize(joinpath(@__DIR__, "serialized_networks", "conventional.serial"), params)
end 

println("Serializing renewable network")
@time begin 
    scenario = "de-elec-renewable"
    network  = "elec_s_100_ec_lvopt_5h"
    params = load_network_params(scenario, network)
    serialize(joinpath(@__DIR__, "serialized_networks", "renewable.serial"), params)
end