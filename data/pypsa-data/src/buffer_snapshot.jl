
function has_buffer_snapshot(n::Py)
    pyisinstance(n, pypsa.Network) || @warn "Object is not a PyPSA network" 
    return pyhasattr(n, "has_buffer") && pytruth(n.has_buffer) 
end

function add_buffer_snapshot!(n::Py, time_delta=Dates.Year(1); force = false)
    pyisinstance(n, pypsa.Network) || @warn "Object is not a PyPSA network"
    
    if !has_buffer_snapshot(n) || force
        snapshots = pyconvert(Vector{DateTime}, n.snapshots)
        push!(snapshots, last(snapshots) + time_delta)
        n.set_snapshots(snapshots)  
        n.has_buffer = true
    end
end

function remove_buffer_snapshot!(n::Py)
    pyisinstance(n, pypsa.Network) || @warn "Object is not a PyPSA network"
    
    if has_buffer_snapshot(n)
        snapshots = pyconvert(Vector{DateTime}, n.snapshots)
        pop!(snapshots)
        n.set_snapshots(snapshots)  
        n.has_buffer = false
    end
end

function init_buffer_snapshot!(n::Py)
    pyisinstance(n, pypsa.Network) || @warn "Object is not a PyPSA network"
    
    # ensure that the network has a buffer snapshot
    add_buffer_snapshot!(n)
    ibuffer = length(n.snapshots)-1

    # set the buffer snapshot to the mean of the current data
    for c in [ "loads", "generators", "stores", "storage_units", "links" ]
        data_dict = getproperty(n, Symbol(c * "_t") )
        
        # check if data is available
        if pytruth( data_dict["p_set"].empty )
            if "p" in data_dict 
                data_dict["p_set"] = data_dict["p"].copy()
            elseif "p0" in data_dict
                data_dict["p_set"] = data_dict["p0"].copy()
            else
                throw("No data available for buffer snapshot")
            end
        end

        p_mean = mean(pyconvert(Matrix, data_dict["p_set"]), dims=1)[1,:]   
        data_dict["p_set"].iloc[ibuffer] = p_mean
    end

    return n
end


function lpf_buffer_snapshot!(n::Py)
    pyisinstance(n, pypsa.Network) || @warn "Object is not a PyPSA network"

    has_buffer_snapshot(n) || init_buffer_snapshot!(n)

    # Disable logging and calculate linear power flow
    logging = pyimport("logging")
    logging_level = logging.getLogger().getEffectiveLevel()
    logging.disable(logging.CRITICAL)
    n.lpf(n.snapshots[-1], pybuiltins.False)
    logging.disable(logging_level)

    return n
end