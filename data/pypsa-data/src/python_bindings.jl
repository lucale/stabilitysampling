
function install_pypsa_environment(verbose::Bool = false)
    conda_dir = joinpath(dirname(dirname(dirname(@__DIR__))), ".CondaPkg")
    if isdir(conda_dir)
        verbose && println("Found Conda packages at ", conda_dir)
    else
        verbose && println("Installing PyPSA Conda environment.")
        CondaPkg.add("numpy")
        CondaPkg.add("scipy")
        CondaPkg.add("pandas")
        CondaPkg.add("pypsa")
    end
end

function setup_pypsa_environment(verbose::Bool = false)
    verbose && println("Importing python bindings for PyPSA.")
    global numpy = pyimport("numpy")
    global pandas = pyimport("pandas")
    global scipy = pyimport("scipy")
    global pypsa = pyimport("pypsa")
    global sys = pyimport("sys")
    global logging = pyimport("logging")
    global _py_logging_level = logging.getLogger().getEffectiveLevel()
    global pywarn = pyimport("warnings")
    pywarn.filterwarnings("ignore")
    return nothing
end

function disable_python_print()
    sys.stdout = pybuiltins.open("/dev/null", "w")
end
function enable_python_print()
    sys.stdout = sys.__stdout__
end
function disable_python_log()
    global _py_logging_level = logging.getLogger().getEffectiveLevel()
    logging.disable(logging.CRITICAL)
end
function enable_python_log()
    logging.disable(_py_logging_level)
end



# convert data frame to Julia
function PythonCall.Core.pyconvert(::Type{<:AbstractDataFrame}, obj::Py)
    pyisinstance(obj, pandas.DataFrame) || @warn "Object is not a pandas DataFrame"

    table = obj.reset_index()
    columns = pyconvert(Vector{String}, table.columns)
    
    # create a dictionary of columns
    dict = Dict()
    for (i,c) in enumerate(columns)
        data = pyconvert(Vector, table[c])
        if all( pyisinstance.(data, pybuiltins.str))
            data = pyconvert.(String, data) 
        end
        dict[c] = data
    end
    return DataFrame(dict)
end

# convert dictionary of data frames to Julia
function PythonCall.Core.pyconvert(::Type{<:AbstractDict{<:AbstractString, <:AbstractDataFrame}}, obj::Py)
    if ! pyisinstance(obj, pybuiltins.dict) ||
       ! pyall([pyisinstance(obj[k], pandas.DataFrame) for k in obj.keys()])
        @warn "Object is not a dictionary of pandas DataFrames" end

    dict = Dict{String, DataFrame}()
    for key in obj.keys()
        dict[pyconvert(String, key)] = pyconvert(DataFrame, obj[key])
    end
    return dict
end

# Convert to pypsa network to NamedTuple
function PythonCall.Core.pyconvert(::Type{<:NamedTuple}, n::Py)
    pyisinstance(n, pypsa.Network) || @warn "Object is not a PyPSA network"

    all_components = [ pyconvert(String, n.components[c]["list_name"]) for c in n.all_components ]

    has_buffer = has_buffer_snapshot(n) 
    has_buffer && remove_buffer_snapshot!(n)
    nt = (;
        snapshots = pyconvert(Vector, n.snapshots),
        ( Symbol(c) => pyconvert(DataFrame, getproperty(n, c)) for c in all_components ) ... ,
        ( Symbol(c * "_t") => pyconvert(Dict{String, DataFrame}, getproperty(n, Symbol(string(c) * "_t"))) for c in all_components ) ...
    )
    has_buffer && add_buffer_snapshot!(n)
    return nt
end
