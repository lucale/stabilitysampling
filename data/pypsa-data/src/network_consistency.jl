function network_consistency_check(n::Network)

    if !has_timeseries(n)
        println("Network not properly solved!"); return false
    end

    # read parameters
    B = incidence_matrix(n)
    k = get_couplings(n)
    c = get_capacities(n)
    

    # read time series
    p = get_power_timeseries(n)
    f = get_flow_timeseries(n)
    ϕ = get_angle_timeseries(n)
    

    # power balance, ∑p = 0
    C1 = sum(p, dims=2)[:,1] .< 1e-3

    # flow consistency for edges, KBᵀϕ = f
    C2 = abs.( ϕ * B * diagm(k) .- f ) .< 1e-3

    # flow consistency for nodes, Bf = p
    C3 = abs.( f * B' .- p) .< 1e-3

    # no overlad, |f| ≤ c
    C4 = abs.(f) .< c'

    # check all constraints
    C1 = all(C1)
    C2 = all(C2)
    C3 = all(C3)
    C4 = all(C4)

    println(" ∑p = 0 : ", C1) # power balance
    println(" KBᵀϕ = f : ", C2) # flow consistency ( for edges R^L )
    println(" Bf = p : ", C3) # flow consistency ( for nodes R^N ) 
    println(" |f| ≤ c : ", C4) # no overload    
    
    return C1 && C2 && C3 && C4
end
