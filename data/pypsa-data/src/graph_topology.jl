
# Nodes and edges
function get_buses(n::Network)
    #valid_buses = fill(true, length(n.buses.Bus))
    valid_buses = n.buses.carrier .== "AC"
    return n.buses.Bus[valid_buses]
end
function get_lines(n::Network)
    valid_lines = fill(true, length(n.lines.Line))
    #valid_lines = ( n.lines.x .< 1e9 ) .&& ( n.lines.s_nom_opt .> 1e-6)
    #valid_lines = valid_lines .&& ( n.lines.build_year .<= 2020 ) 
        ### Why does it not work with the build_year condition??
    return n.lines.Line[valid_lines]
end

Graphs.nv(n::Network) = length(get_buses(n))
Graphs.ne(n::Network) = length(get_lines(n))

#=
# Construct graph
function Graphs.Graph(n::Network)
    buses = get_buses(n)
    lines = get_lines(n)

    idx = indexin(lines, n.lines.Line)   
    u = indexin(n.lines.bus0[idx], buses)
    v = indexin(n.lines.bus1[idx], buses)    
    
    g = Graph(length(buses))
    for e in zip(u,v)
        add_edge!(g, e[1], e[2])
    end

    #TODO: what about HVDC links?

    return g
end
get_graph(n::Network) = Graphs.Graph(n)
=#

# physical bus location geo coordinates
function get_bus_coords(n::Network)
    buses = get_buses(n)
    idx = indexin(buses, n.buses.Bus)
    x = n.buses.x[idx]
    y = n.buses.y[idx]
    return x, y
end

#=
# Embedded graph = graph + coordinates
function EmbeddedGraphs.EmbeddedGraph(n::Network)
    g = Graph(n)
    x,y = get_bus_coords(n)
    return EmbeddedGraph(g, [ zip(x,y)... ])
end
=#
import Graphs: incidence_matrix
function incidence_matrix(n::Network)
    buses = get_buses(n)
    lines = get_lines(n)
    N = length(buses); L = length(lines)
    B = spzeros(Int, N, L)
    idx_line = indexin(lines, n.lines.Line)
    bus0 = n.lines.bus0[idx_line]
    bus1 = n.lines.bus1[idx_line]
    node0 = indexin(bus0, buses)
    node1 = indexin(bus1, buses)
    for (l,(u,v)) in enumerate(zip(node0,node1))
        B[u, l] = -1;  B[v, l] = 1
    end
    return B
end