function get_couplings(n::Network)
    lines = get_lines(n)
    idx = indexin(lines, n.lines.Line)
    return 380^2 ./ n.lines.x[idx]
    #at least if 
    #    @assert all( n.buses.v_nom .== 380.0) 
end

function get_capacities(n::Network)
    lines = get_lines(n)
    idx = indexin(lines, n.lines.Line)
    s = n.lines.s_nom_opt[idx] #./ n.lines.s_max_pu[1]
    return s
end 


H_carrier = Dict(
    "lignite" => 5.0,
    "coal" => 5.0,
    "oil" => 5.0,
    "OCGT" => 5.0,
    "CCGT" => 5.0,
    "nuclear" => 7.0,
    "hydro" => 2.0,
    "biomass" => 4.0,
    "ror" => 3.0,
    "onwind" => 2.0,
    "offwind-ac" => 2.0,
    "offwind-dc" => 2.0,
    "solar" => 0.5,
    "load" => 1.0, # load shedding 
)



function get_inertias(n::Network)
    buses = get_buses(n)
    carriers = unique(n.generators.carrier)
    N = length(buses); C = length(carriers)
    p = zeros(N,C)
    for i=1:N
        prodcap = get_production_capacities(n, buses[i])
        idx = indexin(keys(prodcap), carriers)
        p[i,idx] .= values(prodcap)
    end
    H = map(c->H_carrier[c], carriers) 
    ω0 = 2π * 50 # Hz
    m = sum( 2 .* p .* H', dims=2)[:,1] ./ ω0
    return m
end

function get_dampings(n::Network)
    buses = get_buses(n)
    carriers = unique(n.generators.carrier)
    N = length(buses); C = length(carriers)
    p = zeros(N,C)
    for i=1:N
        prodcap = get_production_capacities(n, buses[i])
        idx = indexin(keys(prodcap), carriers)
        p[i,idx] .= values(prodcap)
    end
    ω0 = 2π * 50 # Hz
    d = sum(p, dims=2)[:,1] ./ ω0
    return d
end