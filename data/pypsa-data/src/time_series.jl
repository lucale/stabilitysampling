## has timeseries if pypsa network was solved properly
function has_timeseries(n::Network)
    return size(n.buses_t["p"],2) > 1 &&
           size(n.lines_t["p0"],2) > 1 &&
           size(n.buses_t["v_ang"],2) > 1
end


## Nodal time series, net injections, aggregated over nodes and carriers

function get_power_timeseries(n::Network)
    buses = get_buses(n)
    idx = indexin(buses, names(n.buses_t["p"]))
    p_t = Matrix( n.buses_t["p"][:, idx] )
    return p_t
end

function get_flow_timeseries(n::Network)
    lines = get_lines(n)
    idx = indexin(lines, names(n.lines_t["p0"]))
    f_t = Matrix( n.lines_t["p1"][:, idx] )
    return f_t
end

function get_angle_timeseries(n::Network)
    buses = get_buses(n)
    idx = indexin(buses, names(n.buses_t["v_ang"]))
    φ_t = Matrix( n.buses_t["v_ang"][:, idx] )
    return φ_t
end

## Timeseries of outputs

# consumption 
function get_load_timeseries(n::Network)

    # read data frames
    L = Matrix(n.loads_t["p"][:, n.loads.Load])
    
    # embedd with zeros into vertex space
    buses = get_buses(n)
    N = length(buses)
    T = length(n.snapshots)
    l = zeros(Float64, (T,N))
    idx = indexin(n.loads.bus, buses)
    l[:, idx] .= L

    return - l
end

# charge 
has_chargers(n::Network) = any(contains.(n.links.carrier, "charger"))
function get_charge_timeseries(n::Network)

    # get charge and discharge time series 
    discharge_mask = contains.(n.links.carrier, "discharger") 
    discharge_buses = n.links.bus1[discharge_mask]
    discharge_links = n.links.Link[discharge_mask]
    p_discharge = Matrix( n.links_t["p1"][:, discharge_links])

    charge_mask = contains.(n.links.carrier, "charger") .&& .! discharge_mask
    charge_buses = n.links.bus0[charge_mask]
    charge_links = n.links.Link[charge_mask]
    p_charge = Matrix( n.links_t["p0"][:, charge_links])

    # initialize timeseries with zeros
    buses = get_buses(n)
    T = length(n.snapshots)
    c = zeros(Float64, T, length(buses))

    # add charge and discharge time series to the matrix 
    if length(discharge_buses) > 0
        c[:, indexin(discharge_buses, buses)] .+= p_discharge # is negative, that's why plus ;)
    end
    if length(charge_buses) > 0
        c[:, indexin(charge_buses, buses)] .+= p_charge
    end 

    return - c # positve charging is negative injection
end


# storage timeseries, caveat: unit is in MWh not MW as for other time series
function get_storage_timeseries(n::Network)
    all_buses = get_buses(n)
    e_buses = replace.(n.stores.bus, " battery" => "")
    e_storage = Matrix( n.stores_t["e"][:, n.stores.Store])
    e = zeros(Float64, length(n.snapshots), length(all_buses))
    if length(buses) > 0
        e[:, indexin(buses, all_buses)] .= e_storage
    end
    return e
end



## Timeseries of inputs

# list all carriers of generators 
function all_generator_carriers(n::Network, p_min=1.0)

    # get carriers with non-zero capacities
    mask_by_capacities = n.generators.p_nom_opt .* n.generators.p_max_pu .>= p_min
    buses = get_buses(n)

    # get carriers with non-zero time series
    p = Matrix(n.generators_t["p"][:,n.generators.Generator])
    mask_by_timeseries = maximum(p , dims=1)[1,:] .>= p_min

    # combine masks
    mask = mask_by_capacities .&& mask_by_timeseries
    carriers = unique( n.generators.carrier[mask] )
    return carriers    
end

# get generator time series for a given generator carrier
function get_generator_timeseries(n::Network, carrier::String)
    mask = n.generators.carrier .== carrier 
    capacities = n.generators.p_nom_opt .* n.generators.p_max_pu * 1e-3
    mask = mask .&& ( capacities .> 0)
    buses = unique(n.generators.bus[mask])
    
    all_buses = get_buses(n)

    # copy matrix from dataframe time series 
    p_G = Matrix(n.generators_t["p"][:, n.generators.Generator[mask]])
    
    # embedd with zeros
    idx = indexin(buses, all_buses)
    p = zeros(Float64, (size(p_G,1), length(all_buses)))
    p[:, idx] .= p_G
    
    return p 
end


## Timeseries of injection patterns

# carrier space time series
function get_carrierspace_power_timeseries(n::Network)
    carriers = all_generator_carriers(n)
    
    l = get_load_timeseries(n)
    N = size(l,2)
    has_chrg = has_chargers(n)
    m = N*(length(carriers)+1+has_chrg)
    p = zeros(Float64, size(l,1), m )

    p[:, 1:N] .= l
    if has_chrg 
        c = get_charge_timeseries(n)
        p[:, N+1:2N] .= c
    end
    for c=1:length(carriers)
        p_c = get_generator_timeseries(n, carriers[c])
        idx = N*(c+has_chrg)
        p[:, idx+1:idx+N] .= p_c
    end
    return p
end


#sum vector in carrier space over carriers to get vector in bus space 
function sum_over_carriers(n::Network, q::AbstractVector{<:Real})
    N = nv(n) ; c = length(q) ÷ N
    p = q[1:N] 
    for i=2:c
        p .+= q[(i-1)*N+1:i*N]
    end
    return p 
end
#sum timeseries in carrier space over carriers to get timeseries in bus space 
function sum_over_carriers(n::Network, q::AbstractMatrix{<:Real})
    N = nv(n) ; c = size(q,2) ÷ N
    p = q[:, 1:N] 
    for i=2:c
        p .+= q[:, (i-1)*N+1:i*N]
    end
    return p 
end



#==============================================================================

function get_all_signals(n::Network)
    signals = Dict( "load" => get_load_timesignal(n) )
    
    p = get_charge_timesignal(n)
    if !all(p .== 0.0) signals["charge"] = p end
    
    all_carriers = all_generator_carriers(n)
    for c in all_carriers
        p = get_carrier_timesignal(n, c)
        if !all(p .== 0.0) signals["gen_" * c] = p end
    end

    return signals
end




function get_all_timeseries(n::Network) # :: Dict{String, Matrix{Float64}}
    
    timeseries = Dict( "load" => get_load_timeseries(n) )
    
    p = get_charge_timeseries(n)
    if !all(p .== 0.0) timeseries["charge"] = p end

    p = get_storage_timeseries(n)
    if !all(p .== 0.0) timeseries["storage"] = p end 

    for c in all_generator_carriers(n)
        p = get_generator_timeseries(n, c)
        if !all(p .== 0.0) timeseries["gen_" * c] = p end  
    end

    return timeseries
end

# sum all time series of generators, loads and charging devices
function get_net_timeseries(n::Network)
    p = zeros(Float64, length(n.snapshots), length(all_nodes(n)) )
    all_timeseries = get_all_timeseries(n)
    for k in keys(all_timeseries)
        if k in ["load", "charge"] || startswith(k, "gen_")
            p = p .+ all_timeseries[k]
        end
    end
    return p
end

function get_full_timeseries_columns(n::Network)    
    carriers = all_generator_carriers(n)
    columns = ["Load", (has_storage ? ["Charge"] : [])..., 
                n.carriers.nice_name[ indexin(carriers, n.carriers.Carrier) ] ... ]
    return columns
end

function get_full_timeseries(n::Network)
    l = get_load_timeseries(n)  
    
    carriers = all_generator_carriers(n)
    p = map( c-> get_generator_timeseries(n, c), carriers )
    
    has_storage = size(n.stores, 1) > 0
    
    data = hcat( l, (has_storage ? [get_charge_timeseries(n)] : []) ..., p...)
    return data
end

function reduce_full(n::Network, p_full::AbstractMatrix{<:Real})
    N = size(n.buses,1)
    p = sum( [ p_full[:, (i-1)*N+1:(i-1)*N+N] for i=1:size(p_full, 2)÷N ] )
    return p
end
function reduce_full(n::Network, p_full::AbstractVector{<:Real})
    N = size(n.buses,1)
    p = sum( [ p_full[ (i-1)*N+1:(i-1)*N+N] for i=1:length(p)÷N ] )
    return p
end

==================================================================================#