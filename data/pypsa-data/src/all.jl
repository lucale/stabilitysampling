using Dates
using DataFrames
using Graphs
using Statistics 
using SparseArrays

#using ProgressMeter

using CondaPkg
using PythonCall



include("python_bindings.jl")
include("network_wrapper.jl")
include("graph_topology.jl")
#include("buffer_snapshot.jl")
include("generators.jl")
include("parameters.jl")
include("time_series.jl")
include("network_consistency.jl")

# install PyPSA environment
verbose = true
install_pypsa_environment(verbose)
setup_pypsa_environment(verbose)
