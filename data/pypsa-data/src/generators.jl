## Carrier space

function get_production_capacities(n::Network, bus::String)
    mask = n.generators.bus .== bus
    carriers = n.generators.carrier[mask]
    powercap = n.generators.p_nom_opt[mask]
    return Dict(zip(carriers, powercap))    
end

function yearly_production(n::Network)
    carriers = unique(n.generators.carrier)
    production = zeros(length(carriers))
    for i=1:length(carriers)    
        gens = n.generators.Generator[n.generators.carrier .== carriers[i]]
        idx = indexin(gens, names(n.generators_t["p"]))
        g = Matrix(n.generators_t["p"][:, idx])
        production[i] = mean(sum(g, dims=2))
    end
    production = production .* (24 * 365.25)
    return Dict(zip(carriers, production))
end
function yearly_load(n::Network)
    buses = get_buses(n)
    idx = indexin(buses, names(n.loads_t["p"]))
    l = Matrix(n.loads_t["p"][:, idx]) 
    return mean(sum(l, dims=2)) * (24 * 365.25)
end

#=
function get_production_capacities(n::Network)
    buses = get_buses(n)
    caps = map(bus -> sum(values(get_production_capacities(n, bus))), buses)  
    return caps
end
=#

# Seperate by node and carrier 
#get_generators(n::Network) = n.generators.Generator
#function generator_buses(n::Network) = indexin(n.generators.bus, all_buses(n))

