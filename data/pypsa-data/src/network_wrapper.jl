
"""
    Network 

    Wrapper for `pypsa.Network` object with buffered pyconvert output.   

    Access the data in a similar fashion, eg. 
    ```julia
    n = Network("path/to/network.nc")
    
    n.buses #... 
    ```

    The wrapped `pypsa.Network` object `network`
    ```julia 
    n.network
    ````
"""
struct Network
    network::Py
    snapshots::Vector{DateTime}
    tables::Dict{Symbol, DataFrame}
    series::Dict{Symbol, Dict{String,DataFrame}}
    buffer::Vector{Int} # #TODO: artificial points in time series 
end

function Network(n::Py)
    pyisinstance(n, pypsa.Network) || 
        ArgumentError("Argument type `$(pytype(n))` not recognized") |> throw
    
    return Network(n,
        pyconvert(Vector, n.snapshots),
        Dict{Symbol, DataFrame}(),
        Dict{Symbol, Dict{String,DataFrame}}(),
        Int[] 
    )
end
function Network(s::String)
    if !endswith(s, ".nc") 
        @warn "Invalid file type of '$(splitpath(s)[end])'. Expected NetCDF4 (.nc) file." 
    end
    if !isfile(s) 
        throw(ErrorException("File '$s' not found!"))
    end
    disable_python_log()
    n = Network(pypsa.Network(s))
    enable_python_log()
    return n
end

function Base.getproperty(n::Network, name::Symbol)
    
    # treat fields as properties 
    if name in fieldnames(Network) return getfield(n, name)
    
    # if property is already converted then return it
    elseif haskey(n.tables, name) return n.tables[name]
    elseif haskey(n.series, name) return n.series[name]

    # otherwise convert property and store it
    elseif hasproperty(n.network, name)
        obj = getproperty(n.network, name)
        if endswith( string(name), "_t") 
            obj = pyconvert(Dict{String, DataFrame}, obj)
            n.series[name] = obj
        else 
            obj = pyconvert(DataFrame, obj)
            n.tables[name] = obj
        end
        return obj

    # if property does not exist in network then throw error
    else ArgumentError("Network has no property `$name`") |> throw end
end
Base.hasproperty(n::Network, name::Symbol) = 
    ( name in fieldnames(Network) || hasproperty(n.network, name) )

function Base.propertynames(n::Network) 
    components = map(
        k -> pyconvert(String, n.network.components[k]["list_name"]),
        pyconvert(Vector{String}, collect( n.network.components.keys() ))[2:end]
    )
    return [fieldnames(Network)..., Symbol.(components) ... ]
end

Base.show(io::IO, n::Network) = println(io, string(n.network))
Base.show(io::IO, ::MIME"text/plain", n::Network) = print(io, "PyPSA Network")




#= 
# Alternatively use a struct directly
struct Network
    buses::DataFrame
    lines::DataFrame
    carriers::DataFrame
    loads::DataFrame
    generators::DataFrame
    snapshots::Vector{Dates.DateTime}
    buses_t::Dict{String, DataFrame}
    lines_t::Dict{String, DataFrame}
    loads_t::Dict{String, DataFrame}
    ...
 
    buffer # ...  
end
# advise against due to boiler plate code
=#
