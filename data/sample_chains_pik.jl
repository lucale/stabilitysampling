include("utils.jl")
using Base.Threads

using LinearAlgebra
println("BLAS Threads: $(LinearAlgebra.BLAS.get_num_threads())")
println("Julia Threads: $(Threads.nthreads())")

println("Setting BLAS Threads to 1")
LinearAlgebra.BLAS.set_num_threads(1)

job_array_idx = parse(Int64, ARGS[1])
total_job_array = parse(Int64, ARGS[2])
println("Job $(job_array_idx) of $(total_job_array)")

if !isdir(joinpath("results", "conventional"))
    error("no results/conventional directory")
end
if !isdir(joinpath("results", "renewable"))
    error("no results/renewable directory")
end


function exp_res_path(e)
    if e.renewable_scenario
        return joinpath("results", "renewable", "chain_$(e.model)_$(e.cov_explained)coverage_beta_$(e.β)_step_$(e.step_size)_chain$(e.chain).serial")
    else
        return joinpath("results", "conventional", "chain_$(e.model)_$(e.cov_explained)coverage_beta_$(e.β)_step_$(e.step_size)_chain$(e.chain).serial")
    end
end

# === Load network 

n_sample::Int64 = 200_000

if "DEBUG" ∈ ARGS
    println("DEBUG")
    n_sample::Int64 = 1000
end

experiments = []
results = []
array_idx = 0
for renewable_scenario in [true, false]
    for β in 2. .^(-7:7)
        for step_size in [0.0001, 0.001, 0.01, 0.1]
            for chain in 1:5
                for model in ["igmm", "mvgmm"]
                    for cov_explained in [0.25, 0.5, 0.75, 0.9, 1.0]
                        if !( cov_explained == 1.0 && model == "mvgmm")
                            if array_idx % total_job_array == job_array_idx
                                e = (; n_sample, renewable_scenario, β, step_size, chain, model, cov_explained)
                                if !ispath(exp_res_path(e))
                                    append!(experiments, [e, ])
                                end
                            end
                            global array_idx += 1
                        end
                    end
                end
            end
        end
    end
end

function sample_S2_chain(n::Network, lp_background; β=0.0, step_size, ndim, progress=nothing, n_sample, args...)

    # Calculate bounds for the PCA model
    pca = PCA(get_power_timeseries(n))
    α_data = project(pca, pca.signal)
    bounds_a = minimum(α_data, dims=1)[1,:]
    bounds_b = maximum(α_data, dims=1)[1,:]

    # Calculate the flow matrix
    #c = get_capacities(n)
    k = get_couplings(n)
    B = incidence_matrix(n)
    #F = flow_matrix(B,k)
    d = get_dampings(n)
    m_inv = 1 ./ get_inertias(n)

    # Define the target log posterior
    α0 = zeros(ndim)
    u0 = synchronous_state(B, k, transform(pca, α0); d, m_inv)
    function lp_target(α)
        u0 .= synchronous_state(B, k, transform(pca, α), initial_guess=u0; d, m_inv)
        linear_syncnorm(B,k, u0; d, m_inv)
    end
    stepsize = step_size .* ( bounds_b[1:ndim] .- bounds_a[1:ndim] )



    println("Sampling for ", round( step_size * 100, sigdigits=4), "% stepsize")
    chain = rce_rwm(
        lp_background, lp_target, n_sample, α0, β; stepsize,
        lower_bounds = bounds_a[1:ndim], upper_bounds = bounds_b[1:ndim],
	    progress
    )
    data = (; α = chain.X, S2 = chain.target, β = chain.β, step_size, accepted = chain.accepted)
    S2 = chain.target
    println("  S2 = ", mean(S2[n_sample÷2:end]), " ± ", std(S2[n_sample÷2:end]), " ∈ ", extrema(S2))
    return data
end


println("Experiments ", length(experiments))
for e in experiments
    println("Running Experiment $e") 
    n = load_network(e.renewable_scenario)
    pca = PCA(get_power_timeseries(n)) 
    ndim = relevant_dims(pca, e.cov_explained)

    if e.renewable_scenario
        scenario = "de-elec-renewable"
        network = "elec_s_100_ec_lvopt_5h"
    else 
        scenario = "de-elec-conventional"
        network = "elec_s_100_ec_lv1.0_5h"
    end
    if e.model == "igmm"
        igmm_full = deserialize( joinpath(cache_folder, scenario, network, "models", "model.igmm.jld2"))
        ψ = Product( igmm_full.v[1:ndim] )
    elseif e.model == "mvgmm"
        ψ = deserialize( joinpath(cache_folder, scenario, network, "models", "model.mvgmm.$(ndim)dims.jld2"))        
    end
    lp_background(α) = logpdf(ψ, α) 

    α0 = zeros(ndim)
    k = get_couplings(n)
    #c = get_capacities(n)
    B = incidence_matrix(n)
    d = get_dampings(n)
    m_inv = 1 ./ get_inertias(n)
    
    #lp_target = α -> redundant_capacity(c, F * transform(pca, α))
    global u0 = synchronous_state(B, k, transform(pca, α0))
    function lp_target(α)
        u0 .= synchronous_state(B, k, transform(pca, α), intitial_guess=u0; d, m_inv)
        linear_syncnorm(B,k, u0; d, m_inv)
    end
    serialize(replace(exp_res_path(e), "serial"=> "params.serial"), e)
    result = sample_S2_chain(n, lp_background; ndim, e...)
    serialize(exp_res_path(e), result)
end

