using Serialization
using Distributions
using Parameters
using ProgressMeter

using CairoMakie 

using Revise 
using StabilitySampling 

n_sample   = 1_000_000 
save_every = 100
n_chains = 9 
force_resample = false
ac_threshold = 0.01

# Define experimental setup by varying parameters
experiments = []
for safety_margin in [0.05]
    for s in [0.01]
        for model in ["igmm", "mvgmm"]
            all_cov_explained = [0.25, 0.5, 0.75, 0.9] 
            if model == "igmm"
                append!(all_cov_explained, [0.99, 1.0])
            end
            for cov_explained in all_cov_explained
                
                scenario = "renewable" 
                all_β = exp.( .- [ 2:.25125:8 ... ] )
                for β in all_β
                    push!(experiments, (; scenario, model, cov_explained, s, β, safety_margin))
                end

                scenario = "conventional" 
                all_β = exp.( .- [ 0.3:.25125:7 ... ] )
                for β in all_β
                    push!(experiments, (; scenario, model, cov_explained, s, β, safety_margin))
                end
            end
        end
    end
end
println("Total experiments ", length(experiments))  


function sample_Cred_chains(;
    scenario::String       = "renewable",
    model::String          = "igmm",
    cov_explained::Float64 = 0.75,
    s::Float64             = 0.01,
    β::Float64             = -1e-3,
    safety_margin::Float64  = 0.05)

    @time begin 
        println("# Preparing $scenario scenario")
        network_path = joinpath(@__DIR__, "pypsa-data", "serialized_networks", "$scenario.serial")
        if !isfile(network_path)
            println("Parameter file '$network_path' not found. Make sure to run 'pypsa-data/serialize_networks.jl' first.")
        end 
        @unpack B,k,c,p = deserialize(network_path)
        F = flow_matrix(B, k)
        pca_path = joinpath(@__DIR__, "cache", scenario, "models", "pca.serial")
        if !isfile(pca_path)
            println("PCA model not found. Make sure to run 'fit_models.jl' first.")
        end
        pca = deserialize(pca_path)
        pmin = minimum(p, dims=1)[1,:] 
        pmax = maximum(p, dims=1)[1,:] 
        pdiff = pmax .- pmin
        pmin .= pmin .- safety_margin .* pdiff
        pmax .= pmax .+ safety_margin .* pdiff
        α_data = project(pca, pca.signal)
        bounds_a = minimum(α_data, dims=1)[1,:]
        bounds_b = maximum(α_data, dims=1)[1,:]
    end

    @time begin 
        println("Loading model")
        ndim = relevant_dims(pca, cov_explained)
        model_path = joinpath(@__DIR__, "cache", scenario, "models")
        if model == "igmm"
            ψ = deserialize(joinpath(model_path, "igmm.serial"))
            ψ = Product(ψ.v[1:ndim])
        elseif model == "mvgmm"
            ψ = deserialize(joinpath(model_path, "mvgmm.$(ndim)dims.serial"))
        end
    end

    
    chain_folder = joinpath(@__DIR__, "cache", scenario, "chains")
    if !isdir(chain_folder) mkpath(chain_folder) end

    # Initialize diagnostics
    acc_rates = zeros(n_chains)
    Cred_stats = (; 
        mean  = zeros(n_chains),
        std   = zeros(n_chains),
        min   = zeros(n_chains),
        max   = zeros(n_chains)
    )
    corrlengths = zeros(n_chains)
    lags = round.( Int, n_sample ÷ save_every ÷ 2 ./ exp.(range(0, stop=5, length=100)) )
            
    # Setup sampling
    lp_background = α -> logpdf(ψ, α)  
    function lp_target(α)
        p = transform(pca, α)
        if all(pmin .<= p .<= pmax)
            return redundant_capacity(c, F * p)
        else
            return Inf # instant rejection
        end
    end

    println("Sampling $scenario: $model $ndim dims, β=$β s=$(round( s * 100, sigdigits=4))% ; $(n_sample) x$(n_chains) ")
    progress=Progress(n_sample * n_chains) 
    skipped = repeat([false], n_chains)
    @time for i=1:n_chains

        fpath = joinpath(chain_folder, "chain_cred.$(model)_$(ndim)dims_beta_$(β)_step_$(s)_safetymargin_$(safety_margin)_chain$i.serial")
        if !isfile(fpath) || force_resample
            
            # Sample chain
            chain = rce_rwm(lp_background, lp_target, n_sample, zeros(ndim), β; 
                stepsize=s .* (bounds_b[1:ndim] .- bounds_a[1:ndim]), 
                lower_bounds=bounds_a[1:ndim], upper_bounds=bounds_b[1:ndim], 
                save_every, progress)

            reevaluated_Cred = redundant_capacity(c, transform(pca, chain.X) * F')
            err = maximum( abs.( chain.target .- reevaluated_Cred ))
            if err > 1e-6
                println("Chain $i (length $(length(chain.target))) has $(sum(err .> 1e-6)) errors. Worst is $(maximum(err)) at $(argmax(err .== maximum(err)))")
            end
            #@assert err < 1e-6 "Chain $i has errors > 1e-6"

            n_burnin = length(chain.target) ÷ 2
            params = (; β, s, safety_margin, model, ndim, scenario, cov_explained, chain=i, save_every, n_sample)
            chain = (; α = chain.X[n_burnin:end, :], 
                       Cred = chain.target[n_burnin:end], 
                       accepted = chain.accepted[n_burnin:end], 
                       parameters = params 
                    )
            


            # Calculate autocorrelation length
            ac = autocor(chain.Cred, lags)
            ac_length = findfirst(ac .< ac_threshold )
            ac_length = isnothing(ac_length) ? Inf : ac_length
            corrlengths[i] = n_burnin + ac_length * save_every

            # Calculate statistics
            acc_rates[i] = mean(chain.accepted)
            Cred_stats.mean[i] = mean(chain.Cred)
            Cred_stats.std[i] = std(chain.Cred)
            Cred_stats.min[i] = minimum(chain.Cred)
            Cred_stats.max[i] = maximum(chain.Cred)
            
            #serialize(fpath, chain)
        
        else 
            #if file exists load chain and calculate statistics
            chain = deserialize(fpath)
            skipped[i] = true
            acc_rates[i] = mean(chain.accepted)
            Cred_stats.mean[i] = mean(chain.Cred)
            Cred_stats.std[i]  = std(chain.Cred)
            Cred_stats.min[i]  = minimum(chain.Cred)
            Cred_stats.max[i]  = maximum(chain.Cred)
            n_burnin = length(chain.Cred) 
            ac = autocor(chain.Cred, lags)
            ac_length = findfirst(ac .< ac_threshold )
            ac_length = isnothing(ac_length) ? Inf : ac_length
            corrlengths[i] = n_burnin + ac_length * save_every
            for i=1:n_sample next!(progress) end
        end 
    end

    println("Resampled ", sum(.! skipped), " chains.")
    println("Acceptance ", round(mean(acc_rates) * 100, sigdigits=4), " ± ", round(std(acc_rates) * 100, sigdigits=4), " % ")
    println("Cred = ", mean(Cred_stats.mean), " ± ", mean(Cred_stats.std), " ∈ [", minimum(Cred_stats.min), " : ", maximum(Cred_stats.max), "]")
    println("Converged ", sum(corrlengths .!= Inf), " / ", n_chains)
    println("Correlation lengths ", mean(corrlengths[corrlengths .!= Inf]), " ± ", std(corrlengths[corrlengths .!= Inf]) )
end

i1 = 1; i2 = length(experiments); is_experiments_limited=false
if length(ARGS) > 0
    i1 = parse(Int, ARGS[1])
    i2 = parse(Int, ARGS[2])
    if i2 > length(experiments)
        warn("Truncated upper limit to $(length(experiments))") 
        i2 = length(experiments) 
    end
    is_experiments_limited=true
end
for i=i1:i2
    println("\n## Experiment $i / $(length(experiments)) " * ( is_experiments_limited ? "(Limited $i1 to $i2)" : "") )
    sample_Cred_chains(; experiments[i]...)
end
