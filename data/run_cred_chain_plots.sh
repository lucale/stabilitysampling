#! /bin/sh

ntotal=32
nbatch=5
nchains=9

# Calculate the number of full batches
nbatches=$(($ntotal / $nbatch))
echo "Starting tmux sessions for $ntotal runs in $nbatches batches of size $nbatch"

# Loop over the full batches
for i in $(seq 1 $nbatches); do
  i1=$((nbatch*($i-1)+1))
  i2=$((nbatch*$i))
  echo "Starting session $i: for runs $i1 to $i2"
  tmux new-session -d -s "$i" "julia --project=.. -t $nchains run_cred_chains.jl $i1 $i2"
done

# Handle remaining runs, if any
i1=$((nbatches * nbatch + 1))
i2=$ntotal
if [ $i1 -le $i2 ]; then  # Use single square brackets for sh
  i=$(($nbatches + 1))
  echo "Starting session $i: for runs $i1 to $i2"
  tmux new-session -d -s "$i" "julia --project=.. -t $nchains run_cred_chains.jl $i1 $i2"
fi