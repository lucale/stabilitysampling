using Serialization
using Distributions
using Parameters

using Revise 
using StabilitySampling 

n_sample = 10_000
force_recompute = false
all_safety_margins = [Inf, 0.05]

for scenario in ["renewable", "conventional" ]
    println("\n## Scenario: $scenario")

    # Network parameters 
    params_path = joinpath(@__DIR__, "pypsa-data", "serialized_networks", "$scenario.serial")
    if !isfile(params_path)
        println("Parameter file '$params_path' not found. Make sure to run 'pypsa-data/serialize_networks.jl' first.")
    end
    
    model_path = joinpath(@__DIR__, "cache", scenario, "models", "MODEL.serial")

    # Output path for QoI 
    save_path = joinpath(@__DIR__, "cache", scenario, "qoi", "qoi_sync.DATA.serial")
    if !isdir(dirname(save_path))
        mkpath(dirname(save_path))
    end

    @unpack k,c,d,m_inv,B,p,f = deserialize(params_path)
    F = flow_matrix(B, k)
    pmin = minimum(p, dims=1)[1,:]
    pmax = maximum(p, dims=1)[1,:]
    pdiff = pmax .- pmin

    #p_bounds = maximum(abs.(p), dims=1)[1,:] .* ( 1 + 0.05 ) # 5% margin

    println("Fitting PCA space")
    pca = deserialize(joinpath(dirname(model_path), "pca.serial"))

    # Routine to calculate QoI
    function calculate_QoI(α::Matrix)
        p = transform(pca, α)
        Cred = redundant_capacity(c, p * F')
        println(" Cred = $(mean(Cred)) ± $(std(Cred)) ∈ [$(minimum(Cred)), $(maximum(Cred))]" )
        u0 = synchronous_state(B, c, p; d,m_inv)
        runi = phase_coherence(B, u0)
        println(" runi = $(mean(runi)) ± $(std(runi)) ∈ [$(minimum(runi)), $(maximum(runi))]" )
        S2 = linear_syncnorm(B, k, u0; d, m_inv)
        println(" S2 = $(mean(S2)) ± $(std(S2)) ∈ [$(minimum(S2)), $(maximum(S2))]" )
        return (; α, #=Cred,=# u0, runi, S2)
    end


    fpath = replace(save_path, "DATA" => "data")
    if !isfile(fpath) || force_recompute
        println("# Calculating QoI for data")
        α = project(pca, pca.signal)
        @time qoi = calculate_QoI(α)
        serialize(fpath, qoi)
    end

    for safety_margin in all_safety_margins
        pmin_safety = pmin .- safety_margin .* pdiff
        pmax_safety = pmax .+ safety_margin .* pdiff

        # IGMM stochastic model
        igmm_path = replace(model_path, "MODEL" => "igmm")
        if !ispath(igmm_path) 
            println("Couldn't find IGMM model at '$igmm_path'. Make sure to run 'fit_models.jl' first.")
        else 
            igmm = deserialize(igmm_path)

            r_igmm = [0.25, 0.5, 0.75, 0.9, 0.99, 1.0]  
            for r in r_igmm 
                ndim = relevant_dims(pca, r)
                fpath = replace(save_path, "DATA" => "igmm_$(ndim)dims_baseline_safetymargin_$(safety_margin)")
                if !isfile(fpath) || force_recompute
                    println("# Calculating QoI for IGMM with $ndim dims ")
                    igmm_submodel = Product( igmm.v[1:ndim] )
                    α = Matrix( rand(igmm_submodel, n_sample)' )

                    p = transform(pca, α)
                    idx_unsafe = .! all( pmin_safety' .<= p .<= pmax_safety', dims=2)[:,1]
                    while sum(idx_unsafe) > 0
                        println("Resampling $(sum(idx_unsafe)) samples")
                        α[idx_unsafe, :] = Matrix( rand(igmm_submodel, sum(idx_unsafe))' )
                        p = transform(pca, α[idx_unsafe, :])
                        idx_unsafe[idx_unsafe] = .! all( pmin_safety' .<= p .<= pmax_safety', dims=2)[:,1]
                    end

                    @time qoi = calculate_QoI(α)
                    serialize(fpath, qoi)
                else
                    println("File '$fpath' already computed. Skipping.")
                end
            end
        end

        # MvGMM stochastic model
        mvgmm_files = filter(f -> startswith(f, "mvgmm"), readdir(dirname(model_path)) ) 
        for mvgmm_file in mvgmm_files
            ndim = parse(Int, replace(split(mvgmm_file, ".")[2], "dims" => "" ))
            fpath = replace(save_path, "DATA" => "mvgmm_$(ndim)dims_baseline_safetymargin_$(safety_margin)")  
            if !isfile(fpath) || force_recompute
                println("# Calculating QoI for MvGMM with $ndim dims ")
                mvgmm = deserialize(joinpath(dirname(model_path), mvgmm_file))
                α = Matrix( rand(mvgmm, n_sample)' )

                p = transform(pca, α)
                idx_unsafe = .! all( pmin_safety' .<= p .<= pmax_safety', dims=2)[:,1]
                while sum(idx_unsafe) > 0
                    println("Resampling $(sum(idx_unsafe)) samples")
                    α[idx_unsafe, :] = Matrix( rand(mvgmm, sum(idx_unsafe))' )
                    p = transform(pca, α[idx_unsafe, :])
                    idx_unsafe[idx_unsafe] = .! all( pmin_safety' .<= p .<= pmax_safety', dims=2)[:,1]
                end

                @time qoi = calculate_QoI(α)
                serialize(fpath, qoi)
            else
                println("File '$fpath' already computed. Skipping.")
            end
        end
    end
end