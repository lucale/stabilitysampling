using Serialization
using Distributions
using Parameters
using ProgressMeter

using CairoMakie 

using Revise 
using StabilitySampling 

function sample_and_save(
    scenario = "renewable",
    n_sample = 100_000_000,
    β = -1e-3, s = 0.01;
    force_recompute = true,
    nchains = Threads.nthreads() )

    # Paths 
    model_path = joinpath(@__DIR__, "cache", scenario, "models", "MODEL.serial")
    params_path = joinpath(@__DIR__, "pypsa-data", "serialized_networks", "$scenario.serial")
    if !isfile(params_path)
        println("Parameter file '$params_path' not found. Make sure to run 'pypsa-data/serialize_networks.jl' first.")
    end
    chain_path = joinpath(@__DIR__, "cache", scenario, "chains", "chain_Cred.MODEL.serial")
    if !isdir(dirname(chain_path))
        mkpath(dirname(chain_path))
    end
        
    @time begin 
        println("Preparing data")
        @unpack k,c,B,p,f = deserialize(params_path)
        F = flow_matrix(B, k)
        Cred_data = redundant_capacity(c, f)
        margin = 0.1
        pmin = minimum(p, dims=1)[1,:] .* ( 1 + margin )
        pmax = maximum(p, dims=1)[1,:] .* ( 1 + margin )
        pca = PCA(p)
        α_data = project(pca, pca.signal)
        bounds_a = minimum(α_data, dims=1)[1,:]
        bounds_b = maximum(α_data, dims=1)[1,:]
    end



    # Load models
    igmm_path = replace(model_path, "MODEL" => "igmm")
    if !ispath(igmm_path) 
        println("Couldn't find IGMM model at '$igmm_path'. Make sure to run 'fit_models.jl' first.")
    end
    r_igmm = [0.25, 0.5, 0.75, 0.9, 0.99, 1.0]  
    igmm_dims = [ relevant_dims(pca, r) for r in r_igmm ]
    igmm = deserialize(igmm_path)
    igmms = [ Product(igmm.v[1:ndim]) for ndim in igmm_dims ]

    mvgmm_files = filter(f->startswith(f, "mvgmm"), readdir(dirname(model_path)))
    mvgmm_dims = parse.(Int, map(f -> replace(split(f, ".")[2], "dims"=>""), mvgmm_files))
    idx = sortperm(mvgmm_dims); mvgmm_files = mvgmm_files[idx]; mvgmm_dims = mvgmm_dims[idx]
    mvgmms = deserialize.(joinpath.(dirname(igmm_path), mvgmm_files))

    for m in ["igmm", "mvgmm"]
        for ψ in ( m == "igmm" ? igmms : mvgmms )
            ndim = length(ψ)
            fpath = replace(chain_path, "MODEL" => "$(m)_$(ndim)dims_beta_$(β)_step_$(s)")
            if isfile(fpath) && !force_recompute
                println("Loading $fpath")
                @unpack α, Cred, acc_rates, β, s = deserialize(fpath)
                
                println("# Loaded $scenario : $m $ndim dims,  β=$β $(round( s * 100, sigdigits=4))% stepsize")
                println(" Acceptance rates ", mean(acc_rates) * 100, " ± ", std(acc_rates) * 100, " % ")
                println(" Cred = ", mean(Cred), " ± ", std(Cred), " ∈ ", extrema(Cred))

            else

                save_every = 20
                burnin = 0.5
                thinning = 0.75

                nlength = ceil(Int, (n_sample ÷ save_every) * burnin * (1 - thinning) ) 
                Cred = zeros(nchains, nlength)
                acc_rates = zeros(nchains)
                α = zeros(nchains, nlength, ndim)

                lp_background(α) = logpdf(ψ, α)
                function lp_target(α) 
                    p = transform(pca, α)
                    if all( pmin .< p .< pmax )
                        return redundant_capacity(c, F * transform(pca, α))
                    else
                        return Inf
                    end
                end

                println("# Sampling $scenario : $m $ndim dims [ β=$β $(round( s * 100, sigdigits=4))% stepsize ] x$(nchains) chains")
                progress = Progress(nchains * n_sample) 
                @time @Threads.threads for ichain = 1:Threads.nthreads()
                    chain = rce_rwm(lp_background, lp_target, n_sample, zeros(ndim), β;
                        stepsize = s .* (bounds_b[1:ndim] .- bounds_a[1:ndim]),
                        lower_bounds=bounds_a[1:ndim], upper_bounds=bounds_b[1:ndim], 
                        save_every, progress
                    )
                    acc_rates[ichain] = mean(chain.accepted)
                    
                    α_chain = clean_chain(chain.X, burnin, thinning)
                    Cred_chain = clean_chain(chain.target, burnin, thinning)                    
                    if size(α_chain, 1) != size(α[ichain, :, :], 1)
                        warn("Resizing chain to match buffer")
                        α_chain = α_chain[1:size(α[ichain, :, :], 1), :]
                        Cred_chain = Cred_chain[1:size(α[ichain, :, :], 1)]
                    end
                    
                    α[ichain, :, :] .= clean_chain(chain.X, burnin, thinning)
                    Cred[ichain, :] .= clean_chain(chain.target, burnin, thinning)

                    
                end
                

                println(" Acceptance rates ", mean(acc_rates) * 100, " ± ", std(acc_rates) * 100, " % ")
                println(" Cred = ", mean(Cred), " ± ", std(Cred), " ∈ ", extrema(Cred))
                serialize(fpath, (; α, Cred, acc_rates, β, s))
            end
        end
    end
end

scenario = "conventional"
n_sample = 100_000_000
β = -7.5e-3; s = 0.01
sample_and_save(scenario, n_sample, β, s, force_recompute=true)