#!/bin/bash

#SBATCH --job-name=s2
#SBATCH --qos=standby
#SBATCH --nodes=1
#SBATCH --cpus-per-task=1
#SBATCH --exclusive=user
#SBATCH --output=slurm_output/s2_%A_%a.out
#SBATCH --error=slurm_output/s2_%A_%a.err
#SBATCH --requeue
#SBATCH --array=0-1000

module load julia

julia --threads=1 --project=.. sample_chains_pik.jl $SLURM_ARRAY_TASK_ID 1001