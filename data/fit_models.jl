using Serialization
using Distributions
using Parameters

using Revise 
using StabilitySampling 

force_recompute = true

for scenario in [ "renewable", "conventional" ]
    println("\n## Scenario: $scenario")

    params_path = joinpath(@__DIR__, "pypsa-data", "serialized_networks", "$scenario.serial")
    if !isfile(params_path)
        println("Parameter file '$params_path' not found. Make sure to run 'pypsa-data/serialize_networks.jl' first.")
    end

    model_path  = joinpath(@__DIR__, "cache", scenario, "models", "MODEL.serial")
    if !isdir(dirname(model_path))
        mkpath(dirname(model_path))
    end

    @unpack p = deserialize(params_path) 

    r_mvgmms = [0.25, 0.5, 0.75, 0.9, 0.95 ]
    if scenario == "renewable"
        n_mvgmms = [  72,  67,   52,  45,   40 ]
    else
        n_mvgmms = [  84,  72,   60,   50,   45 ]
    end

    println("Fitting PCA space")
    pca_path = replace(model_path, "MODEL" => "pca")
    if isfile(pca_path) && !force_recompute
        pca = deserialize(pca_path)
    else
        @time pca = PCA(p)
        serialize(pca_path, pca)
    end



    ## IGMM stochastic model
    # Fit Inverse Gaussian Mixture Model (IGMM) background distribution
    fpath = replace(model_path, "MODEL" => "igmm")
    if isfile(fpath) && !force_recompute
        igmm = deserialize(fpath)
    else
        println("# Fitting IGMM stochastic models ")
        @time igmm = background_igmm(pca)
        serialize(fpath, igmm)

        println("\n# IGMM Components")
        rs = [0.0, 0.25, 0.5, 0.75, 0.9, 0.95, 0.99, 1.0]
        for l=2:length(rs)
            idx = findall( rs[l-1] .< pca.relevance .<= rs[l] )
            ncomp = [ typeof(igmm.v[i]) <: MixtureModel ? length(igmm.v[i].components) : 1 for i in idx]
            println("$(round(rs[l-1] * 100, sigdigits=4)) - $(round(rs[l] * 100, sigdigits=4))%\t$(minimum(idx))-$(maximum(idx))\t$(minimum(ncomp)) - $(maximum(ncomp))")
        end
    end
        

    #MvGMM stochastic model
    for (r,n) in zip(r_mvgmms, n_mvgmms)
        ndim = relevant_dims(pca, r)
        fpath = replace(model_path, "MODEL" => "mvgmm.$(ndim)dims")
        if !isfile(fpath) || force_recompute
            println("# Fitting MvGMM stochastic model $(ndim) dims, $(n) comps")
            @time mvgmm = background_mvgmm(pca, n, relevance=r)
            println("Fitted MvGMM with ", length(mvgmm), " dims.")
            serialize(fpath, mvgmm)
        end
    end


end





