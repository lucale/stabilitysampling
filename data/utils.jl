using Graphs
using Statistics, Distributions
using CairoMakie
using LinearAlgebra
using Test
using Revise 
using StabilitySampling
using ProgressMeter
using Serialization
using Random, Primes, HaltonSequences
using FileIO

# Paths to the data
pypsa_folder = joinpath( @__DIR__, "pypsa-results")
cache_folder = joinpath( @__DIR__, "cache")
plots_folder = joinpath( dirname(@__DIR__), "plots", "images") 

# Network loading functions
list_scenarios() = filter(f->isdir(joinpath(pypsa_folder), f), readdir(pypsa_folder))
list_networks(scenario) = map(f->replace(f, ".nc"=>""), readdir(joinpath(pypsa_folder, scenario, "networks")))
load_network(scenario, network) = Network(joinpath(pypsa_folder, scenario, "networks", network * ".nc"))
function load_network(isrenewable::Bool) 
    if isrenewable
        scenario = "de-elec-renewable"
        network = "elec_s_100_ec_lvopt_5h"
    else 
        scenario = "de-elec-conventional"
        network = "elec_s_100_ec_lv1.0_5h"
    end
    n = load_network(scenario, network)
end 

function calculate_QoI(n::Network, p::Matrix{Float64}; keepsync = false)
    N = nv(n)
    c = get_capacities(n)
    k = get_couplings(n)
    m = get_inertias(n); m_inv = 1 ./ m
    d = get_dampings(n)
    B = incidence_matrix(n)
    F = flow_matrix(B, k)

    f = p * F'
    Cred = redundant_capacity(c, f)
    println(" Cred = $(mean(Cred)) ± $(std(Cred)) ∈ [$(minimum(Cred)), $(maximum(Cred))]")

    println("Calculating sync states ")
    u0 = synchronous_state(B, c, p; d,m_inv,keepsync)
    runi = phase_coherence(B, u0)
    println(" runi = $(mean(runi)) ± $(std(runi)) ∈ [$(minimum(runi)), $(maximum(runi))]")
    
    println("Calculating (linearized) sync norm")
    S2 = linear_syncnorm(B, k, u0; d, m_inv)
    println(" S2 = $(mean(S2)) ± $(std(S2)) ∈ [$(minimum(S2)), $(maximum(S2))]")
    
    return (; p, Cred, u0, runi, S2)
end


function save_figure(fig::Figure, path::String; show=false)
    if ! isfile(path)
        @time begin 
            save(path, fig)
        end
    else
    end
end



function plot_QoI(nt::NamedTuple; figsize=(550, 600), 
    Cred_lims=extrema(nt.Cred), runi_lims=extrema(nt.runi), S2_lims=extrema(nt.S2) )

    figs = [ Figure(); Figure() ]
    ax = map(fig->Axis(fig[1,1]),figs)
    ax[1].xlabel = "Redundant Capacity"
    ax[1].ylabel = "Density"
    xlims!(ax[1], extrema(nt.Cred) .+ (-500, 20))
    ax[2].xlabel = "Phase Coherence"
    ax[2].ylabel = "Density"
    xlims!(ax[2], [-.5, 1.01])
    ax[3].xlabel = "Sync Norm"
    ax[3].ylabel = "Density"
    plt = plot_QoI!(ax, nt)
    return figs,ax,plt
end

function plot_QoI!(axs::Vector{<:Axis}, nt::NamedTuple;
    kwargs...)
    return [
        density!(axs[1], nt.Cred,kwargs...),
        density!(axs[2], nt.runi,kwargs...),
        density!(axs[3], nt.S2,kwargs...),
    ]
end