using Graphs
using Statistics, Distributions
using CairoMakie
using LinearAlgebra
using ProgressMeter
using Serialization
using Random, Primes, HaltonSequences
using FileIO
using Base.Threads
using Test

using Revise 
using StabilitySampling

using DataFrames

function load_params(simpath)
    df = DataFrame()
    for (path, _dirs, files) in walkdir(simpath)
        for filename in files
            if endswith(filename, "params.serial")
                params = deserialize(joinpath(path, filename))
                df_params = DataFrame(map(x -> [x], params))
                df_params.chain_file .= joinpath(path, replace(filename, "params."=>""))
                append!(df, df_params;
                    cols = :union,)
            end
        end
    end
    return df
end

function calculate_props(df_row; burnin = 0.5, thinning = 0.90, save_thin=true)
    chain = deserialize(df_row.chain_file)
    n_sample = df_row.n_sample
    df_row.acceptance_rate = mean(chain.accepted)
    n_burnin = floor(Int, burnin * n_sample)
    idx_thinning = round.(Int, range(n_burnin, n_sample, length=ceil(Int, (n_sample - n_burnin) * (1-thinning))) )

    # Importance sampling 
    w = df_row.β .* chain.S2 ; w = exp.( w .- maximum(w) ); w = w ./ sum(w)
    df_row.S2_mean = sum(w .* chain.S2)
    df_row.S2_mean_β = mean(chain.S2)

    # Compute the correlation length 
    lags = 2 .^ collect( 0:floor(Int, log2(length(chain.S2)) ))
    ac = autocor(chain.S2, lags)
    corlength = findfirst( abs.(ac) .< 1e-3 )
    df_row.corlength = isnothing(corlength) ? Inf : corlength

    if save_thin
        thin_chain = (;
        S2 = chain.S2[idx_thinning],
        α = chain.α[idx_thinning, :],)
        serialize(replace(df_row.chain_file, "results"=>"thin/results"), thin_chain )
    end
end

function add_props(df)
    df[!, "acceptance_rate"] .= 0.
    df[!, "S2_mean"] .= 0.
    df[!, "S2_mean_β"] .= 0.
    df[!, "corlength"] .= 0.
    calculate_props.(eachrow(df))
end


function analyze_chains(df, path::String, burnin=0.5, thinning=0.75)
    @assert endswith(path, "params.serial")

    # Load the chain
    params = deserialize(path)
    chain = deserialize(replace(path, "params.serial" => "serial"))    

    # Print the parameters
    experiment_name = splitext( splitpath(path)[end] )[1]
    df["Experiment"] = experiment_name
    #=
    for n in fieldnames(params)
        println("\t", n, ": \t", getfield(params, n))
    end
    =#

    # Acceptance rate
    acceptance_rate = mean(chain.accepted)
    df[" Acceptance rate"] = acceptance_rate

    # Post processing
    n_sample = params.n_sample
    n_burnin = floor(Int, burnin * n_sample)
    idx_thinning = round.(Int, range(n_burnin, n_sample, length=ceil(Int, (n_sample - n_burnin) * (1-thinning))) )
    chain = (;
        S2 = chain.S2[idx_thinning],
        α = chain.α[idx_thinning, :],
    )

    # Importance sampling 
    w = params.β .* chain.S2 ; w = exp.( w .- maximum(w) ); w = w ./ sum(w)
    S2_mean = sum(w .* chain.S2)
    S2_mean_β = mean(chain.S2)
    println(" S2 mean: ", S2_mean )
    println(" S2 mean β: ", S2_mean_β )

    # Compute the correlation length 
    lags = 2 .^ collect( 0:floor(Int, log2(length(chain.S2)) ))
    ac = autocor(chain.S2, lags)
    corlength = findfirst( ac .< 1e-3 )
    println(" Correlation length: ", corlength)

end

# Load the experiments
experiment_folder = joinpath(@__DIR__, "results/renewable")
experiments_r = joinpath.(experiment_folder, filter( f->endswith(f, "params.serial"), readdir(experiment_folder)) )
experiment_folder = joinpath(@__DIR__, "results/conventional")
experiments_c = joinpath.(experiment_folder, filter( f->endswith(f, "params.serial"), readdir(experiment_folder)) )
experiments = [experiments_c ; experiments_r]
sort!(experiments)

df = DataFrame()


for e in experiments
    analyze_chains!(df, e)
    println("\n")
end